function MatrixPrint(Matrix,name)
%MATRIXPRINT Summary of this function goes here
%   Detailed explanation goes here
filename = ['Prints/' name '.txt']
fileID = fopen(filename,'w');
for i = 1:size(Matrix,1)
    for j = 1:size(Matrix,2)
        fprintf(fileID,'double %s%i%i = %s;\n',name,i,j,char(Matrix(i,j)));
    end
    fprintf(fileID,'\n');
end    

end

