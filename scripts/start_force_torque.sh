# create the dev for the jr3 sensor
sudo mknod /dev/jr3 c 39 0
sudo chmod 0666 /dev/jr3

# load the kernel module 
sudo modprobe jr3pci-driver

source /opt/ros/kinetic/setup.bash
source ~/ros/workspaces/jr3_ws/devel/setup.bash

# set ros master uri
#export ROS_MASTER_URI=http://ur10-ws6:11311
#export ROS_IP=10.152.246.168
# make sure to set ROS_HOSTNAME on other pc properly
# export ROS_HOSTNAME=ur10-ws6
# export ROS_MASTER_URI=http://ur10-ws6:11311


# run the node
rosrun tum_ics_jr3 jr3_node

