#include "../include/shapeDetection.h"
#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui.hpp>
#include <opencv2/calib3d.hpp>

// MSG/SRV INCLUDES
#include "find_goal/Shape.h"
#include "find_goal/ShapeRequest.h"
#include "find_goal/ShapeResponse.h"

namespace shapeDetection {
        
    shapeDetection::shapeDetection():
    m_camMat(3,3,CV_64FC1), m_TW_Cam(3,4,CV_64FC1)//, m_cannyThresholds(2, 0), m_dist(5,1,CV_64FC1)
    {
        m_image = cv::Mat::zeros(480, 600, CV_8UC3);
        m_binaryImg = cv::Mat::zeros(480, 600, CV_RGB2GRAY);
        m_cannyThresholds = {0, 0};
        m_dist = cv::Mat(5,1,CV_64FC1);

        //distortion matrix of camera
        m_dist.at<double>(0,0) = -0.011126661887989320;
        m_dist.at<double>(1,0) = -0.016347691203269087;
        m_dist.at<double>(2,0) =  0.0071667643753467397;
        m_dist.at<double>(3,0) = -0.0051578162628590557;
        m_dist.at<double>(4,0) = -0.18749329833679762;
        //camera matrix
        m_camMat = cv::Mat::zeros(3,3,CV_64FC1);
        m_camMat.at<double>(0,0) = 525.37744109090511;
        m_camMat.at<double>(0,2) = 332.54295396076247;
        m_camMat.at<double>(1,1) = 521.64003133156825;
        m_camMat.at<double>(1,2) = 227.72855035107355;
        m_camMat.at<double>(2,2) = 1.0;

        m_countMutexSL = PTHREAD_MUTEX_INITIALIZER;
        m_countMutexImg = PTHREAD_MUTEX_INITIALIZER;
    }

    shapeDetection::~shapeDetection()
    {
    }

    /** callback: subscribe to image_raw of usb camera and save image in class variable */
    void shapeDetection::getImageCallback(const sensor_msgs::ImageConstPtr &msg) {
        cv_bridge::CvImagePtr cv_ptr;
        try {
            cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
            pthread_mutex_lock(&this->m_countMutexImg);
            m_image = cv_ptr->image;
            pthread_mutex_unlock(&this->m_countMutexImg);
        }
        catch (cv_bridge::Exception& e) {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return;
        }

        ROS_INFO_STREAM("Received an image");

    }

    /** query array parameter from ros::param and return value */
    std::vector<double> shapeDetection::getParam(std::string name) {
        // get params
        std::stringstream s;
        std::vector<double> val;
        s << "~shape_detection";
        if (!ros::param::has(s.str())) {
            ROS_ERROR_STREAM("Parameter for shape detection not available!");
        }
        s << "/" << name;
        ros::param::get(s.str(), val);

        ROS_INFO_STREAM("Param " << name << " has value " << val[0]);
        
        return val;
    }

    /** returns the rgb image in the member variable */
    cv::Mat shapeDetection::getImage() {
        return m_image;
    }

    bool shapeDetection::shapeLocationCallback(find_goal::ShapeRequest& req, find_goal::ShapeResponse& res) {
        
        pthread_mutex_lock(&this->m_countMutexSL);
        if (m_shapeLocation.size() > 0) {
            res.shape = m_shapeLocation;
        } else {
            pthread_mutex_unlock(&this->m_countMutexSL);
            return false;
        }
        pthread_mutex_unlock(&this->m_countMutexSL);
    
        return true;
    }

    /** Set the member variable shape Location  */
    void shapeDetection::setShapeLocation(std::vector<cv::Point3d> shapeLocation) {
        
        pthread_mutex_lock(&this->m_countMutexSL);
        m_shapeLocation.resize(shapeLocation.size());
        for (uint i=0; i<shapeLocation.size(); i++) {
            m_shapeLocation[i].x = shapeLocation[i].x;
            m_shapeLocation[i].y = shapeLocation[i].y;
            m_shapeLocation[i].z = shapeLocation[i].z;
        }
        pthread_mutex_unlock(&this->m_countMutexSL);
    }
    

    /** @brief initialise the shape detection class
     * 
     * - subscribe to image topic
     * - advertise shape locatio service
     * - get parameters from param server
     * 
     */
    void shapeDetection::init() {
        // subscribe to camera image
        ros::NodeHandle n;
        image_transport::ImageTransport it(n);
        m_sub_camera = it.subscribe("image_raw", 1, &shapeDetection::getImageCallback, this);

        // advertise shape service
        m_shape_service = n.advertiseService("/find_goal/shape_location",
                &shapeDetection::shapeLocationCallback,
                this);

        // get params for shape detection
        m_cannyThresholds = getParam("canny_thresh");
        ROS_INFO_STREAM("Canny thresholds: " << m_cannyThresholds[0] << ", " << m_cannyThresholds[1]);

        // get region of interest
        std::vector<double> roi = getParam("region_of_interest");
        m_roi = cv::Rect(roi[0], roi[1], roi[2], roi[3]);

        // get transformation of camera into world
        std::stringstream s;
        std::vector<double> val;
        s << "~shape_detection";
        if (!ros::param::has(s.str())) {
            ROS_ERROR_STREAM("Parameter for shape detection not available!");
        }
        s << "/" << "TW_Cam";
        ros::param::get(s.str(), val);
        m_TW_Cam = cv::Mat::zeros(3,4,CV_64FC1);
        for (int i=0; i<3; i++) {
            for (int j=0; j<4; j++) {
                m_TW_Cam.at<double>(i,j) = val[4*i+j];
            }
        }

        ROS_INFO_STREAM("World->Cam Trafo:" << std::endl << m_TW_Cam);
    }

    /** @brief get contour in image (member variable) and extract shape
     * 
     * use the m_image, create binary image with canny edge detector.
     * Extract the contours of shapes in the image and approximate their
     * locations.
     * 
     * @params
     * roi:
     *      region of interest, specify region were we should look shapes
     * display:
     *      show image with detected shapes
     * canny_thresh:
     *      thresholds of canny edge detector
     * 
     */
    std::vector<cv::Point> shapeDetection::getShape(bool display) {
        // create local copy of image
        pthread_mutex_lock(&this->m_countMutexImg);
        cv::Mat draw_image = m_image.clone();
        pthread_mutex_unlock(&this->m_countMutexImg);

        // extract region of interest from image
        std::vector<double> roi = getParam("region_of_interest");
        m_roi = cv::Rect(roi[0], roi[1], roi[2], roi[3]);
        cv::Point roi_offset(roi[0], roi[1]);
        ROS_INFO_STREAM("roi size " << m_roi << ", image size " << draw_image.size());
        cv::Mat image_roi = draw_image(m_roi);
        m_binaryImg = cv::Mat(m_roi.width, m_roi.height, CV_RGB2GRAY);


        ros::spinOnce();

        // update param
        m_cannyThresholds = getParam("canny_thresh");

        // create binary image
        cv::Canny(image_roi, m_binaryImg, m_cannyThresholds[0], m_cannyThresholds[1], 3);
        ROS_INFO_STREAM("Binar Img size " << m_binaryImg.size());
        
        std::vector<std::vector<cv::Point> > contours;
        std::vector<std::vector<cv::Point> > contours_out;
        std::vector<cv::Point> shape;
        std::vector<cv::Vec4i> hierarchy;
        
        cv::findContours(m_binaryImg, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
        
        contours_out.resize(contours.size());
        if (contours.size() > 0) {
            // variable to mark removeable contours
            std::vector<bool> del_cont(contours_out.size(), false);

            // for all contours, approximate them
            for (size_t k=0; k < contours.size(); k++) {
                approxPolyDP(cv::Mat(contours[k]), contours_out[k], 3, false);
                // for (int j=0; j < contours_out[j].size(); j++) {
                //     // delete if one of the contour points is close to the border
                //     if (contours_out[k][j].x < 10) {
                //         del_cont[k] = true;
                //     }
                //     if (contours_out[k][j].x > m_binaryImg.size().width-10) {
                //         del_cont[k] = true;
                //     }
                //     if (contours_out[k][j].y < 10) {
                //         del_cont[k] = true;
                //     }
                //     if (contours_out[k][j].y > m_binaryImg.size().height -10) {
                //         del_cont[k] = true;
                //     }
                // }
                // add offset to contour points
                std::for_each(contours_out[k].begin(), contours_out[k].end(), [&roi_offset](cv::Point& d) { d+= roi_offset;});
            }

            std::vector<std::vector<cv::Point>> relevant_cont = contours_out;
            // ROS_INFO_STREAM("cont out size " << contours_out.size());
            // for (uint i=0; i<contours_out.size(); i++) {
            //     ROS_INFO_STREAM("delete? " << del_cont[i] << "  " << i);
            //     if (!del_cont[i]) {
            //         ROS_INFO_STREAM("Adding shape no " << i+1);
            //         relevant_cont.push_back(contours_out[i]);
            //     }
            // }

            ROS_INFO_STREAM("relevant cont size " << relevant_cont.size());

            if (display && relevant_cont.size() > 0) {
                for (uint k=0; k<relevant_cont.size(); k++) {
                    try {
                        cv::drawContours(draw_image, contours_out, -1, CV_RGB(250, 100, 50), 2, 8, hierarchy, INT_MAX);
                    }
                    catch( cv::Exception& e ){
                        ROS_INFO_STREAM("Opencv exception caught at drawing contours.");
                    }
                    for (uint j = 0; j <= relevant_cont[k].size(); j++) {
                        cv::circle(draw_image, relevant_cont[k][j], 3, CV_RGB(255,200,50), -1, 8, 0);
                        cv::putText(draw_image, std::to_string(j),  relevant_cont[k][j], cv::FONT_HERSHEY_SIMPLEX, 1, CV_RGB(250,255,50), 1, 8, false);
                    }
                }
                cv::rectangle(draw_image, m_roi, CV_RGB(120, 50, 200), 1, 8, 0);
                namedWindow( "Detected Shapes", cv::WINDOW_AUTOSIZE );
                imshow( "Detected Shapes", draw_image );
                cv::waitKey(1);
            }

            
            // TODO: not just use the first one, determine the best shape
            shape.resize(relevant_cont.front().size());
            shape = relevant_cont.front();
            ROS_INFO_STREAM("# of shapes            " << relevant_cont.size() << ", " << contours_out.size());
            ROS_INFO_STREAM("# of points in shape:  " << shape.size());
        }

        return shape;
    }

    /** @brief calculate the world coordinates of a point in the image
     * 
     * output coordinates are calculated with the inverse of TW_Cam and camMat
     * 
     * Got the ideas here:
     * https://docs.opencv.org/3.2.0/d9/d0c/group__calib3d.html#ga549c2075fac14829ff4a58bc931c033d
     * https://stackoverflow.com/questions/12299870/computing-x-y-coordinate-3d-from-image-point
     */
    cv::Point3d shapeDetection::getWorldLocation(cv::Point img_point, cv::Mat trafo) {
        // z coordinate is always 0
        double s = 0.0, z = 0.0;
        // get rotation and translation
        cv::Rect rot_ind = cv::Rect(0, 0, 3, 3);
        cv::Rect trans_ind = cv::Rect(3, 0, 1, 3);
        cv::Mat rotation = trafo(rot_ind);
        cv::Mat translation = trafo(trans_ind);
        cv::Mat invRotation = rotation.inv(cv::DECOMP_SVD);
        cv::Mat invCamMat = m_camMat.inv(cv::DECOMP_SVD);
        
        // homogeneous coordinates
        cv::Mat img_p  = (cv::Mat_<double>(3,1) << double(img_point.x), double(img_point.y), 1);
        
        // calculate the world coordinates
        cv::Mat invRotCam = invRotation * invCamMat * img_p;
        cv::Mat invRotTra = invRotation * translation;
        // scaling factor
        s = z + invRotTra.at<double>(2,0) / invRotCam.at<double>(2,0);

        cv::Mat world_coords = invRotation * (s * invCamMat * img_p - translation);
        return cv::Point3d(world_coords.at<double>(0,0), world_coords.at<double>(1,0), world_coords.at<double>(2,0));

    }

    /* @brief return world coordinates for vector of image points
     * 
     * use the class member transformation
     * returns vector of Point3d
    */
    std::vector<cv::Point3d> shapeDetection::getWorldLocations(std::vector<cv::Point> shape) {
        std::vector<cv::Point3d> shape_points;
        shape_points.resize(shape.size());
        for (uint i=0; i < shape.size(); i++) {
            shape_points[i] = getWorldLocation(shape[i], m_TW_Cam);
        }
        return shape_points;

    }

    /** @brief calculate the image coordinates of a point in the world
     * 
     * Output coordinates are calculated with TW_Cam. Basically represents
     * the inverse of getWorldLocation.
     */
    cv::Point shapeDetection::getImgLocation(cv::Mat world_point, cv::Mat trafo) {
        cv::Mat img_coords(3,1, CV_64F);
        img_coords = m_camMat * trafo * world_point;
        img_coords = img_coords / img_coords.at<double>(2,0);
        
        // convert to point
        return cv::Point(img_coords.at<double>(0,0), img_coords.at<double>(1,0));
    }

    /** @brief get extrinsic parameters of camera in terms of 'calibration' 
     * 
     * Steps:
     * 1. undistort image
     * 2. extract 4 features, must correspond to those points with known world coordinates
     * 3. solve the PNP problem
     * 4. test the trafo with actual world points
     * 
     */
    void shapeDetection::getExtrinsics(cv::Mat binaryImg) {
        // undistort image
        cv::Mat undistorted;
        cv::undistort(binaryImg, undistorted, m_camMat, m_dist);
        
        // detect features
        std::vector<cv::Point2d> keypoints;
        // use features of our calibration pattern
        int maxCorners = 4;
        cv::goodFeaturesToTrack(undistorted, keypoints, maxCorners, 0.1, 10, cv::noArray(), 3, false, 0.04000000000000000083);
        ROS_INFO_STREAM("Harris found " << keypoints.size() << " features");

        // define world points
        std::vector<cv::Point3d> world_points;

        // get offsets from parameter server
        std::vector<double> offset_paper = getParam("calibration/offset_paper");
        std::vector<double> pose_x = getParam("calibration/pose_x");
        std::vector<double> pose_y = getParam("calibration/pose_y");
        
        // offsets of origin to calibration paper
        double oX = offset_paper[0];
        double oY = offset_paper[1];
        double oZ = offset_paper[2];
        // have to be adjusted for each setup
        world_points.push_back(cv::Point3d(oX + pose_x[0], oY + pose_y[0], oZ + 0.0));
        world_points.push_back(cv::Point3d(oX + pose_x[1], oY + pose_y[1], oZ + 0.0));
        world_points.push_back(cv::Point3d(oX + pose_x[2], oY + pose_y[2], oZ + 0.0));
        world_points.push_back(cv::Point3d(oX + pose_x[3], oY + pose_y[3], oZ + 0.0));

        for (uint i=0; i <keypoints.size(); i++) {
            cv::circle(m_image, keypoints[i], 2, CV_RGB(250,255-50*i,50), -1, 8, 0);
            cv::putText(m_image, std::to_string(i), keypoints[i], cv::FONT_HERSHEY_SIMPLEX, 1, CV_RGB(250,255-50*i,50), 1, 8, false);
        }

        // find extrinsics with a guess for transformation
        cv::Mat tvec = cv::Mat::zeros(3,1,CV_64F);
        tvec.at<double>(0,0) =  0.3;
        tvec.at<double>(1,0) = -0.4;
        tvec.at<double>(2,0) =  0.35;
        cv::Mat rvec = cv::Mat::eye(3,1,CV_64F);
        cv::Mat rot, TW_C;

        // get transformation from from base to camera
        bool use_predefined_trafo = false;
        cv::solvePnP(world_points, keypoints, m_camMat, m_dist, rvec, tvec, use_predefined_trafo, cv::SOLVEPNP_P3P);
        cv::Rodrigues(rvec, rot);
        cv::hconcat(rot, tvec, TW_C);

        // draw base into image
        cv::Mat base_World = cv::Mat::zeros(4, 1, CV_64FC1);
        base_World.at<double>(3,0) = 1.0;
        cv::Point base_Img = getImgLocation(base_World, TW_C);
        ROS_INFO_STREAM("Projected base in the image: " << base_Img);
        cv::circle(m_image, base_Img, 6, CV_RGB(255,10,20), 2, 8, 0);

        // test the backwards projection
        cv::Point3d projected_base = getWorldLocation(base_Img, TW_C);
        ROS_INFO_STREAM("Base " << base_World.t() << ", re-projected base: " << projected_base);

        // test with actual world point, lower left corner of paper
        cv::Mat world_feat = cv::Mat::ones(4, 1, CV_64FC1);
        std::vector<double> test_point = getParam("calibration/test_point");
        world_feat.at<double>(0,0) = test_point[0] + oX;
        world_feat.at<double>(1,0) = test_point[1] + oY;
        world_feat.at<double>(2,0) = test_point[2] + oZ;
        cv::Point feat_point_Img = getImgLocation(world_feat, TW_C);
        ROS_INFO_STREAM("world feat point: " << world_feat.t() << ", back in image: " << feat_point_Img);
        cv::circle(m_image, feat_point_Img, 6, CV_RGB(2,250,100), 2, 8, 0);
        
        // save transformation as parameters by hand
        ROS_WARN_STREAM("Save transformation to parameters by hand!");
        ROS_INFO_STREAM("trafo TW_Cam: " << std::endl << TW_C);

        namedWindow( "Calibration Points", cv::WINDOW_AUTOSIZE);
        imshow( "Calibration Points", m_image );
        cv::waitKey(0);

        
    }
} // shapeDetection