clear

syms q1 q2 q3 q4 q5 q6 q1p q2p q3p q4p q5p q6p q1pp q2pp q3pp q4pp q5pp q6pp real

syms L1 L2 L3 L4 L5 L6 L7 L8 L9 L10 L11 L12 real
syms I111 I112 I113 I122 I123 I133 I211 I212 I213 I222 I223 I233 I311 I312 I313 I322 I323 I333 I411 I412 I413 I422 I423 I433 I511 I512 I513 I522 I523 I533 I611 I612 I613 I622 I623 I633 real
syms m1 m2 m3 m4 m5 m6 gx gy gz real

q = [q1; q2; q3; q4; q5; q6];
qp = [q1p; q2p; q3p; q4; q5; q6];
qpp = [q1pp; q2pp; q3pp; q4pp; q5pp; q6pp];
    
%% Calculate homogeneous transformations
H1_0 = GenerateH(  q1,      L1,     0,     sym(pi)/2);
H2_1 = GenerateH(  q2,      0,     -L3,   0);
H3_2 = GenerateH(  q3,      0,     -L11, 0);
H4_3 = GenerateH(  q4,      L2,     0,     sym(pi)/2);
H5_4 = GenerateH(  q5,      L12,    0,     -sym(pi)/2);
H6_5 = GenerateH(  q6,      L4,     0,     0);

Hcm1_0 = GenerateH(q1,      L1,     0,     0);
Hcm2_1 = GenerateH(q2,      L7,     -L8,   0);
Hcm3_2 = GenerateH(q3,      -L5,     -L10,  0);
Hcm4_3 = GenerateH(q4,      L2,     0,     0);
Hcm5_4 = GenerateH(q5,      L12,    0,     0);
Hcm6_5 = GenerateH(q6,      L4,     0,     0);

H1_0 = simplify(H1_0);
H2_0 = simplify(H1_0 * H2_1);
H3_0 = simplify(H2_0 * H3_2);
H4_0 = simplify(H3_0 * H4_3);
H5_0 = simplify(H4_0 * H5_4);
H6_0 = simplify(H5_0 * H6_5);

Hcm1_0 = simplify(Hcm1_0);
Hcm2_0 = simplify(H1_0*Hcm2_1);
Hcm3_0 = simplify(H2_0*Hcm3_2);
Hcm4_0 = simplify(H3_0*Hcm4_3);
Hcm5_0 = simplify(H4_0*Hcm5_4);
Hcm6_0 = simplify(H5_0*Hcm6_5);



%% Jacobians links
J1_0_1 = [cross([0;0;1],(H1_0(1:3,4)));[0;0;1]];

J1_0 = [J1_0_1, zeros(6,5)];
J1_0_v = J1_0(1:3,:);
J1_0_w = J1_0(4:6,:);


J2_0_1 = [cross([0;0;1],(H2_0(1:3,4)));[0;0;1]];
J2_0_2 = [cross(H1_0(1:3,3),(H2_0(1:3,4)-H1_0(1:3,4)));H1_0(1:3,3)];

J2_0 = [J2_0_1, J2_0_2, zeros(6,4)];
J2_0_v = J2_0(1:3,:);
J2_0_w = J2_0(4:6,:);


J3_0_1 = [cross([0;0;1],(H3_0(1:3,4)));[0;0;1]];
J3_0_2 = [cross(H1_0(1:3,3), (H3_0(1:3,4) - H1_0(1:3,4))); H1_0(1:3,3)];
J3_0_3 = [cross(H2_0(1:3,3), (H3_0(1:3,4) - H2_0(1:3,4))); H2_0(1:3,3)];

J3_0 = simplify([J3_0_1 , J3_0_2, J3_0_3, zeros(6,3)]);
J3_0_v = J3_0(1:3,:);
J3_0_w = J3_0(4:6,:);


J4_0_1 = [cross([0;0;1],(H4_0(1:3,4)));[0;0;1]];
J4_0_2 = [cross(H1_0(1:3,3), (H4_0(1:3,4) - H1_0(1:3,4))); H1_0(1:3,3)];
J4_0_3 = [cross(H2_0(1:3,3), (H4_0(1:3,4) - H2_0(1:3,4))); H2_0(1:3,3)];
J4_0_4 = [cross(H3_0(1:3,3), (H4_0(1:3,4) - H3_0(1:3,4))); H3_0(1:3,3)];

J4_0 = simplify([J4_0_1 , J4_0_2, J4_0_3, J4_0_4, zeros(6,2)]);

J4_0_v = J4_0(1:3,:);
J4_0_w = J4_0(4:6,:);

J5_0_1 = [cross([0;0;1],(H5_0(1:3,4)));[0;0;1]];
J5_0_2 = [cross(H1_0(1:3,3), (H5_0(1:3,4) - H1_0(1:3,4))); H1_0(1:3,3)];
J5_0_3 = [cross(H2_0(1:3,3), (H5_0(1:3,4) - H2_0(1:3,4))); H2_0(1:3,3)];
J5_0_4 = [cross(H3_0(1:3,3), (H5_0(1:3,4) - H3_0(1:3,4))); H3_0(1:3,3)];
J5_0_5 = [cross(H4_0(1:3,3), (H5_0(1:3,4) - H4_0(1:3,4))); H4_0(1:3,3)];

J5_0 = simplify([J5_0_1 , J5_0_2, J5_0_3, J5_0_4, J5_0_5, zeros(6,1)]);

J5_0_v = J5_0(1:3,:);
J5_0_w = J5_0(4:6,:);


J6_0_1 = [cross([0;0;1],(H6_0(1:3,4)));[0;0;1]];
J6_0_2 = [cross(H1_0(1:3,3), (H6_0(1:3,4) - H1_0(1:3,4))); H1_0(1:3,3)];
J6_0_3 = [cross(H2_0(1:3,3), (H6_0(1:3,4) - H2_0(1:3,4))); H2_0(1:3,3)];
J6_0_4 = [cross(H3_0(1:3,3), (H6_0(1:3,4) - H3_0(1:3,4))); H3_0(1:3,3)];
J6_0_5 = [cross(H4_0(1:3,3), (H6_0(1:3,4) - H4_0(1:3,4))); H4_0(1:3,3)];
J6_0_6 = [cross(H5_0(1:3,3), (H6_0(1:3,4) - H5_0(1:3,4))); H5_0(1:3,3)];

J6_0 = simplify([J6_0_1 , J6_0_2, J6_0_3, J6_0_4, J6_0_5, J6_0_6]);

J6_0_v = J6_0(1:3,:);
J6_0_w = J6_0(4:6,:);

%% Jacobian cms

Jcm1_1 = [cross([0;0;1],(Hcm1_0(1:3,4)));[0;0;1]];

Jcm1 = [Jcm1_1, zeros(6,5)];
Jcm1_v = Jcm1(1:3,:);
Jcm1_w = Jcm1(4:6,:);


Jcm2_1 = [cross([0;0;1],(Hcm2_0(1:3,4)));[0;0;1]];
Jcm2_2 = [cross(H1_0(1:3,3),(Hcm2_0(1:3,4)-H1_0(1:3,4)));H1_0(1:3,3)];

Jcm2 = [Jcm2_1, Jcm2_2, zeros(6,4)];
Jcm2_v = Jcm2(1:3,:);
Jcm2_w = Jcm2(4:6,:);


Jcm3_1 = [cross([0;0;1],(H3_0(1:3,4)));[0;0;1]];
Jcm3_2 = [cross(H1_0(1:3,3), (Hcm3_0(1:3,4) - H1_0(1:3,4))); H1_0(1:3,3)];
Jcm3_3 = [cross(H2_0(1:3,3), (Hcm3_0(1:3,4) - H2_0(1:3,4))); H2_0(1:3,3)];

Jcm3 = simplify([Jcm3_1 , Jcm3_2, Jcm3_3, zeros(6,3)]);
Jcm3_v = Jcm3(1:3,:);
Jcm3_w = Jcm3(4:6,:);


Jcm4_1 = [cross([0;0;1],(Hcm4_0(1:3,4)));[0;0;1]];
Jcm4_2 = [cross(H1_0(1:3,3), (Hcm4_0(1:3,4) - H1_0(1:3,4))); H1_0(1:3,3)];
Jcm4_3 = [cross(H2_0(1:3,3), (Hcm4_0(1:3,4) - H2_0(1:3,4))); H2_0(1:3,3)];
Jcm4_4 = [cross(H3_0(1:3,3), (Hcm4_0(1:3,4) - H3_0(1:3,4))); H3_0(1:3,3)];

Jcm4 = simplify([Jcm4_1 , Jcm4_2, Jcm4_3, Jcm4_4, zeros(6,2)]);

Jcm4_v = Jcm4(1:3,:);
Jcm4_w = Jcm4(4:6,:);

Jcm5_1 = [cross([0;0;1],(Hcm5_0(1:3,4)));[0;0;1]];
Jcm5_2 = [cross(H1_0(1:3,3), (Hcm5_0(1:3,4) - H1_0(1:3,4))); H1_0(1:3,3)];
Jcm5_3 = [cross(H2_0(1:3,3), (Hcm5_0(1:3,4) - H2_0(1:3,4))); H2_0(1:3,3)];
Jcm5_4 = [cross(H3_0(1:3,3), (Hcm5_0(1:3,4) - H3_0(1:3,4))); H3_0(1:3,3)];
Jcm5_5 = [cross(H4_0(1:3,3), (Hcm5_0(1:3,4) - H4_0(1:3,4))); H4_0(1:3,3)];

Jcm5 = simplify([Jcm5_1 , Jcm5_2, Jcm5_3, Jcm5_4, Jcm5_5, zeros(6,1)]);

Jcm5_v = Jcm5(1:3,:);
Jcm5_w = Jcm5(4:6,:);


Jcm6_1 = [cross([0;0;1],(Hcm6_0(1:3,4)));[0;0;1]];
Jcm6_2 = [cross(H1_0(1:3,3), (Hcm6_0(1:3,4) - H1_0(1:3,4))); H1_0(1:3,3)];
Jcm6_3 = [cross(H2_0(1:3,3), (Hcm6_0(1:3,4) - H2_0(1:3,4))); H2_0(1:3,3)];
Jcm6_4 = [cross(H3_0(1:3,3), (Hcm6_0(1:3,4) - H3_0(1:3,4))); H3_0(1:3,3)];
Jcm6_5 = [cross(H4_0(1:3,3), (Hcm6_0(1:3,4) - H4_0(1:3,4))); H4_0(1:3,3)];
Jcm6_6 = [cross(H5_0(1:3,3), (Hcm6_0(1:3,4) - H5_0(1:3,4))); H5_0(1:3,3)];

Jcm6 = simplify([Jcm6_1 , Jcm6_2, Jcm6_3, Jcm6_4, Jcm6_5, Jcm6_6]);

Jcm6_v = Jcm6(1:3,:);
Jcm6_w = Jcm6(4:6,:);

%% Inertia Matrices

I1 = [I111, I112, I113;
      I112, I122, I123;
      I113, I123, I133];
  
I2 = [I211, I212, I213;
      I212, I222, I223;
      I213, I223, I233];
  
I3 = [I311, I312, I313;
      I312, I322, I323;
      I313, I323, I333];

I4 = [I411, I412, I413;
      I412, I422, I423;
      I413, I423, I433];
  
I5 = [I511, I512, I513;
      I512, I522, I523;
      I513, I523, I533];
  
I6 = [I611, I612, I613;
      I612, I622, I623;
      I613, I623, I633];



 %% Rotation Matrices
 
Rcm1_0 = Hcm1_0(1:3,1:3);
Rcm2_0 = Hcm2_0(1:3,1:3);
Rcm3_0 = Hcm3_0(1:3,1:3);
Rcm4_0 = Hcm4_0(1:3,1:3);
Rcm5_0 = Hcm5_0(1:3,1:3);
Rcm6_0 = Hcm6_0(1:3,1:3);

R1_0 = H1_0(1:3,1:3);
R2_0 = H2_0(1:3,1:3);
R3_0 = H3_0(1:3,1:3);
R4_0 = H4_0(1:3,1:3);
R5_0 = H5_0(1:3,1:3);
R6_0 = H6_0(1:3,1:3);


%% Translation Vectors
tcm1_0 = Hcm1_0(1:3,4);
tcm2_0 = Hcm2_0(1:3,4);
tcm3_0 = Hcm3_0(1:3,4);
tcm4_0 = Hcm4_0(1:3,4);
tcm5_0 = Hcm5_0(1:3,4);
tcm6_0 = Hcm6_0(1:3,4);

t1_0 = H1_0(1:3,4);
t2_0 = H2_0(1:3,4);
t3_0 = H3_0(1:3,4);
t4_0 = H4_0(1:3,4);
t5_0 = H5_0(1:3,4);
t6_0 = H6_0(1:3,4);



%% M-Matrix 


M1 = simplify(expand(m1 * (Jcm1_v' * Jcm1_v) + Jcm1_w' * Rcm1_0 * I1 * Rcm1_0' * Jcm1_w));
M2 = simplify(expand(m2 * (Jcm2_v' * Jcm2_v) + Jcm2_w' * Rcm2_0 * I2 * Rcm2_0' * Jcm2_w));
M3 = simplify(expand(m3 * (Jcm3_v' * Jcm3_v) + Jcm3_w' * Rcm3_0 * I3 * Rcm3_0' * Jcm3_w));
M4 = simplify(expand(m4 * (Jcm4_v' * Jcm4_v) + Jcm4_w' * Rcm4_0 * I4 * Rcm4_0' * Jcm4_w));
M5 = simplify(expand(m5 * (Jcm5_v' * Jcm5_v) + Jcm5_w' * Rcm5_0 * I5 * Rcm5_0' * Jcm5_w));
M6 = simplify(expand(m6 * (Jcm6_v' * Jcm6_v) + Jcm6_w' * Rcm6_0 * I6 * Rcm6_0' * Jcm6_w));

M = simplify(M1 + M2 + M3 + M4 + M5 + M6);

%Checking skew symmetry
M - transpose(M)

%% C-Matrix

C = sym(zeros(6));

for k=1:6 
    for j=1:6 
        C(k,j) = simplify(expand(0.5 * ((diff(M(k,j),q1) + diff(M(k,1), q(j)) - diff(M(1,j), q(k))) * q1p + ...
                                        (diff(M(k,j),q2) + diff(M(k,2), q(j)) - diff(M(2,j), q(k))) * q2p + ...
                                        (diff(M(k,j),q3) + diff(M(k,3), q(j)) - diff(M(3,j), q(k))) * q3p + ... 
                                        (diff(M(k,j),q4) + diff(M(k,4), q(j)) - diff(M(4,j), q(k))) * q4p + ... 
                                        (diff(M(k,j),q5) + diff(M(k,5), q(j)) - diff(M(5,j), q(k))) * q5p + ... 
                                        (diff(M(k,j),q6) + diff(M(k,6), q(j)) - diff(M(6,j), q(k))) * q6p))); 
    end 
end
C = simplify(C);

% Checking condition

Md = simplify(diff(M,q1)*q1p + diff(M,q2)*q2p + diff(M,q3)*q3p + diff(M,q4)*q4p + diff(M,q5)*q5p + diff(M,q6)*q6p);
N = simplify(Md - 2*C);
simplify(N + transpose(N))

%% G-Matrix

grav = [gx; gy; gz];
P = m1*grav'*tcm1_0 + m2*grav'*tcm2_0 + m3*grav'*tcm3_0 + m4*grav'*tcm4_0 + m5*grav'*tcm5_0 + m6*grav'*tcm6_0;

G = sym(zeros(6,1));
for k=1:6 
    G(k,1) = simplify(diff(P, q(k)));
end







