#ifndef DRAWING_H
#define DRAWING_H

#include <ros/ros.h>
#include <tumtools/Math/MathTools.h>

#include "ur10bender/drawConfig.h"
#include "ur10bender/drawConfigRequest.h"
#include "ur10bender/drawConfigResponse.h"

#include <std_msgs/String.h>
#include <std_srvs/Trigger.h>

class Drawing {

private:
    ros::ServiceClient m_shapeClient;
    ros::Publisher m_goalPubQ;
    ros::Publisher m_goalPubX;
    ros::Publisher m_drawGoalsPub;
    ros::Subscriber m_goalsReached;
    ros::ServiceServer m_drawPlanning;
    ros::ServiceServer m_drawInitialise;

    std::vector<Tum::Vector3d> m_shapeCoordinates;

    // starting time
    double m_startTime;
    int m_currStep;
    // duration of steps between points
    std::vector<double> m_timeSteps;

    // 0: not started yet, 1: first step, 2: currently drawing, 3: finished
    int m_drawing;
    bool m_goalReachedFS;
    

    pthread_mutex_t m_countMutex;

public:
    Drawing();
    ~Drawing();

    void init(ros::NodeHandle n, double start_time);
    int serviceCall();

    void nextStep(double time);
    void nextStepFSSimple(double time);
    void calculateTimeSteps();

    double getStartTime();
    double sizeTimeSteps();
    int getDrawing();
    void setDrawing(int drawing);

    void publishGoals();
    
    bool startDrawPlanning(ur10bender::drawConfigRequest& req,
                                    ur10bender::drawConfigResponse& res);

    bool initDrawing(std_srvs::TriggerRequest& req,
                                    std_srvs::TriggerResponse& res);

    void GoalReachedCallback(const std_msgs::String& msg);
};


#endif // DRAWING_H