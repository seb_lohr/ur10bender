
#include "../include/Dinamica.h"
#include<tum_ics_ur_robot_msgs/ControlData.h>


bool Dinamica::ini(){
    std::stringstream s;
    Tum::VDouble p;

    s.str("");
    s << "/Bender/simple_effort_ctrl/length";
    ros::param::get(s.str(),p);

    Tum::Vector12d L;

    for(int i=0; i<12; i++) {
        L(i)=p[i];
    }

    L1=L[0];
    L2=L[1];
    L3=L[2];
    L4=L[3];
    L5=L[4];
    L6=L[5];
    L7=L[6];
    L8=L[7];
    L9=L[8];
    L10=L[9];
    L11=L[10];
    L12=L[11];

    s.str("");
    s << "/Bender/simple_effort_ctrl/mass";
    ros::param::get(s.str(),p);

    Tum::Vector6d m;

    for(int i=0; i<6; i++) {
        m(i)=p[i];
    }
    m1=m[0];
    m2=m[1];
    m3=m[2];
    m4=m[3];
    m5=m[4];
    m6=m[5];

    s.str("");
    s << "/Bender/simple_effort_ctrl/inertias1";
    ros::param::get(s.str(),p);
    Tum::Vector6d I1;

    for(int i=0;i<6;i++)
    {
        I1(i)=p[i];
    }

    s.str("");
    s << "/Bender/simple_effort_ctrl/inertias2";
    ros::param::get(s.str(),p);
    Tum::Vector6d I2;
    for(int i=0;i<6;i++)
    {
        I2(i)=p[i];
    }

    s.str("");
    s << "/Bender/simple_effort_ctrl/inertias3";
    ros::param::get(s.str(),p);
    Tum::Vector6d I3;
    for(int i=0;i<6;i++)
    {
        I3(i)=p[i];
    }

    s.str("");
    s << "/Bender/simple_effort_ctrl/inertias4";
    ros::param::get(s.str(),p);
    Tum::Vector6d I4;
    for(int i=0;i<6;i++)
    {
        I4(i)=p[i];
    }

    s.str("");
    s << "/Bender/simple_effort_ctrl/inertias5";
    ros::param::get(s.str(),p);
    Tum::Vector6d I5;
    for(int i=0;i<6;i++)
    {
        I5(i)=p[i];
    }

    s.str("");
    s << "/Bender/simple_effort_ctrl/inertias6";
    ros::param::get(s.str(),p);
    Tum::Vector6d I6;
    for(int i=0;i<6;i++)
    {
        I6(i)=p[i];
    }

    // s.str("");
    // s << "/Bender/AdaptiveCtrl/theta";
    // ros::param::get(s.str(),p);

    // s.str("");
    // s << "/Bender/simple_effort_ctrl/gamma";
    // ros::param::get(s.str(),p);
    // m_gamma = p[0];

    for(int i=0;i<76;i++) {
        m_Theta(i)=p[i];
    }
    m_Theta.setZero();
    iniTheta();
    // Gravity
    double g = 9.81;

    //Gravity Vector
    gx = 0*g;
    gy = 0*g;
    gz = -1*g;

    Tum::Vector3d gvec;
    gvec << gx, gy, gz;

    return true;
}


Tum::Vector6d Dinamica::G(Tum::Vector6d &Q){
    // Joint Position
    double q1 = Q[0];
    double q2 = Q[1];
    double q3 = Q[2];
    double q4 = Q[3];
    double q5 = Q[4];
    // double q6 = Q[5];

    double G1 = L2*gx*m4*cos(q1) + L2*gx*m5*cos(q1) + L2*gx*m6*cos(q1) - L5*gx*m3*cos(q1) - L5*gx*m4*cos(q1) + L7*gx*m2*cos(q1) - L5*gx*m5*cos(q1) + L7*gx*m3*cos(q1) - L5*gx*m6*cos(q1) + L7*gx*m4*cos(q1) + L7*gx*m5*cos(q1) + L7*gx*m6*cos(q1) + L2*gy*m4*sin(q1) + L2*gy*m5*sin(q1) + L2*gy*m6*sin(q1) - L5*gy*m3*sin(q1) - L5*gy*m4*sin(q1) + L7*gy*m2*sin(q1) - L5*gy*m5*sin(q1) + L7*gy*m3*sin(q1) - L5*gy*m6*sin(q1) + L7*gy*m4*sin(q1) + L7*gy*m5*sin(q1) + L7*gy*m6*sin(q1) + L4*gx*m6*cos(q1)*cos(q5) - L3*gy*m3*cos(q1)*cos(q2) - L3*gy*m4*cos(q1)*cos(q2) - L3*gy*m5*cos(q1)*cos(q2) - L3*gy*m6*cos(q1)*cos(q2) - L8*gy*m2*cos(q1)*cos(q2) + L3*gx*m3*cos(q2)*sin(q1) + L3*gx*m4*cos(q2)*sin(q1) + L3*gx*m5*cos(q2)*sin(q1) + L3*gx*m6*cos(q2)*sin(q1) + L8*gx*m2*cos(q2)*sin(q1) + L4*gy*m6*cos(q5)*sin(q1) - L12*gy*m5*sin(q2 + q3 + q4)*cos(q1) - L12*gy*m6*sin(q2 + q3 + q4)*cos(q1) + L12*gx*m5*sin(q2 + q3 + q4)*sin(q1) + L12*gx*m6*sin(q2 + q3 + q4)*sin(q1) + L10*gx*m3*cos(q2)*cos(q3)*sin(q1) + L11*gx*m4*cos(q2)*cos(q3)*sin(q1) + L11*gx*m5*cos(q2)*cos(q3)*sin(q1) + L11*gx*m6*cos(q2)*cos(q3)*sin(q1) + L10*gy*m3*cos(q1)*sin(q2)*sin(q3) + L11*gy*m4*cos(q1)*sin(q2)*sin(q3) + L11*gy*m5*cos(q1)*sin(q2)*sin(q3) + L11*gy*m6*cos(q1)*sin(q2)*sin(q3) - L10*gx*m3*sin(q1)*sin(q2)*sin(q3) - L11*gx*m4*sin(q1)*sin(q2)*sin(q3) - L11*gx*m5*sin(q1)*sin(q2)*sin(q3) - L11*gx*m6*sin(q1)*sin(q2)*sin(q3) + L4*gy*m6*cos(q2 + q3 + q4)*cos(q1)*sin(q5) - L4*gx*m6*cos(q2 + q3 + q4)*sin(q1)*sin(q5) - L10*gy*m3*cos(q1)*cos(q2)*cos(q3) - L11*gy*m4*cos(q1)*cos(q2)*cos(q3) - L11*gy*m5*cos(q1)*cos(q2)*cos(q3) - L11*gy*m6*cos(q1)*cos(q2)*cos(q3);
    double G2 = L3*gx*m3*cos(q1)*sin(q2) - L11*gz*m4*cos(q2 + q3) - L11*gz*m5*cos(q2 + q3) - L11*gz*m6*cos(q2 + q3) - L3*gz*m3*cos(q2) - L3*gz*m4*cos(q2) - L3*gz*m5*cos(q2) - L3*gz*m6*cos(q2) - L8*gz*m2*cos(q2) - L12*gz*m5*sin(q2 + q3 + q4) - L12*gz*m6*sin(q2 + q3 + q4) - L10*gz*m3*cos(q2 + q3) + L3*gx*m4*cos(q1)*sin(q2) + L3*gx*m5*cos(q1)*sin(q2) + L3*gx*m6*cos(q1)*sin(q2) + L8*gx*m2*cos(q1)*sin(q2) + L3*gy*m3*sin(q1)*sin(q2) + L3*gy*m4*sin(q1)*sin(q2) + L3*gy*m5*sin(q1)*sin(q2) + L3*gy*m6*sin(q1)*sin(q2) + L8*gy*m2*sin(q1)*sin(q2) - L12*gx*m5*cos(q2 + q3 + q4)*cos(q1) - L12*gx*m6*cos(q2 + q3 + q4)*cos(q1) - L12*gy*m5*cos(q2 + q3 + q4)*sin(q1) - L12*gy*m6*cos(q2 + q3 + q4)*sin(q1) + L4*gz*m6*cos(q2 + q3 + q4)*sin(q5) + L10*gx*m3*cos(q1)*cos(q2)*sin(q3) + L10*gx*m3*cos(q1)*cos(q3)*sin(q2) + L11*gx*m4*cos(q1)*cos(q2)*sin(q3) + L11*gx*m4*cos(q1)*cos(q3)*sin(q2) + L11*gx*m5*cos(q1)*cos(q2)*sin(q3) + L11*gx*m5*cos(q1)*cos(q3)*sin(q2) + L11*gx*m6*cos(q1)*cos(q2)*sin(q3) + L11*gx*m6*cos(q1)*cos(q3)*sin(q2) + L10*gy*m3*cos(q2)*sin(q1)*sin(q3) + L10*gy*m3*cos(q3)*sin(q1)*sin(q2) + L11*gy*m4*cos(q2)*sin(q1)*sin(q3) + L11*gy*m4*cos(q3)*sin(q1)*sin(q2) + L11*gy*m5*cos(q2)*sin(q1)*sin(q3) + L11*gy*m5*cos(q3)*sin(q1)*sin(q2) + L11*gy*m6*cos(q2)*sin(q1)*sin(q3) + L11*gy*m6*cos(q3)*sin(q1)*sin(q2) - L4*gx*m6*sin(q2 + q3 + q4)*cos(q1)*sin(q5) - L4*gy*m6*sin(q2 + q3 + q4)*sin(q1)*sin(q5);
    double G3 = gx*m3*(L10*cos(q1)*cos(q2)*sin(q3) + L10*cos(q1)*cos(q3)*sin(q2)) + gx*m4*(L11*cos(q1)*cos(q2)*sin(q3) + L11*cos(q1)*cos(q3)*sin(q2)) - gx*m6*(L12*cos(q2 + q3 + q4)*cos(q1) - L11*cos(q1)*cos(q2)*sin(q3) - L11*cos(q1)*cos(q3)*sin(q2) + L4*sin(q2 + q3 + q4)*cos(q1)*sin(q5)) + gy*m3*(L10*cos(q2)*sin(q1)*sin(q3) + L10*cos(q3)*sin(q1)*sin(q2)) + gy*m4*(L11*cos(q2)*sin(q1)*sin(q3) + L11*cos(q3)*sin(q1)*sin(q2)) - gz*m6*(L11*cos(q2 + q3) + L12*sin(q2 + q3 + q4) - L4*cos(q2 + q3 + q4)*sin(q5)) - gy*m6*(L12*cos(q2 + q3 + q4)*sin(q1) - L11*cos(q2)*sin(q1)*sin(q3) - L11*cos(q3)*sin(q1)*sin(q2) + L4*sin(q2 + q3 + q4)*sin(q1)*sin(q5)) + gx*m5*(L11*cos(q1)*cos(q2)*sin(q3) - L12*cos(q2 + q3 + q4)*cos(q1) + L11*cos(q1)*cos(q3)*sin(q2)) - gz*m5*(L11*cos(q2 + q3) + L12*sin(q2 + q3 + q4)) + gy*m5*(L11*cos(q2)*sin(q1)*sin(q3) - L12*cos(q2 + q3 + q4)*sin(q1) + L11*cos(q3)*sin(q1)*sin(q2)) - L10*gz*m3*cos(q2 + q3) - L11*gz*m4*cos(q2 + q3);
    double G4 = L4*gz*m6*cos(q2 + q3 + q4)*sin(q5) - L12*gz*m6*sin(q2 + q3 + q4) - gx*m6*cos(q1)*(L12*cos(q2 + q3 + q4) + L4*sin(q2 + q3 + q4)*sin(q5)) - gy*m6*sin(q1)*(L12*cos(q2 + q3 + q4) + L4*sin(q2 + q3 + q4)*sin(q5)) - L12*gx*m5*cos(q2 + q3 + q4)*cos(q1) - L12*gy*m5*cos(q2 + q3 + q4)*sin(q1) - L12*gz*m5*sin(q2 + q3 + q4);
    double G5 = L4*gy*m6*(cos(q1)*sin(q5) + cos(q2 + q3 + q4)*cos(q5)*sin(q1)) - L4*gx*m6*(sin(q1)*sin(q5) - cos(q2 + q3 + q4)*cos(q1)*cos(q5)) + L4*gz*m6*sin(q2 + q3 + q4)*cos(q5);
    double G6 = 0;

    Tum::Vector6d G;
    G << G1,G2,G3,G4,G5,G6;

    return G;
}

// returns current cartesian Position of endeffektor in euler angles
Tum::Vector6d Dinamica::FKPosition(Tum::Vector6d Q){
    // Joint Position
    double q1 = Q[0];
    double q2 = Q[1];
    double q3 = Q[2];
    double q4 = Q[3];
    double q5 = Q[4];
    double q6 = Q[5];
    // double theta;
    // double theta1;
    // double theta2;
    // double psi;
    // double psi1;
    // double psi2;
    // double phi;
    // double phi1;
    // double phi2;

    // Matrix entries
    double Hef11 = cos(q6)*(sin(q1)*sin(q5) + cos(q2 + q3 + q4)*cos(q1)*cos(q5)) - sin(q2 + q3 + q4)*cos(q1)*sin(q6);
    double Hef12 = - sin(q6)*(sin(q1)*sin(q5) + cos(q2 + q3 + q4)*cos(q1)*cos(q5)) - sin(q2 + q3 + q4)*cos(q1)*cos(q6);
    double Hef13 = cos(q5)*sin(q1) - cos(q2 + q3 + q4)*cos(q1)*sin(q5);
    double Hef14 = L4*(cos(q5)*sin(q1) - cos(q2 + q3 + q4)*cos(q1)*sin(q5)) - cos(q1)*(L11*cos(q2 + q3) + L3*cos(q2)) + L2*sin(q1) + L12*sin(q2 + q3 + q4)*cos(q1);

    double Hef21 = - cos(q6)*(cos(q1)*sin(q5) - cos(q2 + q3 + q4)*cos(q5)*sin(q1)) - sin(q2 + q3 + q4)*sin(q1)*sin(q6);
    double Hef22 = sin(q6)*(cos(q1)*sin(q5) - cos(q2 + q3 + q4)*cos(q5)*sin(q1)) - sin(q2 + q3 + q4)*cos(q6)*sin(q1);
    double Hef23 = - cos(q1)*cos(q5) - cos(q2 + q3 + q4)*sin(q1)*sin(q5);
    double Hef24 = L12*sin(q2 + q3 + q4)*sin(q1) - sin(q1)*(L11*cos(q2 + q3) + L3*cos(q2)) - L2*cos(q1) - L4*(cos(q1)*cos(q5) + cos(q2 + q3 + q4)*sin(q1)*sin(q5));

    double Hef31 = cos(q2 + q3 + q4)*sin(q6) + sin(q2 + q3 + q4)*cos(q5)*cos(q6);
    double Hef32 = cos(q2 + q3 + q4)*cos(q6) - sin(q2 + q3 + q4)*cos(q5)*sin(q6);
    double Hef33 = -sin(q2 + q3 + q4)*sin(q5);
    double Hef34 = L1 - L11*sin(q2 + q3) - L3*sin(q2) - L12*cos(q2 + q3 + q4) - L4*sin(q2 + q3 + q4)*sin(q5);


    // double Hef41 = 0;
    // double Hef42 = 0;
    // double Hef43 = 0;
    // double Hef44 = 1;

    Tum::Matrix3d Ref;
    Ref << Hef11, Hef12, Hef13,
          Hef21, Hef22, Hef23,
          Hef31, Hef32, Hef33;

    // save orientation of EF as rotation
    // m_RotEF = Ref;

    Tum::Vector3d angles;
    angles = Ref.eulerAngles(0, 1, 2);

    //  postion endeffector
    Tum::Vector6d pos;
    pos <<  Hef14, Hef24, Hef34, angles;
    return pos;
}

Eigen::Matrix3d Dinamica::rotationEF(Tum::Vector6d Q) {
    // Joint Position
    double q1 = Q[0];
    double q2 = Q[1];
    double q3 = Q[2];
    double q4 = Q[3];
    double q5 = Q[4];
    double q6 = Q[5];
    // double theta;
    // double theta1;
    // double theta2;
    // double psi;
    // double psi1;
    // double psi2;
    // double phi;
    // double phi1;
    // double phi2;

    // Matrix entries
    double Hef11 = cos(q6)*(sin(q1)*sin(q5) + cos(q2 + q3 + q4)*cos(q1)*cos(q5)) - sin(q2 + q3 + q4)*cos(q1)*sin(q6);
    double Hef12 = - sin(q6)*(sin(q1)*sin(q5) + cos(q2 + q3 + q4)*cos(q1)*cos(q5)) - sin(q2 + q3 + q4)*cos(q1)*cos(q6);
    double Hef13 = cos(q5)*sin(q1) - cos(q2 + q3 + q4)*cos(q1)*sin(q5);

    double Hef21 = - cos(q6)*(cos(q1)*sin(q5) - cos(q2 + q3 + q4)*cos(q5)*sin(q1)) - sin(q2 + q3 + q4)*sin(q1)*sin(q6);
    double Hef22 = sin(q6)*(cos(q1)*sin(q5) - cos(q2 + q3 + q4)*cos(q5)*sin(q1)) - sin(q2 + q3 + q4)*cos(q6)*sin(q1);
    double Hef23 = - cos(q1)*cos(q5) - cos(q2 + q3 + q4)*sin(q1)*sin(q5);

    double Hef31 = cos(q2 + q3 + q4)*sin(q6) + sin(q2 + q3 + q4)*cos(q5)*cos(q6);
    double Hef32 = cos(q2 + q3 + q4)*cos(q6) - sin(q2 + q3 + q4)*cos(q5)*sin(q6);
    double Hef33 = -sin(q2 + q3 + q4)*sin(q5);
    // double Hef34 = L1 - L11*sin(q2 + q3) - L3*sin(q2) + L12*cos(q2 + q3 + q4) + L4*sin(q2 + q3 + q4)*sin(q5);

    // double Hef41 = 0;
    // double Hef42 = 0;
    // double Hef43 = 0;
    // double Hef44 = 1;

    Tum::Matrix3d Ref;
    Ref << Hef11, Hef12, Hef13,
          Hef21, Hef22, Hef23,
          Hef31, Hef32, Hef33;
    return Ref;
}


Tum::Matrix6d Dinamica::JacobianEF(Tum::Vector6d Q){
    // Joint Position
    double q1 = Q[0];
    double q2 = Q[1];
    double q3 = Q[2];
    double q4 = Q[3];
    double q5 = Q[4];
    // double q6 = Q[5];

    // Matrix entries
    double Jef11 = L4*(cos(q1)*cos(q5) + cos(q2 + q3 + q4)*sin(q1)*sin(q5)) + sin(q1)*(L11*cos(q2 + q3) + L3*cos(q2)) + L2*cos(q1) - L12*sin(q2 + q3 + q4)*sin(q1);
    double Jef12 = cos(q1)*(L11*sin(q2 + q3) + L3*sin(q2) + L12*cos(q2 + q3 + q4) + L4*sin(q2 + q3 + q4)*sin(q5));
    double Jef13 = cos(q1)*(L11*sin(q2 + q3) + L12*cos(q2 + q3 + q4) + L4*sin(q2 + q3 + q4)*sin(q5));
    double Jef14 = cos(q1)*(L12*cos(q2 + q3 + q4) + L4*sin(q2 + q3 + q4)*sin(q5));
    double Jef15 = -L4*(sin(q1)*sin(q5) + cos(q2 + q3 + q4)*cos(q1)*cos(q5));
    double Jef16 = 0;

    double Jef21 = L4*(cos(q5)*sin(q1) - cos(q2 + q3 + q4)*cos(q1)*sin(q5)) - cos(q1)*(L11*cos(q2 + q3) + L3*cos(q2)) + L2*sin(q1) + L12*sin(q2 + q3 + q4)*cos(q1);
    double Jef22 = sin(q1)*(L11*sin(q2 + q3) + L3*sin(q2) + L12*cos(q2 + q3 + q4) + L4*sin(q2 + q3 + q4)*sin(q5));
    double Jef23 = sin(q1)*(L11*sin(q2 + q3) + L12*cos(q2 + q3 + q4) + L4*sin(q2 + q3 + q4)*sin(q5));
    double Jef24 = sin(q1)*(L12*cos(q2 + q3 + q4) + L4*sin(q2 + q3 + q4)*sin(q5));
    double Jef25 = L4*(cos(q1)*sin(q5) - cos(q2 + q3 + q4)*cos(q5)*sin(q1));
    double Jef26 = 0;

    double Jef31 = 0;
    double Jef32 = (L4*sin(q2 + q3 + q4 - q5))/2 - L11*cos(q2 + q3) - L3*cos(q2) - (L4*sin(q2 + q3 + q4 + q5))/2 + L12*sin(q2 + q3 + q4);
    double Jef33 = (L4*sin(q2 + q3 + q4 - q5))/2 - L11*cos(q2 + q3) - (L4*sin(q2 + q3 + q4 + q5))/2 + L12*sin(q2 + q3 + q4);
    double Jef34 = L12*sin(q2 + q3 + q4) - L4*cos(q2 + q3 + q4)*sin(q5);
    double Jef35 = -L4*sin(q2 + q3 + q4)*cos(q5);
    double Jef36 = 0;

    double Jef41 = 0;
    double Jef42 = sin(q1);
    double Jef43 = sin(q1);
    double Jef44 = sin(q1);
    double Jef45 = sin(q2 + q3 + q4)*cos(q1);
    double Jef46 = cos(q5)*sin(q1) - cos(q2 + q3 + q4)*cos(q1)*sin(q5);

    double Jef51 = 0;
    double Jef52 = -cos(q1);
    double Jef53 = -cos(q1);
    double Jef54 = -cos(q1);
    double Jef55 = sin(q2 + q3 + q4)*sin(q1);
    double Jef56 = - cos(q1)*cos(q5) - cos(q2 + q3 + q4)*sin(q1)*sin(q5);

    double Jef61 = 1;
    double Jef62 = 0;
    double Jef63 = 0;
    double Jef64 = 0;
    double Jef65 = -cos(q2 + q3 + q4);
    double Jef66 = -sin(q2 + q3 + q4)*sin(q5);



    m_Jef << Jef11, Jef12, Jef13, Jef14, Jef15, Jef16,
            Jef21, Jef22, Jef23, Jef24, Jef25, Jef26,
            Jef31, Jef32, Jef33, Jef34, Jef35, Jef36,
            Jef41, Jef42, Jef43, Jef44, Jef45, Jef46,
            Jef51, Jef52, Jef53, Jef54, Jef55, Jef56,
            Jef61, Jef62, Jef63, Jef64, Jef65, Jef66;

    return m_Jef;
}

Tum::Matrix6d Dinamica::JacobianDotEF(Tum::Vector6d Q, Tum::Vector6d Q_p){

    double q1 = Q[0];
    double q2 = Q[1];
    double q3 = Q[2];
    double q4 = Q[3];
    double q5 = Q[4];
    // double q6 = Q[5];

    double q1p = Q_p[0];
    double q2p = Q_p[1];
    // double q3p = Q_p[2];
    double q4p = Q_p[3];
    double q5p = Q_p[4];
    // double q6p = Q_p[5];

    Tum::Matrix6d Jef_dot;

    double Jef_dot11 = q4p*(L12*cos(q2 + q3 + q4)*sin(q1) + L4*sin(q2 + q3 + q4)*sin(q1)*sin(q5)) - 2*q2p*(L3*sin(q1)*sin(q2) - L12*cos(q2 + q3 + q4)*sin(q1) + L11*cos(q2)*sin(q1)*sin(q3) + L11*cos(q3)*sin(q1)*sin(q2) - L4*sin(q2 + q3 + q4)*sin(q1)*sin(q5)) - q1p*(L4*(cos(q5)*sin(q1) + cos(q2 + q3 + q4)*cos(q1)*sin(q5)) + L2*sin(q1) - L5*sin(q1) + L7*sin(q1) - L3*cos(q1)*cos(q2) - L12*sin(q2 + q3 + q4)*cos(q1) - L11*cos(q1)*cos(q2)*cos(q3) + L11*cos(q1)*sin(q2)*sin(q3)) - L4*q5p*(cos(q1)*sin(q5) + cos(q2 + q3 + q4)*cos(q5)*sin(q1));
    double Jef_dot12 = 2*q2p*cos(q1)*(L11*cos(q2 + q3) + L3*cos(q2) + L12*sin(q2 + q3 + q4) - L4*cos(q2 + q3 + q4)*sin(q5)) + q4p*cos(q1)*(L12*sin(q2 + q3 + q4) - L4*cos(q2 + q3 + q4)*sin(q5)) - q1p*sin(q1)*(L11*sin(q2 + q3) + L3*sin(q2) - L12*cos(q2 + q3 + q4) - L4*sin(q2 + q3 + q4)*sin(q5)) - L4*q5p*sin(q2 + q3 + q4)*cos(q1)*cos(q5);
    double Jef_dot13 = q4p*cos(q1)*(L12*sin(q2 + q3 + q4) - L4*cos(q2 + q3 + q4)*sin(q5)) + 2*q2p*cos(q1)*(L11*cos(q2 + q3) + L12*sin(q2 + q3 + q4) - L4*cos(q2 + q3 + q4)*sin(q5)) + q1p*sin(q1)*(L12*cos(q2 + q3 + q4) - L11*sin(q2 + q3) + L4*sin(q2 + q3 + q4)*sin(q5)) - L4*q5p*sin(q2 + q3 + q4)*cos(q1)*cos(q5);
    double Jef_dot14 = 2*q2p*cos(q1)*(L12*sin(q2 + q3 + q4) - L4*cos(q2 + q3 + q4)*sin(q5)) + q4p*cos(q1)*(L12*sin(q2 + q3 + q4) - L4*cos(q2 + q3 + q4)*sin(q5)) + q1p*sin(q1)*(L12*cos(q2 + q3 + q4) + L4*sin(q2 + q3 + q4)*sin(q5)) - L4*q5p*sin(q2 + q3 + q4)*cos(q1)*cos(q5);
    double Jef_dot15 = - q1p*(L4*cos(q1)*sin(q5) + L4*cos(q2 + q3 + q4)*cos(q5)*sin(q1)) - q5p*(L4*cos(q5)*sin(q1) + L4*cos(q2 + q3 + q4)*cos(q1)*sin(q5)) - 2*L4*q2p*sin(q2 + q3 + q4)*cos(q1)*cos(q5) - L4*q4p*sin(q2 + q3 + q4)*cos(q1)*cos(q5);
    double Jef_dot16 = 0;

    double Jef_dot21 = 2*q2p*(L3*cos(q1)*sin(q2) - L12*cos(q2 + q3 + q4)*cos(q1) + L11*cos(q1)*cos(q2)*sin(q3) + L11*cos(q1)*cos(q3)*sin(q2) - L4*sin(q2 + q3 + q4)*cos(q1)*sin(q5)) - q4p*(L12*cos(q2 + q3 + q4)*cos(q1) + L4*sin(q2 + q3 + q4)*cos(q1)*sin(q5)) + q1p*(L4*(cos(q1)*cos(q5) - cos(q2 + q3 + q4)*sin(q1)*sin(q5)) + L2*cos(q1) - L5*cos(q1) + L7*cos(q1) + L3*cos(q2)*sin(q1) + L12*sin(q2 + q3 + q4)*sin(q1) + L11*cos(q2)*cos(q3)*sin(q1) - L11*sin(q1)*sin(q2)*sin(q3)) - L4*q5p*(sin(q1)*sin(q5) - cos(q2 + q3 + q4)*cos(q1)*cos(q5));
    double Jef_dot22 = 2*q2p*sin(q1)*(L11*cos(q2 + q3) + L3*cos(q2) + L12*sin(q2 + q3 + q4) - L4*cos(q2 + q3 + q4)*sin(q5)) + q1p*cos(q1)*(L11*sin(q2 + q3) + L3*sin(q2) - L12*cos(q2 + q3 + q4) - L4*sin(q2 + q3 + q4)*sin(q5)) + q4p*sin(q1)*(L12*sin(q2 + q3 + q4) - L4*cos(q2 + q3 + q4)*sin(q5)) - L4*q5p*sin(q2 + q3 + q4)*cos(q5)*sin(q1);
    double Jef_dot23 = q4p*sin(q1)*(L12*sin(q2 + q3 + q4) - L4*cos(q2 + q3 + q4)*sin(q5)) - q1p*cos(q1)*(L12*cos(q2 + q3 + q4) - L11*sin(q2 + q3) + L4*sin(q2 + q3 + q4)*sin(q5)) + 2*q2p*sin(q1)*(L11*cos(q2 + q3) + L12*sin(q2 + q3 + q4) - L4*cos(q2 + q3 + q4)*sin(q5)) - L4*q5p*sin(q2 + q3 + q4)*cos(q5)*sin(q1);
    double Jef_dot24 = 2*q2p*sin(q1)*(L12*sin(q2 + q3 + q4) - L4*cos(q2 + q3 + q4)*sin(q5)) - q1p*cos(q1)*(L12*cos(q2 + q3 + q4) + L4*sin(q2 + q3 + q4)*sin(q5)) + q4p*sin(q1)*(L12*sin(q2 + q3 + q4) - L4*cos(q2 + q3 + q4)*sin(q5)) - L4*q5p*sin(q2 + q3 + q4)*cos(q5)*sin(q1);
    double Jef_dot25 = L4*q5p*(cos(q1)*cos(q5) - cos(q2 + q3 + q4)*sin(q1)*sin(q5)) - L4*q1p*(sin(q1)*sin(q5) - cos(q2 + q3 + q4)*cos(q1)*cos(q5)) - 2*L4*q2p*sin(q2 + q3 + q4)*cos(q5)*sin(q1) - L4*q4p*sin(q2 + q3 + q4)*cos(q5)*sin(q1);
    double Jef_dot26 = 0;

    double Jef_dot31 = 0;
    double Jef_dot32 = 2*q2p*((L4*cos(q2 + q3 + q4 + q5))/2 + L11*sin(q2 + q3) + L3*sin(q2) - (L4*cos(q2 + q3 + q4 - q5))/2 - L12*cos(q2 + q3 + q4)) - q4p*((L4*cos(q2 + q3 + q4 - q5))/2 - (L4*cos(q2 + q3 + q4 + q5))/2 + L12*cos(q2 + q3 + q4)) + q5p*((L4*cos(q2 + q3 + q4 + q5))/2 + (L4*cos(q2 + q3 + q4 - q5))/2);
    double Jef_dot33 = 2*q2p*((L4*cos(q2 + q3 + q4 + q5))/2 + L11*sin(q2 + q3) - (L4*cos(q2 + q3 + q4 - q5))/2 - L12*cos(q2 + q3 + q4)) - q4p*((L4*cos(q2 + q3 + q4 - q5))/2 - (L4*cos(q2 + q3 + q4 + q5))/2 + L12*cos(q2 + q3 + q4)) + q5p*((L4*cos(q2 + q3 + q4 + q5))/2 + (L4*cos(q2 + q3 + q4 - q5))/2);
    double Jef_dot34 = L4*q5p*cos(q2 + q3 + q4)*cos(q5) - q4p*(L12*cos(q2 + q3 + q4) + L4*sin(q2 + q3 + q4)*sin(q5)) - 2*q2p*(L12*cos(q2 + q3 + q4) + L4*sin(q2 + q3 + q4)*sin(q5));
    double Jef_dot35 = 2*L4*q2p*cos(q2 + q3 + q4)*cos(q5) + L4*q4p*cos(q2 + q3 + q4)*cos(q5) - L4*q5p*sin(q2 + q3 + q4)*sin(q5);
    double Jef_dot36 = 0;

    double Jef_dot41 = 0;
    double Jef_dot42 = q1p*cos(q1);
    double Jef_dot43 = q1p*cos(q1);
    double Jef_dot44 = q1p*cos(q1);
    double Jef_dot45 = q1p*sin(q2 + q3 + q4)*sin(q1) - q4p*cos(q2 + q3 + q4)*cos(q1) - 2*q2p*cos(q2 + q3 + q4)*cos(q1);
    double Jef_dot46 = q1p*(cos(q1)*cos(q5) - cos(q2 + q3 + q4)*sin(q1)*sin(q5)) - q5p*(sin(q1)*sin(q5) - cos(q2 + q3 + q4)*cos(q1)*cos(q5)) - 2*q2p*sin(q2 + q3 + q4)*cos(q1)*sin(q5) - q4p*sin(q2 + q3 + q4)*cos(q1)*sin(q5);

    double Jef_dot51 = 0;
    double Jef_dot52 = q1p*sin(q1);
    double Jef_dot53 = q1p*sin(q1);
    double Jef_dot54 = q1p*sin(q1);
    double Jef_dot55 = - q1p*sin(q2 + q3 + q4)*cos(q1) - 2*q2p*cos(q2 + q3 + q4)*sin(q1) - q4p*cos(q2 + q3 + q4)*sin(q1);
    double Jef_dot56 = q1p*(cos(q5)*sin(q1) + cos(q2 + q3 + q4)*cos(q1)*sin(q5)) + q5p*(cos(q1)*sin(q5) + cos(q2 + q3 + q4)*cos(q5)*sin(q1)) - 2*q2p*sin(q2 + q3 + q4)*sin(q1)*sin(q5) - q4p*sin(q2 + q3 + q4)*sin(q1)*sin(q5);

    double Jef_dot61 = 0;
    double Jef_dot62 = 0;
    double Jef_dot63 = 0;
    double Jef_dot64 = 0;
    double Jef_dot65 = - 2*q2p*sin(q2 + q3 + q4) - q4p*sin(q2 + q3 + q4);
    double Jef_dot66 = 2*q2p*cos(q2 + q3 +  q4)*sin(q5) + q4p*cos(q2 + q3 + q4)*sin(q5) + q5p*sin(q2 + q3 + q4)*cos(q5);



    Jef_dot <<  Jef_dot11, Jef_dot12, Jef_dot13, Jef_dot14, Jef_dot15, Jef_dot16,
                Jef_dot21, Jef_dot22, Jef_dot23, Jef_dot24, Jef_dot25, Jef_dot26,
                Jef_dot31, Jef_dot32, Jef_dot33, Jef_dot34, Jef_dot35, Jef_dot36,
                Jef_dot41, Jef_dot42, Jef_dot43, Jef_dot44, Jef_dot45, Jef_dot46,
                Jef_dot51, Jef_dot52, Jef_dot53, Jef_dot54, Jef_dot55, Jef_dot56,
                Jef_dot61, Jef_dot62, Jef_dot63, Jef_dot64, Jef_dot65, Jef_dot66;

    return Jef_dot;

}



Tum::Vector6d Dinamica::JacobianPhi(Tum::Vector6d Q){

    // double q1 = Q[0];
    double q2 = Q[1];
    double q3 = Q[2];
    double q4 = Q[3];
    double q5 = Q[4];

    double J_phi1 = 0;
    double J_phi2 = L12*sin(q2 + q3 + q4) - L3*cos(q2) - L11*cos(q2 + q3) - L4*cos(q2 + q3 + q4)*sin(q5);
    double J_phi3 = L12*sin(q2 + q3 + q4) - L11*cos(q2 + q3) - L4*cos(q2 + q3 + q4)*sin(q5);
    double J_phi4 = L12*sin(q2 + q3 + q4) - L4*cos(q2 + q3 + q4)*sin(q5);
    double J_phi5 = -L4*sin(q2 + q3 + q4)*cos(q5);
    double J_phi6 = 0;


    Tum::Vector6d J_phi;

    J_phi << J_phi1, J_phi2, J_phi3, J_phi4, J_phi5, J_phi6;

    return J_phi;
}

Tum::Vector6d Dinamica::JacobianPhiDot(Tum::Vector6d Q, Tum::Vector6d Qp) {

    double q1 = Q[0];
    double q2 = Q[1];
    double q3 = Q[2];
    double q4 = Q[3];
    double q5 = Q[4];

    double q1p = Qp[0];
    double q2p = Qp[1];
    // double q3p = Qp[2];
    double q4p = Qp[3];
    double q5p = Qp[4];

    double J_phi_dot1 = q1p*(L4*(cos(q1)*cos(q5) + cos(q2 + q3 + q4)*sin(q1)*sin(q5)) + sin(q1)*(L11*cos(q2 + q3) + L3*cos(q2)) + L2*cos(q1) - L12*sin(q2 + q3 + q4)*sin(q1)) + q4p*(L12*cos(q2 + q3 + q4)*cos(q1) + L4*sin(q2 + q3 + q4)*cos(q1)*sin(q5)) + 2*q2p*(cos(q1)*(L11*sin(q2 + q3) + L3*sin(q2)) + L12*cos(q2 + q3 + q4)*cos(q1) + L4*sin(q2 + q3 + q4)*cos(q1)*sin(q5)) - L4*q5p*(sin(q1)*sin(q5) + cos(q2 + q3 + q4)*cos(q1)*cos(q5));
    double J_phi_dot2 = 2*q2p*sin(q1)*(L11*cos(q2 + q3) + L3*cos(q2) - L12*sin(q2 + q3 + q4) + L4*cos(q2 + q3 + q4)*sin(q5)) + q1p*cos(q1)*(L11*sin(q2 + q3) + L3*sin(q2) + L12*cos(q2 + q3 + q4) + L4*sin(q2 + q3 + q4)*sin(q5)) - q4p*sin(q1)*(L12*sin(q2 + q3 + q4) - L4*cos(q2 + q3 + q4)*sin(q5)) + L4*q5p*sin(q2 + q3 + q4)*cos(q5)*sin(q1);
    double J_phi_dot3 = q1p*(L11*sin(q2 + q3)*cos(q1) + L12*cos(q2 + q3 + q4)*cos(q1) + L4*sin(q2 + q3 + q4)*cos(q1)*sin(q5)) - q4p*(L12*sin(q2 + q3 + q4)*sin(q1) - L4*cos(q2 + q3 + q4)*sin(q1)*sin(q5)) + 2*q2p*(L11*cos(q2 + q3)*sin(q1) - L12*sin(q2 + q3 + q4)*sin(q1) + L4*cos(q2 + q3 + q4)*sin(q1)*sin(q5)) + L4*q5p*sin(q2 + q3 + q4)*cos(q5)*sin(q1);
    double J_phi_dot4 = q1p*(L12*cos(q2 + q3 + q4)*cos(q1) + L4*sin(q2 + q3 + q4)*cos(q1)*sin(q5)) - 2*q2p*(L12*sin(q2 + q3 + q4)*sin(q1) - L4*cos(q2 + q3 + q4)*sin(q1)*sin(q5)) - q4p*(L12*sin(q2 + q3 + q4)*sin(q1) - L4*cos(q2 + q3 + q4)*sin(q1)*sin(q5)) + L4*q5p*sin(q2 + q3 + q4)*cos(q5)*sin(q1);
    double J_phi_dot5 = L4*q5p*(cos(q1)*cos(q5) + cos(q2 + q3 + q4)*sin(q1)*sin(q5)) - L4*q1p*(sin(q1)*sin(q5) + cos(q2 + q3 + q4)*cos(q1)*cos(q5)) + 2*L4*q2p*sin(q2 + q3 + q4)*cos(q5)*sin(q1) + L4*q4p*sin(q2 + q3 + q4)*cos(q5)*sin(q1);
    double J_phi_dot6 = 0;



    Tum::Vector6d J_phi_dot;

    J_phi_dot << J_phi_dot1, J_phi_dot2, J_phi_dot3, J_phi_dot4, J_phi_dot5, J_phi_dot6;

    return J_phi_dot;

}




My::VectorRegressor Dinamica::getTheta() {

    return m_Theta;
}


void Dinamica::printTheta(){
    std::ofstream yamlprint;
    yamlprint.open("/home/no1/ros/workspaces/ur10bender_ws/src/ur10/launch/configs/ur10bender_theta.yaml");
    yamlprint << "Simple_effort_ctrl:\n\n";
    yamlprint << " Theta: [";
    for (int i=0; i<75; i++){
      yamlprint << m_Theta[i] << ", ";
    }
    yamlprint << m_Theta[75];
    yamlprint << "]\n";
    yamlprint.close();
    ROS_INFO_STREAM("Printed to file\n");
}

Tum::Vector6d Dinamica::FKVelocity(Tum::Vector6d Q_p){
    Tum::Vector6d vel;
    vel = m_Jef*Q_p;
    return vel;
}

Tum::Vector6d Dinamica::JefInversTimes(Tum::Vector6d &Vector){
    Tum::Vector6d v;

    v = m_Jef.inverse()*Vector;
    return v;
}
