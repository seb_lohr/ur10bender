// SYSTEM DEPENDENCIES //
#include<ros/ros.h>
#include <boost/algorithm/string.hpp>

// MSG HEADERS //
#include"ur10bender/GoalQ.h"
#include "find_goal/Shape.h"
#include "find_goal/ShapeRequest.h"
#include "find_goal/ShapeResponse.h"

// PACKAGE LIBS //
#include <../include/shapeDetection.h>


#define STD_DOF 6

int main(int argc, char **argv) {
    ros::init(argc, argv, "find_goal_node");
    ros::NodeHandle n;

    // get arguments
    std::string calibrate = argv[1];

    // register goal publisher
    // ros::Publisher goal_pub = n.advertise<ur10bender::GoalQ>("goal_pub", 1000);
    ros::Rate loop_rate(10);

    // register service

    // instance of shape detection class
    shapeDetection::shapeDetection shapeDetect;

    shapeDetect.init();

    ros::init(argc,argv,"findGoal",ros::init_options::AnonymousName);

    ros::Rate r(0.3);
    std::string goal_in;

    ROS_INFO_STREAM("Start the detection loop");

    while(ros::ok()) {
        ros::spinOnce();

        r.sleep();

        if (calibrate == "true") {
            ROS_INFO_STREAM("Let's calibrate.");
            // features need binaryImg image
            cv::Mat binary_img;
            cv::cvtColor(shapeDetect.getImage(), binary_img, CV_BGR2GRAY);
            if (cv::countNonZero(binary_img) > 1) {
                shapeDetect.getExtrinsics(binary_img);
                break;
            } else {
                ROS_INFO_STREAM("Wait for image...");
            }
        } else {
            // display image with shapes: true
            bool display = true;
            std::vector<cv::Point> shape = shapeDetect.getShape(display);
            if (shape.size() == 0) {
                ROS_INFO_STREAM("not yet a shape discovered");
                continue;
            }
            std::vector<cv::Point3d> shape_world = shapeDetect.getWorldLocations(shape);
            
            // set those shape points as the desired shape coordinates
            shapeDetect.setShapeLocation(shape_world);

        }
        ros::spinOnce();

    }

}