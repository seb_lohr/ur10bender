#ifndef DINAMICA_H
#define DINAMICA_H

#include <fstream>
#include<tum_ics_ur_robot_lli/RobotControllers/ControlEffort.h>
#include"matrixdefs.h"


// using Container = Eigen::Matrix<double, 94, 1>;

class Dinamica{
    // Member variables
private:
    double L1;
    double L2;
    double L3;
    double L4;
    double L5;
    double L6;
    double L7;
    double L8;
    double L9;
    double L10;
    double L11;
    double L12;

    double m1;
    double m2;
    double m3;
    double m4;
    double m5;
    double m6;

    double gx;
    double gy;
    double gz;


    My::VectorRegressor m_Theta;

    // double m_gamma;
    Tum::Matrix6d Beta;
    Tum::Matrix6d m_Jef;

  // Member methods

  public:

    Tum::Vector6d FKPosition(Tum::Vector6d Q);
    Eigen::Matrix3d rotationEF(Tum::Vector6d Q);
    Tum::Vector6d FKVelocity(Tum::Vector6d Q_p);
    Tum::Matrix6d JacobianEF(Tum::Vector6d Q);
    Tum::Matrix6d JacobianDotEF(Tum::Vector6d Q, Tum::Vector6d Q_p);
    Tum::Vector6d JefInversTimes(Tum::Vector6d &Vector);
    Tum::Vector6d G(Tum::Vector6d &Q);
    bool ini();

    Tum::Vector6d JacobianPhi(Tum::Vector6d Q);
    Tum::Vector6d JacobianPhiDot(Tum::Vector6d Q, Tum::Vector6d Qp);

    My::MatrixRegressor CalculateYr(Tum::Vector6d Q, Tum::Vector6d Qp, Tum::Vector6d Qp_r, Tum::Vector6d Qpp_r);
    void CalculateTheta(Tum::Vector6d q, Tum::Vector6d q_p, Tum::Vector6d qr_p, Tum::Vector6d qr_pp, Tum::Vector6d Sq, double dt, double gamma);
    void iniTheta();
    void zeroTheta();

    My::VectorRegressor getTheta();
    void printTheta();

  // Memeber methods
  private:
};


#endif // DINAMICA_H
