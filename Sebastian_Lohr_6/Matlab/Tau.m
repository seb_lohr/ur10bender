function [ tau ] = Tau( u )
%TAU Summary of this function goes here
%   Detailed explanation goes here
% Times are defined in TrajGen.m file
global t0 t1 t2 t3 t4 t5 t6 singularity t_s

persistent Qp_r_old Xc R Z DeltaX_old DeltaQ_old
% t0 = 0;
% t1 = 3.5;
% t2 = 8;
% t3 = 17;


t=u(1);

dt = str2double(get_param('DSimulator_robot3GDL','FixedStep'));

q1d=u(2);
q2d=u(3);
q3d=u(4);

q1dp=u(5);
q2dp=u(6);
q3dp=u(7);

q1dpp=u(8);
q2dpp=u(9);
q3dpp=u(10);

Kd=diag([u(11);u(12);u(13)]);
Kp=diag([u(14);u(15);u(16)]);

q1=u(17);
q2=u(18);
q3=u(19);

q1p=u(20);
q2p=u(21);
q3p=u(22);

Q=[q1;q2;q3];
Qp=[q1p;q2p;q3p];

Qd=[q1d;q2d;q3d];
Qdp=[q1dp;q2dp;q3dp];
Qdpp=[q1dpp;q2dpp;q3dpp];


%Robot Parameters

m1=u(23);
m2=u(24);
m3=u(25);

g=u(26);

L1=u(27);
L2=u(28);
L4=u(29);
L6=u(30);
L7=u(31);
L9=u(32);
L3=u(33);
L5=u(34);double G1 = L2*gx*m4*cos(q1) + L2*gx*m5*cos(q1) + L2*gx*m6*cos(q1) - L5*gx*m3*cos(q1) - L5*gx*m4*cos(q1) + L7*gx*m2*cos(q1) - L5*gx*m5*cos(q1) + L7*gx*m3*cos(q1) - L5*gx*m6*cos(q1) + L7*gx*m4*cos(q1) + L7*gx*m5*cos(q1) + L7*gx*m6*cos(q1) + L2*gy*m4*sin(q1) + L2*gy*m5*sin(q1) + L2*gy*m6*sin(q1) - L5*gy*m3*sin(q1) - L5*gy*m4*sin(q1) + L7*gy*m2*sin(q1) - L5*gy*m5*sin(q1) + L7*gy*m3*sin(q1) - L5*gy*m6*sin(q1) + L7*gy*m4*sin(q1) + L7*gy*m5*sin(q1) + L7*gy*m6*sin(q1) + L4*gx*m6*cos(q1)*cos(q5) - L3*gy*m3*cos(q1)*cos(q2) - L3*gy*m4*cos(q1)*cos(q2) - L3*gy*m5*cos(q1)*cos(q2) - L3*gy*m6*cos(q1)*cos(q2) - L8*gy*m2*cos(q1)*cos(q2) + L3*gx*m3*cos(q2)*sin(q1) + L3*gx*m4*cos(q2)*sin(q1) + L3*gx*m5*cos(q2)*sin(q1) + L3*gx*m6*cos(q2)*sin(q1) + L8*gx*m2*cos(q2)*sin(q1) + L4*gy*m6*cos(q5)*sin(q1) - L12*gy*m5*sin(q2 - q3 + q4)*cos(q1) - L12*gy*m6*sin(q2 - q3 + q4)*cos(q1) + L12*gx*m5*sin(q2 - q3 + q4)*sin(q1) + L12*gx*m6*sin(q2 - q3 + q4)*sin(q1) + L10*gx*m3*cos(q2)*cos(q3)*sin(q1) + L11*gx*m4*cos(q2)*cos(q3)*sin(q1) + L11*gx*m5*cos(q2)*cos(q3)*sin(q1) + L11*gx*m6*cos(q2)*cos(q3)*sin(q1) - L10*gy*m3*cos(q1)*sin(q2)*sin(q3) - L11*gy*m4*cos(q1)*sin(q2)*sin(q3) - L11*gy*m5*cos(q1)*sin(q2)*sin(q3) - L11*gy*m6*cos(q1)*sin(q2)*sin(q3) + L10*gx*m3*sin(q1)*sin(q2)*sin(q3) + L11*gx*m4*sin(q1)*sin(q2)*sin(q3) + L11*gx*m5*sin(q1)*sin(q2)*sin(q3) + L11*gx*m6*sin(q1)*sin(q2)*sin(q3) + L4*gy*m6*cos(q2 - q3 + q4)*cos(q1)*sin(q5) - L4*gx*m6*cos(q2 - q3 + q4)*sin(q1)*sin(q5) - L10*gy*m3*cos(q1)*cos(q2)*cos(q3) - L11*gy*m4*cos(q1)*cos(q2)*cos(q3) - L11*gy*m5*cos(q1)*cos(q2)*cos(q3) - L11*gy*m6*cos(q1)*cos(q2)*cos(q3);
double G2 = L3*gx*m3*cos(q1)*sin(q2) - L12*gz*m6*sin(q2 - q3 + q4) - L3*gz*m3*cos(q2) - L3*gz*m4*cos(q2) - L3*gz*m5*cos(q2) - L3*gz*m6*cos(q2) - L8*gz*m2*cos(q2) - L10*gz*m3*cos(q2 - q3) - L11*gz*m4*cos(q2 - q3) - L11*gz*m5*cos(q2 - q3) - L11*gz*m6*cos(q2 - q3) - L12*gz*m5*sin(q2 - q3 + q4) + L3*gx*m4*cos(q1)*sin(q2) + L3*gx*m5*cos(q1)*sin(q2) + L3*gx*m6*cos(q1)*sin(q2) + L8*gx*m2*cos(q1)*sin(q2) + L3*gy*m3*sin(q1)*sin(q2) + L3*gy*m4*sin(q1)*sin(q2) + L3*gy*m5*sin(q1)*sin(q2) + L3*gy*m6*sin(q1)*sin(q2) + L8*gy*m2*sin(q1)*sin(q2) - L12*gx*m5*cos(q2 - q3 + q4)*cos(q1) - L12*gx*m6*cos(q2 - q3 + q4)*cos(q1) - L12*gy*m5*cos(q2 - q3 + q4)*sin(q1) - L12*gy*m6*cos(q2 - q3 + q4)*sin(q1) + L4*gz*m6*cos(q2 - q3 + q4)*sin(q5) - L10*gx*m3*cos(q1)*cos(q2)*sin(q3) + L10*gx*m3*cos(q1)*cos(q3)*sin(q2) - L11*gx*m4*cos(q1)*cos(q2)*sin(q3) + L11*gx*m4*cos(q1)*cos(q3)*sin(q2) - L11*gx*m5*cos(q1)*cos(q2)*sin(q3) + L11*gx*m5*cos(q1)*cos(q3)*sin(q2) - L11*gx*m6*cos(q1)*cos(q2)*sin(q3) + L11*gx*m6*cos(q1)*cos(q3)*sin(q2) - L10*gy*m3*cos(q2)*sin(q1)*sin(q3) + L10*gy*m3*cos(q3)*sin(q1)*sin(q2) - L11*gy*m4*cos(q2)*sin(q1)*sin(q3) + L11*gy*m4*cos(q3)*sin(q1)*sin(q2) - L11*gy*m5*cos(q2)*sin(q1)*sin(q3) + L11*gy*m5*cos(q3)*sin(q1)*sin(q2) - L11*gy*m6*cos(q2)*sin(q1)*sin(q3) + L11*gy*m6*cos(q3)*sin(q1)*sin(q2) - L4*gx*m6*sin(q2 - q3 + q4)*cos(q1)*sin(q5) - L4*gy*m6*sin(q2 - q3 + q4)*sin(q1)*sin(q5);
double G3 = gx*m3*(L10*cos(q1)*cos(q2)*sin(q3) - L10*cos(q1)*cos(q3)*sin(q2)) + gx*m4*(L11*cos(q1)*cos(q2)*sin(q3) - L11*cos(q1)*cos(q3)*sin(q2)) + gy*m6*(L12*cos(q2 - q3 + q4)*sin(q1) + L11*cos(q2)*sin(q1)*sin(q3) - L11*cos(q3)*sin(q1)*sin(q2) + L4*sin(q2 - q3 + q4)*sin(q1)*sin(q5)) + gy*m3*(L10*cos(q2)*sin(q1)*sin(q3) - L10*cos(q3)*sin(q1)*sin(q2)) + gy*m4*(L11*cos(q2)*sin(q1)*sin(q3) - L11*cos(q3)*sin(q1)*sin(q2)) + gy*m5*(L12*cos(q2 - q3 + q4)*sin(q1) + L11*cos(q2)*sin(q1)*sin(q3) - L11*cos(q3)*sin(q1)*sin(q2)) + gz*m5*(L12*sin(q2 - q3 + q4) + L11*cos(q2 - q3)) + gz*m6*(L12*sin(q2 - q3 + q4) + L11*cos(q2 - q3) - L4*cos(q2 - q3 + q4)*sin(q5)) + gx*m6*(L12*cos(q2 - q3 + q4)*cos(q1) + L11*cos(q1)*cos(q2)*sin(q3) - L11*cos(q1)*cos(q3)*sin(q2) + L4*sin(q2 - q3 + q4)*cos(q1)*sin(q5)) + gx*m5*(L12*cos(q2 - q3 + q4)*cos(q1) + L11*cos(q1)*cos(q2)*sin(q3) - L11*cos(q1)*cos(q3)*sin(q2)) + L10*gz*m3*cos(q2 - q3) + L11*gz*m4*cos(q2 - q3);
double G4 = L4*gz*m6*cos(q2 - q3 + q4)*sin(q5) - L12*gz*m6*sin(q2 - q3 + q4) - gx*m6*cos(q1)*(L12*cos(q2 - q3 + q4) + L4*sin(q2 - q3 + q4)*sin(q5)) - gy*m6*sin(q1)*(L12*cos(q2 - q3 + q4) + L4*sin(q2 - q3 + q4)*sin(q5)) - L12*gx*m5*cos(q2 - q3 + q4)*cos(q1) - L12*gy*m5*cos(q2 - q3 + q4)*sin(q1) - L12*gz*m5*sin(q2 - q3 + q4);
double G5 = L4*gy*m6*(cos(q1)*sin(q5) + cos(q2 - q3 + q4)*cos(q5)*sin(q1)) - L4*gx*m6*(sin(q1)*sin(q5) - cos(q2 - q3 + q4)*cos(q1)*cos(q5)) + L4*gz*m6*sin(q2 - q3 + q4)*cos(q5);
double G6 = 0;
L8=u(35);
L10=u(36);

L=[L1;L2;L4;L6;L7;L9;L3;L5;L8;L10];


Kis=diag([u(37);u(38);u(39)]);
Ki=diag([0;0;0]);


gx=u(40)*g;
gy=u(41)*g;
gz=u(42)*g;

I111=u(43);
I112=u(44);
I113=u(45);
I122=u(46);
I123=u(47);
I133=u(48);

I211=u(49);
I212=u(50);
I213=u(51);
I222=u(52);
I223=u(53);
I233=u(54);

I311=u(55);
I312=u(56);
I313=u(57);
I322=u(58);
I323=u(59);
I333=u(60);


%X_c = [X_c1;X_c2;X_c3];


    M(1,1) = I133 + I211 + I311/2 + I322/2 - (I311*cos(2*q2 - 2*q3))/2 + (I322*cos(2*q2 - 2*q3))/2 - I312*sin(2*q2 - 2*q3) + (L3^2*m3)/2 + L7^2*m2 + L9^2*m3 + (L10^2*m3)/2 - I211*cos(q2)^2 + I222*cos(q2)^2 + I212*sin(2*q2) + (L3^2*m3*cos(2*q2))/2 + L8^2*m2*cos(q2)^2 + (L10^2*m3*cos(2*q2 - 2*q3))/2 + L3*L10*m3*cos(q3) + L3*L10*m3*cos(2*q2 - q3);
    M(1,2) = I223*cos(q2) + I213*sin(q2) + I323*cos(q2 - q3) - I313*sin(q2 - q3) + L3*L9*m3*sin(q2) + L7*L8*m2*sin(q2) + L9*L10*m3*sin(q2 - q3);
    M(1,3) = I313*sin(q2 - q3) - I323*cos(q2 - q3) - L9*L10*m3*sin(q2 - q3);
    M(2,1) = I223*cos(q2) + I213*sin(q2) + I323*cos(q2 - q3) - I313*sin(q2 - q3) + L3*L9*m3*sin(q2) + L7*L8*m2*sin(q2) + L9*L10*m3*sin(q2 - q3);
    M(2,2) = m3*L3^2 + 2*m3*cos(q3)*L3*L10 + m2*L8^2 + m3*L10^2 + I233 + I333;
    M(2,3) = - m3*L10^2 - L3*m3*cos(q3)*L10 - I333;
    M(3,1) = I313*sin(q2 - q3) - I323*cos(q2 - q3) - L9*L10*m3*sin(q2 - q3);
    M(3,2) = - m3*L10^2 - L3*m3*cos(q3)*L10 - I333;
    M(3,3) = m3*L10^2 + I333;


    C(1,1) = (q3p*(2*I312*cos(2*q2 - 2*q3) - I311*sin(2*q2 - 2*q3) + I322*sin(2*q2 - 2*q3) + L10^2*m3*sin(2*q2 - 2*q3) - L3*L10*m3*sin(q3) + L3*L10*m3*sin(2*q2 - q3)))/2 - (q2p*(m3*sin(2*q2)*L3^2 + 2*m3*sin(2*q2 - q3)*L3*L10 + m2*sin(2*q2)*L8^2 + m3*sin(2*q2 - 2*q3)*L10^2 + 2*I312*cos(2*q2 - 2*q3) - I311*sin(2*q2 - 2*q3) + I322*sin(2*q2 - 2*q3) - 2*I212*cos(2*q2) - I211*sin(2*q2) + I222*sin(2*q2)))/2;
    C(1,2) = q3p*(I313*cos(q2 - q3) + I323*sin(q2 - q3) - L9*L10*m3*cos(q2 - q3)) - (q1p*(m3*sin(2*q2)*L3^2 + 2*m3*sin(2*q2 - q3)*L3*L10 + m2*sin(2*q2)*L8^2 + m3*sin(2*q2 - 2*q3)*L10^2 + 2*I312*cos(2*q2 - 2*q3) - I311*sin(2*q2 - 2*q3) + I322*sin(2*q2 - 2*q3) - 2*I212*cos(2*q2) - I211*sin(2*q2) + I222*sin(2*q2)))/2 + q2p*(I213*cos(q2) - I223*sin(q2) - I313*cos(q2 - q3) - I323*sin(q2 - q3) + L3*L9*m3*cos(q2) + L7*L8*m2*cos(q2) + L9*L10*m3*cos(q2 - q3));
    C(1,3) = q2p*(I313*cos(q2 - q3) + I323*sin(q2 - q3) - L9*L10*m3*cos(q2 - q3)) - q3p*(I313*cos(q2 - q3) + I323*sin(q2 - q3) - L9*L10*m3*cos(q2 - q3)) + (q1p*(2*I312*cos(2*q2 - 2*q3) - I311*sin(2*q2 - 2*q3) + I322*sin(2*q2 - 2*q3) + L10^2*m3*sin(2*q2 - 2*q3) - L3*L10*m3*sin(q3) + L3*L10*m3*sin(2*q2 - q3)))/2;
    C(2,1) = (q1p*(m3*sin(2*q2)*L3^2 + 2*m3*sin(2*q2 - q3)*L3*L10 + m2*sin(2*q2)*L8^2 + m3*sin(2*q2 - 2*q3)*L10^2 + 2*I312*cos(2*q2 - 2*q3) - I311*sin(2*q2 - 2*q3) + I322*sin(2*q2 - 2*q3) - 2*I212*cos(2*q2) - I211*sin(2*q2) + I222*sin(2*q2)))/2;
    C(2,2) = -L3*L10*m3*q3p*sin(q3);
    C(2,3) = -L3*L10*m3*sin(q3)*(q2p - q3p);
    C(3,1) = -(q1p*(2*I312*cos(2*q2 - 2*q3) - I311*sin(2*q2 - 2*q3) + I322*sin(2*q2 - 2*q3) + L10^2*m3*sin(2*q2 - 2*q3) - L3*L10*m3*sin(q3) + L3*L10*m3*sin(2*q2 - q3)))/2;
    C(3,2) = L3*L10*m3*q2p*sin(q3);
    C(3,3) = 0;


    G(1,1) = gx*m3*(L9*cos(q1) + L3*cos(q2)*sin(q1) + L10*cos(q2)*cos(q3)*sin(q1) + L10*sin(q1)*sin(q2)*sin(q3)) + gx*m2*(L7*cos(q1) + L8*cos(q2)*sin(q1)) + gy*m2*(L7*sin(q1) - L8*cos(q1)*cos(q2)) - gy*m3*(L3*cos(q1)*cos(q2) - L9*sin(q1) + L10*cos(q1)*cos(q2)*cos(q3) + L10*cos(q1)*sin(q2)*sin(q3));
    G(2,1) = gx*m3*(L3*cos(q1)*sin(q2) - L10*cos(q1)*cos(q2)*sin(q3) + L10*cos(q1)*cos(q3)*sin(q2)) + gy*m3*(L3*sin(q1)*sin(q2) - L10*cos(q2)*sin(q1)*sin(q3) + L10*cos(q3)*sin(q1)*sin(q2)) - gz*m3*(L3*cos(q2) + L10*cos(q2 - q3)) - L8*gz*m2*cos(q2) + L8*gx*m2*cos(q1)*sin(q2) + L8*gy*m2*sin(q1)*sin(q2);
    G(3,1) = gx*m3*(L10*cos(q1)*cos(q2)*sin(q3) - L10*cos(q1)*cos(q3)*sin(q2)) + gy*m3*(L10*cos(q2)*sin(q1)*sin(q3) - L10*cos(q3)*sin(q1)*sin(q2)) + L10*gz*m3*cos(q2 - q3);

%% PD (+G)-controller
%Joint Errors

if (t<t0)
    DeltaQ = Q - Qd;
    DeltaQp = Qp -Qdp;
    
    %Define controllers PD, PD+G

    tauc = - Kp * DeltaQ - Kd * Qp + G;
%     if (t>0)
%         tauc = - Kp * DeltaQ - Kd * Qp;
%     end
%     V = 0.5 * Qp'*M*Qp;
    tau=[tauc;DeltaQ;DeltaQp];

end
%% Other Controller
if (t<t1 && t>=t0)
    DeltaQ = Q - Qd;
    DeltaQp = Qp - Qdp;

    
    %Break
    %Qp_r = [0;0;0];
    
    %PD-like
    Qp_r = Qdp - Kp*DeltaQ;
    
    %PID-like
    
    %if(t==t0)
    %    DeltaQ_old = zeros(3,1);
    %end
    %Qp_r = Qdp - Kp*DeltaQ - Ki*(DeltaQ+DeltaQ_old)*dt;
    %DeltaQ_old = DeltaQ;
    
    
    
    Sq = Qp - Qp_r;
    
    if(t==0)
        Qp_r_old = zeros(3,1);
    end
    
    Qpp_r = (Qp_r - Qp_r_old)./dt;
    
    Qp_r_old = Qp_r;
    



    tauc = - Kd*Sq + M*Qpp_r + C*Qp_r + G;
    
    tau=[tauc;DeltaQ;DeltaQp];

end


%% Safety for Singularities

if (singularity)
    
    DeltaQ = Q - Qd;
    DeltaQp = Qp - Qdp;
    
    
    Qp_r = Qdp - Kp*DeltaQ;


    if(t==t_s)
        Qp_r_old = zeros(3,1);
    end
    
    Qpp_r = (Qp_r - Qp_r_old)./dt;
    
    Qp_r_old = Qp_r;

    
    Sq = Qp - Qp_r;
    
    tauc = - Kd*Sq + M*Qpp_r + C*Qp_r + G;
    
    tau=[tauc;DeltaQ;DeltaQp];


end
%% Operational Space Controller
if (t>=t1 &&  t<t2 && not(singularity))

    
    DeltaX = FK_ur10_3DOF([Q;Qp;L]) - Qd;
    DeltaXp =  DifFK_ur10_3DOF([Q;Qp;L]) - Qdp;
    
    %PD-like
    Xp_r = Qdp - Kp*DeltaX;
   
    %PID-like
    %if(t==t1)
    %    DeltaQ_old = zeros(3,1);
    %end
    %Xp_r = Qdp - Kp*DeltaX - Ki*(DeltaX+DeltaX_old)*dt;
    %DeltaQ_old = DeltaQ;
    
    J3_0 = ...
    [ L2*cos(q1) + L4*cos(q1) + L3*cos(q2)*sin(q1) + L5*cos(q2)*cos(q3)*sin(q1) + L5*sin(q1)*sin(q2)*sin(q3), cos(q1)*(L3*sin(q2) + L5*sin(q2 - q3)), -L5*sin(q2 - q3)*cos(q1);
      L2*sin(q1) + L4*sin(q1) - L3*cos(q1)*cos(q2) - L5*cos(q1)*cos(q2)*cos(q3) - L5*cos(q1)*sin(q2)*sin(q3), sin(q1)*(L3*sin(q2) + L5*sin(q2 - q3)), -L5*sin(q2 - q3)*sin(q1);
                                                                                                           0,         - L3*cos(q2) - L5*cos(q2 - q3),          L5*cos(q2 - q3)];

    Qp_r = J3_0\Xp_r;

    %Qp_r = Qdp - Kp*DeltaQ - Ki*DeltaQ*dt;

    Sq = Qp - Qp_r;
    
    if(t==t1)
        Qp_r_old = zeros(3,1);
    end
    
    Qpp_r = (Qp_r - Qp_r_old)./dt;
    Qp_r_old = Qp_r;
    
    tauc = - Kd*Sq + M*Qpp_r + C*Qp_r + G;
    tau=[tauc;DeltaX;DeltaXp];

end

%% Circular Controller 1    
if (t>=t2 &&  t<t3 && not(singularity))
    
    if(t==t2)
        R = 0.25;
        Z = 0.1;
        Xc = FK_ur10_3DOF([Q;Qp;L]) + [R;0;0];
    end
    
    Tr = 3;
    Tz = 1;
    
    Qd(1,1) = Xc(1) - R * cos(2*pi*(t-t2)/Tr);
    Qd(2,1) = Xc(2) + R * sin(2*pi*(t-t2)/Tr);
    Qd(3,1) = Xc(3) + Z * sin(2*pi*(t-t2)/Tz);
    
    Qdp(1,1) = R * 2*pi/Tr * sin(2*pi*(t-t2)/Tr);
    Qdp(2,1) = R * 2*pi/Tr *cos(2*pi*(t-t2)/Tr);
    Qdp(3,1) = Z * 2*pi/Tz *cos(2*pi*(t-t2)/Tz);
    
    DeltaX = FK_ur10_3DOF([Q;Qp;L]) - Qd;
    DeltaXp =  DifFK_ur10_3DOF([Q;Qp;L]) - Qdp;

   
    Xp_r = Qdp - Kp*DeltaX;
    
    
    J3_0 = ...
    [ L2*cos(q1) + L4*cos(q1) + L3*cos(q2)*sin(q1) + L5*cos(q2)*cos(q3)*sin(q1) + L5*sin(q1)*sin(q2)*sin(q3), cos(q1)*(L3*sin(q2) + L5*sin(q2 - q3)), -L5*sin(q2 - q3)*cos(q1);
      L2*sin(q1) + L4*sin(q1) - L3*cos(q1)*cos(q2) - L5*cos(q1)*cos(q2)*cos(q3) - L5*cos(q1)*sin(q2)*sin(q3), sin(q1)*(L3*sin(q2) + L5*sin(q2 - q3)), -L5*sin(q2 - q3)*sin(q1);
                                                                                                           0,         - L3*cos(q2) - L5*cos(q2 - q3),          L5*cos(q2 - q3)];

    Qp_r = J3_0\Xp_r;

    %Qp_r = Qdp - Kp*DeltaQ - Ki*DeltaQ*dt;

    Sq = Qp - Qp_r;
    
    if(t==t1)
        Qp_r_old = zeros(3,1);
    end
    
    Qpp_r = (Qp_r - Qp_r_old)./dt;
    Qp_r_old = Qp_r;
    
    tauc = - Kd*Sq + M*Qpp_r + C*Qp_r + G;
    
    if(t==t2)
        DeltaXp = [0;0;0];
    end

    tau=[tauc;DeltaX;DeltaXp];
    
    
end

%% Circular Controller 1    
if (t>=t3 && not(singularity))
        
    Tr = 1;
    Tz = 0.2;
    
    Qd(1,1) = Xc(1) - R * cos(2*pi*(t-t3)/Tr);
    Qd(2,1) = Xc(2) + R * sin(2*pi*(t-t3)/Tr);
    Qd(3,1) = Xc(3) + Z * sin(2*pi*(t-t3)/Tz);
    
    Qdp(1,1) = R * 2*pi/Tr * sin(2*pi*(t-t3)/Tr);
    Qdp(2,1) = R * 2*pi/Tr *cos(2*pi*(t-t3)/Tr);
    Qdp(3,1) = Z * 2*pi/Tz *cos(2*pi*(t-t3)/Tz);
    
    DeltaX = FK_ur10_3DOF([Q;Qp;L]) - Qd;
    DeltaXp =  DifFK_ur10_3DOF([Q;Qp;L]) - Qdp;
    
   
    Xp_r = Qdp - Kp*DeltaX;
    
    
    J3_0 = ...
    [ L2*cos(q1) + L4*cos(q1) + L3*cos(q2)*sin(q1) + L5*cos(q2)*cos(q3)*sin(q1) + L5*sin(q1)*sin(q2)*sin(q3), cos(q1)*(L3*sin(q2) + L5*sin(q2 - q3)), -L5*sin(q2 - q3)*cos(q1);
      L2*sin(q1) + L4*sin(q1) - L3*cos(q1)*cos(q2) - L5*cos(q1)*cos(q2)*cos(q3) - L5*cos(q1)*sin(q2)*sin(q3), sin(q1)*(L3*sin(q2) + L5*sin(q2 - q3)), -L5*sin(q2 - q3)*sin(q1);
                                                                                                           0,         - L3*cos(q2) - L5*cos(q2 - q3),          L5*cos(q2 - q3)];

    Qp_r = J3_0\Xp_r;

    %Qp_r = Qdp - Kp*DeltaQ - Ki*DeltaQ*dt;

    Sq = Qp - Qp_r;
    
    if(t==t1)
        Qp_r_old = zeros(3,1);
    end
    
    Qpp_r = (Qp_r - Qp_r_old)./dt;
    Qp_r_old = Qp_r;
    
    tauc = - Kd*Sq + M*Qpp_r + C*Qp_r + G;
    
    if(t==t3)
        DeltaXp = [0;0;0];
    end

    tau=[tauc;DeltaX;DeltaXp];
    
    
end

end

