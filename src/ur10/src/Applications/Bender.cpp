#include <tum_ics_ur_robot_lli/Robot/RobotArmConstrained.h>
#include "std_msgs/String.h"

#include <QApplication>
#include <signal.h>
#include <visualization_msgs/Marker.h>

#include "ur10bender/GoalQ.h"
#include "../include/Control.h"

using namespace Eigen;



int main(int argc, char **argv) {
    ros::init(argc, argv, "talker");
    ros::NodeHandle n;
    ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);
    
    ros::Rate loop_rate(10);
    int count = 0;

    QApplication a(argc,argv);

    ros::init(argc,argv,"testRobotArmClass",ros::init_options::AnonymousName);

    QString configFilePath=argv[1];

    ROS_INFO_STREAM("Config File: "<<configFilePath.toStdString().c_str());

    tum_ics_ur_robot_lli::Robot::RobotArmConstrained robot(configFilePath);
    tum_ics_ur_robot_lli::RobotTime time(ros::Time::now());

    //starts robotArm communication and the thread
    //inits Kinematic Model, Dynamic Model
    if(!robot.init()){
        return -1;
    }

    tum_ics_ur_robot_lli::RobotControllers::BenderControl controller;


    //The control must be connected to the robot after the init()-->The dynamic model needs to
    //be initialized!
    //also calls control.init(), e.g. load ctrl gains
    if(!robot.add(&controller)) {
        return -1;
    }


    controller.setQHome(robot.qHome());
    controller.setQPark(robot.qPark());

    robot.start();

    ROS_INFO_STREAM("Start main thread");

    ros::Rate r(30);

    while(ros::ok()) {

        ros::spinOnce();

        r.sleep();

        std_msgs::String msg;

        std::stringstream ss;
        ss << count;
        msg.data = ss.str();

        chatter_pub.publish(msg);

        ros::spinOnce();

        ++count;

    }

    // ros::spin();

    ROS_INFO_STREAM("main: Stoping RobotArm()");
    robot.stop(); //stops robotArm thread




    ROS_INFO_STREAM("main: Stopped!");

}








//    QString scriptFilePath="/home/dean/indigo_ws_iros_flo/src/tom_robot/tom_configs/Arms/parameters/Scripts/progTOM_right";

//    ur10_robot_lli::CommInterface commInterface(computerIp, commandPort, trajPort,robotIp, scriptFilePath, robotScriptPort);


//    usleep(20*1E6);


//    //commInterface.stop();

//    commInterface.setFinishMState();


//    ur10_robot_lli::ScriptLoader scriptLoader(robotIp,
//    "/home/dean/indigo_ws_iros_flo/src/tom_robot/tom_configs/Arms/parameters/Scripts/progParkTOM_RA");

