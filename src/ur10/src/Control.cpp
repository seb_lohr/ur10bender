#include "../include/Control.h"

#include<tum_ics_ur_robot_msgs/ControlData.h>
#include"ur10bender/DataController.h"
#include"ur10bender/GoalQ.h"
#include"ur10bender/GoalX.h"
#include"ur10bender/DrawGoals.h"
#include"ur10bender/CtrlGain.h"
#include "std_msgs/String.h"

namespace tum_ics_ur_robot_lli{
  namespace RobotControllers{

    BenderControl::BenderControl(double weight, const QString &name):
        ControlEffort(name,SPLINE_TYPE,JOINT_SPACE,weight),
        m_trajgenQ(),
        m_trajgenX()
    {
        pubCtrlData=n.advertise<ur10bender::DataController>("/bender/SimpleEffortCtrlData",100);

        m_controlPeriod=0.002; //set the control period to the standard 2 ms

        m_controlPeriod_2=m_controlPeriod/2.0;
        m_ctrlChange = false;
        m_startFlag = false;
        m_goalChangeQ = false;
        m_goalChangeX = false;
        m_ctrlChange = false;
        m_calc_new_poly = true;
        m_ontheway = false;
        m_first_time_force = true;
        m_never_contact_before = true;


        m_KdX.setZero();
        m_KpX.setZero();
        m_Ki.setZero();
        m_Kp.setZero();
        m_Kd.setZero();

        m_timeLast = 0.0;
        m_timeLeft = 0.0;
        m_totalTimeQ = 100.0;
        m_totalTimeX = 100.0;
        m_ctrlChangeTime = 0.0;

        m_DeltaF = 0.0;
        m_penForce = 0.0;
        m_switchCaseForce = 0.0;
        m_X.setZero();
        m_Xd.setZero();
        m_Xd_p.setZero();
        m_Xd_pp.setZero();
        m_Xr_p.setZero();
        m_Xr_pp.setZero();
        m_Sx.setZero();
        m_Force.resize(2);
        m_Torque.resize(2);
        m_Force[0] = Vector3d(0,0,0);
        m_Force[1] = Vector3d(0,0,0);

        m_DeltaQ.setZero();
        m_DeltaQp.setZero();
        m_iDeltaQ.setZero();

        m_DeltaX.setZero();

        m_goal_q.setZero();
        m_goal_x.setZero();

        m_countMutex = PTHREAD_MUTEX_INITIALIZER;


        ROS_INFO_STREAM("SimpleEffortCtrl Control Period: "<<m_controlPeriod<<" ("<<m_controlPeriod_2<<")");

    }

    BenderControl::~BenderControl()
    {

    }

    void BenderControl::setQInit(const JointState& qinit)
    {
        m_qInit=qinit;
    }
    void BenderControl::setQHome(const JointState& qhome)
    {
        m_qHome=qhome;
    }
    void BenderControl::setQPark(const JointState& qpark)
    {
        m_qPark=qpark;
    }

    void BenderControl::setQGoal(const Vector6d& qgoal) {
        m_goal_q = qgoal;
    }

    // callback function: subscribe to goal in joint space
    void BenderControl::QGoalCalllback(const ur10bender::GoalQConstPtr& msg) {
        Vector6d curr_goal;
        for(int i=0;i<STD_DOF;i++) {
            curr_goal(i)= msg->q_desired[i];
        }
        
        curr_goal = DEG2RAD(curr_goal);

        if (curr_goal != m_goal_q) {
            // if goal has changed, set a flag to calculate new trajectory in next update()
            m_goalChangeQ = true;
            ROS_WARN_STREAM("JS Goal [RAD] changed from " << std::endl << m_goal_q.transpose() << " to " << std::endl << curr_goal.transpose() );
            // update goal
            m_goal_q = curr_goal;
            m_totalTimeQ = msg->now_time;
            if (m_totalTimeQ == 0.0){
              m_totalTimeQ = 10.0;
            }
            ROS_INFO_STREAM("within a period of " << m_totalTimeQ << " sec");
        }

        if (msg->set_ctrl && m_controllerType == OS_CONTROLLER) {
            ROS_INFO_STREAM("Controll change requested. Switching to adaptive JS controller");
            ros::param::set("~simple_effort_ctrl/controller", JS_ADAPTIVE_CONTROLLER);
        }
    }

    // callback function: subscribe to goal in os space
    void BenderControl::XGoalCalllback(const ur10bender::GoalXConstPtr& msg) {
        Vector6d curr_goal;
        for(int i=0;i<3;i++) {
            curr_goal(i)= msg->x_desired[i];
        }

        for(int i=3;i<6;i++) {
            // orientations in rad
            curr_goal(i)= msg->orientation_desired[i-3];// * 180 / 3.14;
        }

        if (curr_goal != m_goal_x) {
            // if goal has changed, set a flag to calculate new trajectory in next update()
            m_goalChangeX = true;
            ROS_WARN_STREAM("OS Goal [m] changed from " << std::endl << m_goal_x.transpose() << " to " << std::endl << curr_goal.transpose() );
            // update goal
            m_goal_x = curr_goal;
            pthread_mutex_lock(&this->m_countMutex);
            m_totalTimeX = msg->now_time;
            if (m_totalTimeX == 0.0){
              m_totalTimeX = 10.0;
            }
            ROS_INFO_STREAM("within a period of " << m_totalTimeX << " sec");

            pthread_mutex_unlock(&this->m_countMutex);

        }

        if (msg->set_ctrl && m_controllerType != OS_CONTROLLER) {
            ROS_INFO_STREAM("Controll change requested. Switching to OS controller");
            ros::param::set("~simple_effort_ctrl/controller", OS_CONTROLLER);
        }
    }

    // callback function: subscribe to goal in os space
    void BenderControl::DrawGoalCalllback(const ur10bender::DrawGoalsConstPtr& msg) {
        m_drawGoals.resize(msg->size);
        for (int k=0; k<msg->size; k++) {
            ur10bender::GoalX curr_msg = msg->draw_goals[k];
            
            Vector6d curr_goal;
            for(int i=0;i<3;i++) {
                curr_goal(i)= curr_msg.x_desired[i];
            }
            for(int i=3;i<6;i++) {
                // orientations in degrees converted in angles
                curr_goal(i)= curr_msg.orientation_desired[i-3];
            }
            m_drawGoals[k] = curr_goal;
        }
    }

    // callback function: subscribe to gain messages
    void BenderControl::GainCalllback(const ur10bender::CtrlGainConstPtr& msg) {
        Matrix6d new_gain;
        new_gain.setZero();

        for(int i=0;i<6;i++) {
            new_gain(i,i) = msg->values[i];
        }

        std::string gain_name = msg->name;
        
        if (gain_name.compare("kpq") == 0) {
            ROS_INFO_STREAM("New " << gain_name << " Gains: " << std::endl << new_gain);
            m_Kp = new_gain;
        } else if (gain_name.compare("kpx") == 0) {
            ROS_INFO_STREAM("New " << gain_name << " Gains: " << std::endl << new_gain);
            m_KpX = new_gain;
        } else if (gain_name.compare("kd") == 0) {
            ROS_INFO_STREAM("New " << gain_name << " Gains: " << std::endl << new_gain);
            m_Kd = new_gain;
            m_KdX = new_gain;
        } else if (gain_name.compare("integral") == 0) {
            ROS_INFO_STREAM("New " << gain_name << " Gains: " << std::endl << new_gain);
            m_Ki = new_gain;
        } else if (gain_name.compare("gamma") == 0) {
            m_gamma = msg->values[0];
            ROS_INFO_STREAM("New " << gain_name << ": " << m_gamma);
        }
    }

    // Callback to get Values from Force/Torque sensor 
    void BenderControl::WrenchCallback(const geometry_msgs::WrenchStampedConstPtr& msg) {
        // get force
        geometry_msgs::Vector3 f = msg->wrench.force;
        m_Force[1] = m_Force[0];
        m_Force[0] << f.x, f.y, f.z;

        // get torque
        geometry_msgs::Vector3 t = msg->wrench.torque;
        m_Torque[1] = m_Torque[0];
        m_Torque[0] << t.x, t.y, t.z;
    }

    double BenderControl::manipulabilityIndex(Vector6d Q) {
        // get jacobian
        Matrix6d Jef = dinamica.JacobianEF(Q);
        Matrix6d JJt = Jef * Jef.transpose();

        return std::sqrt(JJt.determinant());
    }

    void BenderControl::CalculateForce(){
        // take second component (y) of current value
        double Foffset = 0.0;
        std::string s = "~simple_effort_ctrl/ForceOffset";
        ros::param::get(s, Foffset);

        double unbiasedFy = std::abs(m_Force[0](1) - Foffset - m_switchCaseForce);
        if (std::abs(unbiasedFy)>100){
            return;
        }
        m_penForce = 0.998*m_penForce + 0.002*unbiasedFy;
    }

    void BenderControl::butterworth() {
        // take second component (y) of current and last values
        m_penForce = 0.1602 * m_Force[0](1) + 0.1602 * m_Force[1](1) - 0.6796 * m_penForce;

    }

    bool BenderControl::init(){

        std::string ns="~simple_effort_ctrl";
        std::stringstream s;

        if (!ros::param::has(ns))
        {
            s<<"BenderControl init(): Control gains not defined --"<<ns<<"--, did you load them in the rosparam server??";
            m_error=true;
            m_errorString=s.str().c_str();
            return false;
        }


        VDouble p;
        
        /////D GAINS
        s<<ns<<"/gains_d";
        ros::param::get(s.str(),p);

        if(p.size()<STD_DOF)
        {
            s.str("");
            s<<"BenderControl init(): Wrong number of d_gains --"<<p.size()<<"--";
            m_error=true;
            m_errorString=s.str().c_str();
            return false;
        }
        for(int i=0;i<STD_DOF;i++)
        {
            m_Kd(i,i)=p[i];
        }

        ROS_WARN_STREAM("Kd: \n"<<m_Kd);

        /////P GAINS
        s.str("");
        s<<ns<<"/gains_p";
        ros::param::get(s.str(),p);

        if(p.size()<STD_DOF)
        {
            s.str("");
            s<<"BenderControl init(): Wrong number of p_gains --"<<p.size()<<"--";
            m_error=true;
            m_errorString=s.str().c_str();
            return false;
        }
        for(int i=0;i<STD_DOF;i++)
        {
            m_Kp(i,i)=p[i]/m_Kd(i,i);
        }

        ROS_WARN_STREAM("Kp: \n"<<m_Kp);

        // D Gains OS Space
        s.str("");
        s<<ns<<"/gains_dX";
        ros::param::get(s.str(),p);

        if(p.size()<STD_DOF)
        {
            s.str("");
            s<<"BenderControl init(): Wrong number of p_gains --"<<p.size()<<"--";
            m_error=true;
            m_errorString=s.str().c_str();
            return false;
        }

        m_KdX.setZero();
        for(int i=0;i<STD_DOF;i++)
        {
            m_KdX(i,i)=p[i];
        }

        // P Gains OS Space
        s.str("");
        s<<ns<<"/gains_pX";
        ros::param::get(s.str(),p);

        if(p.size()<STD_DOF)
        {
            s.str("");
            s<<"BenderControl init(): Wrong number of p_gains --"<<p.size()<<"--";
            m_error=true;
            m_errorString=s.str().c_str();
            return false;
        }
        for(int i=0;i<STD_DOF;i++)
        {
            m_KpX(i,i)=p[i];// /m_Kd(i,i);
        }
        ROS_WARN_STREAM("KpX: \n"<<m_KpX);


        // gamma: js adaptive controller
        s.str("");
        s << "/Bender/simple_effort_ctrl/gamma";
        ros::param::get(s.str(),p);
        m_gamma = p[0];
        ROS_WARN_STREAM("Gamma: "<<m_gamma);

        // beta: force controller
        s.str("");
        s << "/Bender/simple_effort_ctrl/beta";
        ros::param::get(s.str(),p);
        m_beta = p[0];
        ROS_WARN_STREAM("Beta: "<<m_beta);

        // Controller
        s.str("");
        s<<"~simple_effort_ctrl/controller";
        ros::param::get(s.str(),m_controllerType);
        ROS_WARN_STREAM("ControllerType: " << m_controllerType);

        // Goal Force
        s.str("");
        s<<"~simple_effort_ctrl/penForce";
        ros::param::get(s.str(),m_penForce_d);
        ROS_WARN_STREAM("penForce: " << m_penForce_d);

        /////Desired Orientation while drawings
        s.str("");
        s<<ns<<"/simple_effort_ctrl/orientation";
        ros::param::get(s.str(),p);
        for(int i=0;i<3;i++)
        {
            m_Xw_Drawing(i)=p[i];
        }
        ROS_WARN_STREAM("desired orientation while drawing: " << m_Xw_Drawing.transpose());

        /////GOAL
        s.str("");
        s<<ns<<"/goal";
        ros::param::get(s.str(),p);

        if(p.size()<STD_DOF)
        {
            s.str("");
            s<<"BenderControl init(): Wrong number of joint goals --"<<p.size()<<"--";
            m_error=true;
            m_errorString=s.str().c_str();
            return false;
        }
        for(int i=0;i<STD_DOF;i++)
        {
            m_goal_q(i)=p[i];
        }
        m_totalTimeQ=p[STD_DOF];


        // subscribe to goal publisher
        m_goalQ_sub = n.subscribe("/bender/goal_q", 1000, &BenderControl::QGoalCalllback, this);
        m_goalX_sub = n.subscribe("/bender/goal_x", 1000, &BenderControl::XGoalCalllback, this);
        m_drawgoals_sub = n.subscribe("/bender/draw_goals", 1000, &BenderControl::DrawGoalCalllback, this);
        // subscribe to gain publisher
        m_gain_sub = n.subscribe("/bender/ctrl_gains", 1000, &BenderControl::GainCalllback, this);
        // subscribe to force torque sensor
        m_force_sub = n.subscribe("/wrist_ft", 100, &BenderControl::WrenchCallback, this);
        // 
        m_goal_reached_pub = n.advertise<std_msgs::String>("/bender/goal_reached",10);

        pthread_mutex_lock(&this->m_countMutex);
        if(!(m_totalTimeQ > 0))
        {
            m_totalTimeQ = 100.0;
        }
        if(!(m_totalTimeX > 0))
        {
            m_totalTimeX = 100.0;
        }
        pthread_mutex_unlock(&this->m_countMutex);

        // only joint space goal is interesting in the beginning
        ROS_WARN_STREAM("Goal [DEG]: " << std::endl << m_goal_q.transpose());
        ROS_WARN_STREAM("Total Time [s]: "<<m_totalTimeQ);
        m_goal_q =DEG2RAD(m_goal_q);
        ROS_WARN_STREAM("Goal [RAD]: " << std::endl << m_goal_q.transpose());
        
        // initialise the dynamic model
        dinamica.ini();


        return true;
    }


    Vector6d BenderControl::NoController(){
        return Vector6d::Zero();
    }


    Vector6d BenderControl::JS_SimplePD(const RobotTime &time, const JointState &current){

        double currentTime = time.tD();

        if(!m_startFlag)
        {
            m_qStart=current.q;
            m_startFlag=true;
            Vector6d goal;
            goal.setZero();
            // FIXME: do m_trajgenQ in update function?
            m_targetTimeQ = m_totalTimeQ;
            m_trajgenQ.CalculatePolynomial6d(m_qStart,m_goal_q,goal, goal, goal, goal, currentTime, m_totalTimeQ);
        }

        m_Qd=m_trajgenQ.GetCurrentStatesOnTrajectory6d(currentTime,m_targetTimeQ);

        m_DeltaQ=current.q-m_Qd.col(0);
        m_DeltaQp=current.qp-m_Qd.col(1);

        JointState js_r;

        js_r = current;
        js_r.qp = m_Qd.col(1)-m_Kp*m_DeltaQ;
        js_r.qpp = m_Qd.col(2)-m_Kp*m_DeltaQp;

        Vector6d Sq = current.qp - js_r.qp;

        return -m_Kd*Sq;
    }


    Vector6d BenderControl::JS_adaptiveController(const RobotTime &time, const JointState &current){

        double currentTime = time.tD();

        // Vector6d Q   = current.q;
        // ROS_INFO_STREAM(X.transpose());

        if(!m_startFlag || m_ctrlChange) {
            m_qStart = current.q;
            Vector6d Q_p = current.qp;
            Vector6d Q_pp = current.qpp;
            m_startFlag = true;
            Vector6d goal;
            m_goal_q = m_qStart;
            goal.setZero();

            // ROS_INFO_STREAM("start: " << m_qStart.transpose());
            // ROS_INFO_STREAM("goal:  " << m_goal_q.transpose());
            // FIXME: do m_trajgenQ in update function?
            m_targetTimeQ = m_totalTimeQ + currentTime;
            m_trajgenQ.CalculatePolynomial6d(m_qStart,m_goal_q, Q_p, goal, Q_pp, goal, currentTime, m_targetTimeQ);
        }

        m_Qd=m_trajgenQ.GetCurrentStatesOnTrajectory6d(currentTime,m_targetTimeQ);

        m_DeltaQ = current.q  - m_Qd.col(0);
        m_DeltaQp= current.qp - m_Qd.col(1);

        JointState q_r;
        q_r = current;
        q_r.qp = m_Qd.col(1) - m_Kp*m_DeltaQ;
        q_r.qpp = m_Qd.col(2)- m_Kp*m_DeltaQp;

        Vector6d Sq = current.qp - q_r.qp;


        m_dt = time.tD() - m_timeLast;
        m_timeLast = time.tD();
        if(m_dt<=0){
            m_dt = 0;
        }

        // only update if we did not change controller recently
        if (time.tD() - m_ctrlChangeTime > 2 || time.tD() < 2) {
            // update adaptive parameters
            dinamica.CalculateTheta(current.q, current.qp, q_r.qp, q_r.qpp, Sq, m_dt, m_gamma);
        }
        
        // controller: tau = -Kd * Sq + Yr * (-gamma) Yr' * Sq
        Vector6d adaptive = dinamica.CalculateYr(current.q, current.qp, q_r.qp, q_r.qpp) * dinamica.getTheta();
        Vector6d tauQ = -m_Kd * Sq;

        Vector6d tau = tauQ + adaptive;
        
        // just for message
        m_adaptive = adaptive;
        m_Sq = Sq;
        m_Qrp = q_r.qp;
        m_Qrpp = q_r.qpp;
        m_X = dinamica.FKPosition(current.q);

        return tau;
    }


    // Operational Space Controller
    Vector6d BenderControl::OS_controller(const RobotTime &time, const JointState &current){
        
        // q and q_p
        Vector6d Q   = current.q;
        Vector6d Q_p = current.qp;
        Vector6d Q_pp = current.qpp;

        // Jacobians
        Matrix6d Jef     = dinamica.JacobianEF(Q);
        Matrix6d Jef_inv = Jef.inverse();
        Matrix6d Jef_p   = dinamica.JacobianDotEF(Q, Q_p);

        // current cartesian positions, orientation in euler angles
        Vector6d X = dinamica.FKPosition(Q);
        // Vector6d X_p_FK = dinamica.FKVelocity(Q_p);
        Vector6d X_p = Jef * Q_p;
        Vector6d X_pp = Jef_p * Q_p + Jef * Q_pp;
        // current EF orientation
        Matrix3d Rot_EF = dinamica.rotationEF(Q);
        
        double currentTime = time.tD();
        if(!(m_startFlag) || m_ctrlChange) {
            m_xStart = X;
            m_goal_x = m_xStart;

            // ROS_INFO_STREAM("start: " << m_xStart.transpose());
            // ROS_INFO_STREAM("goal:  " << m_goal_x.transpose());
            m_totalTimeX = 10;
            Vector6d goal_p;
            goal_p.setZero();
            m_targetTimeX = m_totalTimeX + currentTime;
            m_trajgenX.CalculatePolynomial6d(m_xStart, m_goal_x, X_p, goal_p, X_pp, goal_p, currentTime, m_targetTimeX);
        }

        My::Matrix63d Xdes = m_trajgenX.GetCurrentStatesOnTrajectory6d(currentTime,m_targetTimeX);

        // x desired
        m_Xd    = Xdes.col(0);
        m_Xd_p  = Xdes.col(1);
        m_Xd_pp = Xdes.col(2);
        
        // desired orientation as rotation matrix
        m_Rd =  AngleAxisd(m_Xd(3), Vector3d(1,0,0)) *
                AngleAxisd(m_Xd(4), Vector3d(0,1,0)) * 
                AngleAxisd(m_Xd(5), Vector3d(0,0,1));

        // rotation between ef and desired position
        Matrix3d Re_d = Rot_EF.transpose() * m_Rd;

        // unit quaternion of Re_d with respect to base
        Quaterniond quat_e_de(Re_d);
        quat_e_de.normalize();
        Vector3d orientation_err = Rot_EF * quat_e_de.vec();

        Vector6d DeltaX = X - m_Xd;
        DeltaX.bottomRows(3) = -orientation_err;
        // reference for PD-Like: Sq = JefInv * (X_p - Xr_p)
        Vector6d Xr_p = m_Xd_p - m_KpX * DeltaX; 
        Vector6d Xr_pp= m_Xd_pp - m_KpX * (X_p - m_Xd_p);
        Vector6d Sx   = X_p - Xr_p;
        Vector6d Sq   = Jef_inv * Sx;

        // time delta since last step
        m_dt = time.tD() - m_timeLast;
        m_timeLast = time.tD();
        if(m_dt<=0){
            m_dt = 0;
        }

        // adaptive part: Qr_pp = JefInv * ( Xr_pp - JefDot * Qr_p)
        Vector6d Qr_p  = Jef_inv * Xr_p;
        Vector6d Qr_pp = Jef_inv * (Xr_pp - Jef_p * Qr_p);

        // only update if we did not change controller recently
        if (time.tD() - m_ctrlChangeTime > 2) {
            // update adaptive parameters
            dinamica.CalculateTheta(current.q, current.qp, Qr_p, Qr_pp, Sq, m_dt, m_gamma);
        }

        Vector6d adaptive =  dinamica.CalculateYr(Q, Q_p, Qr_p, Qr_pp) * dinamica.getTheta();
        Vector6d tau = -m_KdX * Sq + adaptive;

        // just for message
        m_Sq = Sq;
        m_Sx = Sx;
        m_Qrp = Qr_p;
        m_Qrpp = Qr_pp;
        m_Xr_p = Xr_p;
        m_Xr_pp = Xr_pp;
        m_X = X;
        m_X_p = X_p;
        m_DeltaX = DeltaX;

        m_adaptive = adaptive;
        m_tau = tau;

        return tau;
    }


    Vector6d BenderControl::FS_simpleController(const RobotTime &time, const JointState &current){
        
        std::string s = "~simple_effort_ctrl/penForce";
        ros::param::get(s, m_penForce_d);
        

        // q and q_p
        Vector6d Q   = current.q;
        Vector6d Q_p = current.qp;
        Vector6d Q_pp = current.qpp;

        // Jacobians
        Matrix6d Jef     = dinamica.JacobianEF(Q);
        Matrix6d Jef_inv = Jef.inverse();
        Matrix6d Jef_p   = dinamica.JacobianDotEF(Q, Q_p);

        // current cartesian positions, orientation in euler angles
        Vector6d X = dinamica.FKPosition(Q);
        // Vector6d X_p_FK = dinamica.FKVelocity(Q_p);
        Vector6d X_p = Jef * Q_p;
        // current EF orientation
        Matrix3d Rot_EF = dinamica.rotationEF(Q);

        double currentTime = time.tD();
        if(!(m_startFlag) || m_ctrlChange) {
            My::Matrix63d Xd = m_trajgenX.GetCurrentStatesOnTrajectory6d(currentTime,m_targetTimeX);
            m_Xd_fixed = Xd.col(0);
            m_Xd = m_Xd_fixed;
            m_goal_x = m_Xd_fixed;
            m_xStart = m_Xd_fixed;
            m_startFlag = true;
            ROS_INFO_STREAM("Changed into Force Controller, m_Xd = " << m_Xd.transpose());

            pthread_mutex_lock(&this->m_countMutex);
            m_totalTimeX = 10;
            pthread_mutex_unlock(&this->m_countMutex);
        }

        My::Matrix63d Xdes = m_trajgenF.GetCurrentStatesOnTrajectory6d(currentTime,m_targetTimeF);
        // x desired
        m_Xd = Xdes.col(0);
        m_Xd_p = Xdes.col(1);
        m_Xd_pp = Xdes.col(2);


        if(m_penForce > m_penForce_d && m_penForce < m_penForce_d*2){
        if(m_never_contact_before){
            m_never_contact_before = false;
            std_msgs::String msg;
            msg.data = "goal reached";
            m_goal_reached_pub.publish(msg);
        }
        
        m_Xd(2) = m_good_Z_pos;
        m_Xd_p(2) = 0;
        m_Xd_pp(2) = 0;
        m_calc_new_poly = false;
        }

        if(m_penForce < m_penForce_d){
            m_switchCaseForce = 0.0;
            if (!m_calc_new_poly){
                m_timeLeft = m_targetTimeF-time.tD();
                ROS_INFO_STREAM("time left:  " << m_timeLeft);
            }
            if(m_first_time_force){
                Vector6d goal_p;
                goal_p.setZero();
                m_trajgenF.CalculatePolynomial6d(m_xStart, m_goal_x, goal_p, goal_p, goal_p, goal_p, currentTime, m_targetTimeF);
                m_first_time_force = false;
            }
            // m_Xd_p.setZero();
            m_Xd_p(2) = -0.002;
            // m_Xd_pp.setZero();
            m_Xd_pp(2) = 0.0;
            m_good_Z_pos = X(2);
            m_calc_new_poly = true;
        }
        
        if(m_penForce > m_penForce_d*2){
            // ROS_INFO_STREAM("penforce bigger desired = " << m_penForce);
            m_switchCaseForce = -0.0;
            if (!m_calc_new_poly){
                m_timeLeft = m_targetTimeF-time.tD();
                ROS_INFO_STREAM("timeleft:  " << m_timeLeft);
            }
            if(m_first_time_force){
                Vector6d goal_p;
                goal_p.setZero();
                m_trajgenF.CalculatePolynomial6d(m_xStart, m_goal_x, goal_p, goal_p, goal_p, goal_p, currentTime, m_targetTimeF);
                m_first_time_force = false;
            }
            // m_Xd_p.setZero();
            m_Xd_p(2) = +0.002;
            // m_Xd_pp.setZero();
            m_Xd_pp(2) = 0.0;
            m_good_Z_pos = X(2);
            m_calc_new_poly = true;
        }


        // desired orientation as rotation matrix
        m_Rd =  AngleAxisd(m_Xd(3), Vector3d(1,0,0)) *
                AngleAxisd(m_Xd(4), Vector3d(0,1,0)) *
                AngleAxisd(m_Xd(5), Vector3d(0,0,1));

        // rotation between ef and desired position
        Matrix3d Re_d = Rot_EF.transpose() * m_Rd;

        // unit quaternion of Re_d with respect to base
        Quaterniond quat_e_de(Re_d);
        quat_e_de.normalize();
        Vector3d orientation_err = Rot_EF * quat_e_de.vec();

        double DeltaZ =  m_DeltaX[2];
        Vector6d DeltaX = X - m_Xd;

        if(m_calc_new_poly){
            DeltaX(2) = DeltaZ;
        }
        

        // ROS_INFO_STREAM(m_Xd_p(2) << DeltaX(2));

        DeltaX.bottomRows(3) = -orientation_err;
        // reference for PD-Like: Sq = JefInv * (X_p - Xr_p)
        Vector6d Xr_p = m_Xd_p - m_KpX * DeltaX;
        Vector6d Xr_pp= m_Xd_pp - m_KpX * (X_p - m_Xd_p);
        Vector6d Sx   = X_p - Xr_p;
        Vector6d Sq   = Jef_inv * Sx;

        // time delta since last step
        m_dt = time.tD() - m_timeLast;
        m_timeLast = time.tD();
        if(m_dt<=0){
            m_dt = 0;
        }

        // adaptive part: Qr_pp = JefInv * ( Xr_pp - JefDot * Qr_p)
        Vector6d Qr_p  = Jef_inv * Xr_p;
        Vector6d Qr_pp = Jef_inv * (Xr_pp - Jef_p * Qr_p);

        // only update if we did not change controller recently
        if (time.tD() - m_ctrlChangeTime > 2) {
            // update adaptive parameters
            dinamica.CalculateTheta(current.q, current.qp, Qr_p, Qr_pp, Sq, m_dt, m_gamma);
        }

        Vector6d adaptive =  dinamica.CalculateYr(Q, Q_p, Qr_p, Qr_pp) * dinamica.getTheta();
        Vector6d tau = -m_KdX * Sq + adaptive;

        // // just for message
        m_Sq = Sq;
        m_Sx = Sx;
        m_Qrp = Qr_p;
        m_Qrpp = Qr_pp;
        m_Xr_p = Xr_p;
        m_Xr_pp = Xr_pp;
        m_X = X;
        // m_X_p = X_p;
        m_DeltaX = DeltaX;

        // m_adaptive = adaptive;
        // m_tau = tau;

        return tau;
        
    }


    Vector6d BenderControl::FS_controller(const RobotTime &time, const JointState &current){
        
        std::string s = "~simple_effort_ctrl/penForce";
        ros::param::get(s, m_penForce_d);

        // q and q_p
        Vector6d Q   = current.q;
        Vector6d Q_p = current.qp;
        Vector6d Q_pp = current.qpp;

        // Jacobians
        Matrix6d Jef     = dinamica.JacobianEF(Q);
        Matrix6d Jef_inv = Jef.inverse();
        Matrix6d Jef_p   = dinamica.JacobianDotEF(Q, Q_p);
        Vector6d J_phi   = dinamica.JacobianPhi(Q);
        Vector6d J_phi_p = dinamica.JacobianPhiDot(Q, Q_p);

        // current cartesian positions, orientation in euler angles
        Vector6d X = dinamica.FKPosition(Q);
        Vector6d X_p = Jef * Q_p;
        Vector6d X_pp = Jef_p * Q_p + Jef * Q_pp;

        // current EF orientation
        Matrix3d Rot_EF = dinamica.rotationEF(Q);

        
        double currentTime = time.tD();
        if(!(m_startFlag) || m_ctrlChange) {
            pthread_mutex_lock(&this->m_countMutex);
            My::Matrix63d Xd = m_trajgenX.GetCurrentStatesOnTrajectory6d(currentTime,m_targetTimeX);
            Vector6d Xd_curr    = Xd.col(0);
            Vector6d Xd_p_curr  = Xd.col(1);
            Vector6d Xd_pp_curr  = Xd.col(2);
            m_goal_x = Xd_curr;
            m_startFlag = true;
            ROS_INFO_STREAM("start: " << Xd_curr.transpose());
            ROS_INFO_STREAM("goal:  " << m_goal_x.transpose());
            m_totalTimeX = 10;
            Vector6d goal_p;
            goal_p.setZero();
            m_targetTimeFC = m_totalTimeX + currentTime;
            m_trajgenFC.CalculatePolynomial6d(Xd_curr, m_goal_x, Xd_p_curr, goal_p, Xd_pp_curr, goal_p, currentTime, m_targetTimeFC);
            pthread_mutex_unlock(&this->m_countMutex);
        }

        My::Matrix63d Xdes = m_trajgenFC.GetCurrentStatesOnTrajectory6d(currentTime,m_targetTimeFC);

        // x desired
        m_Xd    = Xdes.col(0);
        m_Xd_p  = Xdes.col(1);
        m_Xd_pp = Xdes.col(2);

        // desired orientation as rotation matrix
        m_Rd =  AngleAxisd(m_Xd(3), Vector3d(1,0,0)) *
                AngleAxisd(m_Xd(4), Vector3d(0,1,0)) *
                AngleAxisd(m_Xd(5), Vector3d(0,0,1));

        // rotation between ef and desired position
        Matrix3d Re_d = Rot_EF.transpose() * m_Rd;

        // unit quaternion of Re_d with respect to base
        Quaterniond quat_e_de(Re_d);
        quat_e_de.normalize();
        Vector3d orientation_err = Rot_EF * quat_e_de.vec();

        Vector6d DeltaX = X - m_Xd;
        DeltaX.bottomRows(3) = -orientation_err;

        // time delta since last step
        m_dt = time.tD() - m_timeLast;
        m_timeLast = time.tD();
        if(m_dt<=0){
            m_dt = 0;
        }

        // Force Controller

        // Q = I - J_phi^T * J_phi
        Matrix6d CQ;
        CQ = Matrix6d::Identity(6,6) - J_phi*J_phi.transpose();
        ROS_INFO_STREAM(Jef*J_phi);

        double testfactor;
        testfactor = -0.5 * (X[2]*100+1.6);
        if (testfactor < 0){testfactor = 0;}
        // ROS_INFO_STREAM("testfactor: "<< testfactor);

        m_DeltaF += (m_penForce + testfactor - m_penForce_d) * m_dt;

        Vector6d qd_r = Jef_inv*(m_Xd_p - m_KpX * DeltaX);
        Vector6d qd_r_test = CQ*qd_r;

        // ROS_INFO_STREAM(qd_r - qd_r_test);

        Vector6d Qr_p = CQ*(qd_r) + m_beta * J_phi * m_DeltaF;
        Vector6d Sq   = Q_p - Qr_p;
        
        // Vector6d Xr_pp= m_Xd_pp - m_KpX * (X_p - m_Xd_p);
        
        // Vector6d Xr_pp = m_Xd_pp - m_KpX * (X_p - m_Xd_p);
        // Vector6d Qr_pp = Jef_inv * (Xr_pp - Jef_p * Qr_p)+ m_beta * (J_phi_p * m_DeltaF + J_phi * (m_penForce + testfactor - m_penForce_d));

        
        // adaptive part: Qr_pp = JefInv * ( Xr_pp - JefDot * Qr_p)
        // Vector6d Qr_pp = Jef_inv * (Xr_pp - Jef_p * Qr_p);


        // // only update if we did not change controller recently
        // if (time.tD() - m_ctrlChangeTime > 2) {
        //     // update adaptive parameterss
        //     dinamica.CalculateTheta(current.q, current.qp, Qr_p, Qr_pp, Sq, m_dt, m_gamma);
        // }
         
        // Vector6d adaptive =  dinamica.CalculateYr(Q, Q_p, Qr_p, Qr_pp) * dinamica.getTheta();
        Vector6d adaptive = m_adaptive;
        Vector6d tau = -m_KdX * Sq + adaptive;

        // just for message
        m_Sq = CQ*(m_Xd_p - m_KpX * DeltaX);
        m_Sx = Jef*J_phi;
        m_Qrp = Qr_p;
        // m_Qrpp = Qr_pp;
        // m_Xr_p = Xr_p;
        // m_Xr_pp = Xr_pp;
        m_X = X;
        m_X_p = X_p;
        m_DeltaX = DeltaX;

        m_adaptive = adaptive;
        m_tau = tau;

        return tau;

    }


    Vector6d BenderControl::update(const RobotTime &time, const JointState &current){
        
        // get controller from parameter server
        std::stringstream s;
        int controllerType;
        Vector6d goal_p;
        goal_p.setZero();
        Vector6d Q = current.q;
        Vector6d Q_p = current.qp;
        Vector6d Q_pp = current.qpp;

        CalculateForce();
        // butterworth();

        s<<"~simple_effort_ctrl/controller";
        ros::param::get(s.str(), controllerType);

        // for the first XX seconds, always use JS ctrl
        if (time.tD() < 15) {
            m_controllerType = JS_ADAPTIVE_CONTROLLER;
        } else {
            m_ctrlChange = false;
            if (m_controllerType != controllerType) {
                ROS_INFO_STREAM("Controller changed from " << m_controllerType << " to " << controllerType);
                m_ctrlChange = true;
                m_controllerType = controllerType;
                m_ctrlChangeTime = time.tD();

            }
        }
        
        if (m_goalChangeQ && (m_controllerType == JS_ADAPTIVE_CONTROLLER)) {
            ROS_INFO_STREAM("Replan trajectory with controller " << m_controllerType);
            m_goalChangeQ = false;
            double currentTime = time.tD();

            My::Matrix63d Qd = m_trajgenQ.GetCurrentStatesOnTrajectory6d(currentTime,m_targetTimeQ);
            Vector6d Qd_curr    = Qd.col(0);
            Vector6d Qd_p_curr  = Qd.col(1);
            Vector6d Qd_pp_curr  = Qd.col(2);

            m_targetTimeQ = m_totalTimeQ + currentTime;
            m_trajgenQ.CalculatePolynomial6d(Qd_curr ,m_goal_q, Qd_p_curr, goal_p, Qd_pp_curr, goal_p, time.tD(), m_targetTimeQ);
        }


        if (m_goalChangeX && (m_controllerType == OS_CONTROLLER)) {
            ROS_INFO_STREAM("Replan trajectory with controller " << m_controllerType);
            m_goalChangeX = false;

            double currentTime = time.tD();

            My::Matrix63d Xd = m_trajgenX.GetCurrentStatesOnTrajectory6d(currentTime,m_targetTimeX);
            Vector6d Xd_curr    = Xd.col(0);
            Vector6d Xd_p_curr  = Xd.col(1);
            Vector6d Xd_pp_curr  = Xd.col(2);
            m_targetTimeX = m_totalTimeX + time.tD();
            m_trajgenX.CalculatePolynomial6d(Xd_curr, m_goal_x, Xd_p_curr, goal_p, Xd_pp_curr, goal_p, time.tD(), m_targetTimeX);

            ROS_INFO_STREAM("Start: " << m_xStart.transpose());
        }

        if (m_goalChangeX && (m_controllerType == FS_CONTROLLER)) {
            ROS_INFO_STREAM("Replan trajectory with controller " << m_controllerType);
            m_goalChangeX = false;

            double currentTime = time.tD();

            My::Matrix63d Xd = m_trajgenFC.GetCurrentStatesOnTrajectory6d(currentTime,m_targetTimeFC);
            Vector6d Xd_curr    = Xd.col(0);
            Vector6d Xd_p_curr  = Xd.col(1);
            Vector6d Xd_pp_curr  = Xd.col(2);
            m_targetTimeFC = m_totalTimeX + time.tD();
            m_trajgenFC.CalculatePolynomial6d(Xd_curr, m_goal_x, Xd_p_curr, goal_p, Xd_pp_curr, goal_p, time.tD(), m_targetTimeFC);

            ROS_INFO_STREAM("Start: " << m_xStart.transpose());
        }

        if (m_goalChangeX && (m_controllerType == FS_SIMPLE_CONTROLLER)) {
            ROS_INFO_STREAM("Replan trajectory with controller " << m_controllerType);
            m_goalChangeX = false;

            double currentTime = time.tD();

            My::Matrix63d Xd = m_trajgenF.GetCurrentStatesOnTrajectory6d(currentTime,m_targetTimeF);
            Vector6d Xd_curr    = Xd.col(0);
            Vector6d Xd_p_curr  = Xd.col(1);
            Vector6d Xd_pp_curr  = Xd.col(2);
            
            m_goal_x(3) = m_Xd_fixed(3);
            m_goal_x(4) = m_Xd_fixed(4);
            m_goal_x(5) = m_Xd_fixed(5);
            pthread_mutex_lock(&this->m_countMutex);
            m_targetTimeF = m_totalTimeX + time.tD();
            pthread_mutex_unlock(&this->m_countMutex);
            m_trajgenF.CalculatePolynomial6d(Xd_curr, m_goal_x, Xd_p_curr, goal_p, Xd_pp_curr, goal_p, time.tD(), m_targetTimeF);

            ROS_INFO_STREAM("Start: " << Xd_curr.transpose());
            ROS_INFO_STREAM("Goal: " << m_goal_x.transpose());
            ROS_INFO_STREAM("time: " << time.tD());
            ROS_INFO_STREAM("Targettime: " << m_targetTimeF);
            m_ontheway = true;
        }
        
        m_manipulability = manipulabilityIndex(Q);
        double manipulabilityThreshold = 0.1;
        s.str("");
        s<<"~simple_effort_ctrl/manipulabilityThreshold";
        ros::param::get(s.str(), manipulabilityThreshold);
        
        // singularity avoidance
        if ((m_controllerType != JS_ADAPTIVE_CONTROLLER) && (m_manipulability < manipulabilityThreshold)){
            ROS_WARN_STREAM("Singularity close!");
            m_controllerType = JS_ADAPTIVE_CONTROLLER;
            m_ctrlChange = true;
            // m_targetTimeQ = 2;
            ROS_INFO_STREAM("Controller changed. Switching to JS controller");
            ros::param::set("~simple_effort_ctrl/controller", JS_ADAPTIVE_CONTROLLER);
            m_goal_q = Q;

            // m_trajgenQ.CalculatePolynomial6d(Q , Q, Q_p, goal_p, Q_pp, goal_p, time.tD(), m_targetTimeQ);
        }

        Vector6d tau;

        switch (m_controllerType) {
            // No controller - only Gravity
            case NO_CONTROLLER:
                tau = NoController();
                break;

            // Simple PD-Controller
            case SIMPLE_JS_PD_CONTROLLER:
                tau = JS_SimplePD(time, current);
                break;

            // Operational space controller
            case OS_CONTROLLER:
                tau = OS_controller(time,current);
                break;

            // Operational space controller
            case JS_ADAPTIVE_CONTROLLER:
                tau = JS_adaptiveController(time,current);
                m_counter++;
                break;

            // Operational space controller
            case FS_SIMPLE_CONTROLLER:
                tau = FS_simpleController(time,current);
                m_counter++;
                break;

            case FS_CONTROLLER:
                tau = FS_controller(time,current);
                m_counter++;
                break;

            default:
                tau.setZero();
                break;
        }

        ur10bender::DataController msg;

        msg.header.stamp=ros::Time::now();
        msg.time=time.tD();

        for(int i=0;i<STD_DOF;i++)
        {
          msg.q[i]  = current.q(i)*180/3.141;
          msg.qp[i] = current.qp(i)*180/3.141;
          msg.qpp[i]= current.qpp(i)*180/3.141;
          // ROS_INFO_STREAM(m_Qd(0,i));
          msg.qd[i]      = m_Qd(0,i)*180/3.141;
          msg.qpd[i]     = m_Qd(1,i)*180/3.141;
          msg.Dq[i]      = m_DeltaQ(i)*180/3.141;
          msg.Dqp[i]     = m_DeltaQp(i)*180/3.141;
          msg.Xef_0[i]   = m_X(i);
          msg.Xefp_0[i]   = m_X_p(i);
          msg.Xd_0[i]   = m_Xd(i);
          msg.Xdp_0[i]   = m_Xd_p(i);
          msg.Sq[i] = m_Sq(i);
          msg.Sx[i] = m_Sx(i);
          msg.Qrp[i] = m_Xr_p(i);
          msg.Qrpp[i] = m_Xr_pp(i);
          msg.torques[i] = tau(i) - m_adaptive(i);
          msg.tau_adap[i] = m_adaptive(i);
        }

        for(int i=0;i<3;i++){
          msg.Dx[i]   = m_DeltaX(i);
          msg.Dw[i]   = m_DeltaX(i+3)*180/3.141;
        }

        msg.penforce = m_penForce;
        msg.manipu = m_manipulability;

        for(int i=0;i<68;i++){
          msg.theta[i] = dinamica.getTheta()[i];
        }

        pubCtrlData.publish(msg);

        return tau;

    }


    bool BenderControl::start(){
    }

    bool BenderControl::stop()
    {
    }

  }
}
