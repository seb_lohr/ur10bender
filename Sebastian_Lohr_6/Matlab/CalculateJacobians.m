clear

syms q1 q2 q3 q1p q2p q3p q1pp q2pp q3pp L1 L2 L3 L4 L5 L6 L7 L8 L9 L10 real
syms I111 I112 I113 I122 I123 I133 I211 I212 I213 I222 I223 I233 I311 I312 I313 I322 I323 I333 real
syms m1 m2 m3 gx gy gz real

q = [q1; q2; q3];
qp = [q1p; q2p; q3p];
qpp = [q1pp; q2pp; q3pp];
    
%% Calculate homogeneous transformations
H1_0 = GenerateH(q1, L1, 0, sym(pi)/2);
H2_1 = GenerateH(q2,L7,-L3,sym(pi));
H3_2 = GenerateH(q3,-L4-L2+L7,-L5,0);

Hcm1_0 = GenerateH(q1, L1, 0, 0);
Hcm2_1 = GenerateH(q2,L7,-L8,0);
Hcm3_2 = GenerateH(q3,L7-L9,-L10,0);


H1_0 = simplify(H1_0);
Hcm1_0 = simplify(Hcm1_0);

H2_0 = simplify(H1_0 * H2_1);
Hcm2_0 = simplify(H1_0 * Hcm2_1);

H3_0 = simplify(H1_0 * H2_1 * H3_2);
Hcm3_0 = simplify(H1_0 * H2_1 * Hcm3_2);


%% Calculate Jacobians
Jcm1_0_1 = [cross([0;0;1],(Hcm1_0(1:3,4)));[0;0;1]];

Jcm1_0 = [Jcm1_0_1, zeros(6,2)];

Jcm1_0_v = Jcm1_0(1:3,:);
Jcm1_0_w = Jcm1_0(4:6,:);


Jcm2_0_1 = [cross([0;0;1],(Hcm2_0(1:3,4)));[0;0;1]];
Jcm2_0_2 = [cross(H1_0(1:3,3),(Hcm2_0(1:3,4)-H1_0(1:3,4)));H1_0(1:3,3)];

Jcm2_0 = [Jcm2_0_1, Jcm2_0_2, zeros(6,1)];

Jcm2_0_v = Jcm2_0(1:3,:);
Jcm2_0_w = Jcm2_0(4:6,:);


Jcm3_0_1 = [cross([0;0;1],(Hcm3_0(1:3,4)));[0;0;1]];
Jcm3_0_2 = [cross(H1_0(1:3,3), (Hcm3_0(1:3,4) - H1_0(1:3,4))); H1_0(1:3,3)];
Jcm3_0_3 = [cross(H2_0(1:3,3), (Hcm3_0(1:3,4) - H2_0(1:3,4))); H2_0(1:3,3)];

Jcm3_0 = simplify([Jcm3_0_1 , Jcm3_0_2, Jcm3_0_3]);

Jcm3_0_v = Jcm3_0(1:3,:);
Jcm3_0_w = Jcm3_0(4:6,:);

J3_0_1 = [cross([0;0;1],(H3_0(1:3,4)));[0;0;1]];
J3_0_2 = [cross(H1_0(1:3,3), (H3_0(1:3,4) - H1_0(1:3,4))); H1_0(1:3,3)];
J3_0_3 = [cross(H2_0(1:3,3), (H3_0(1:3,4) - H2_0(1:3,4))); H2_0(1:3,3)];

J3_0 = simplify([J3_0_1 , J3_0_2, J3_0_3]);

J3_0_v = J3_0(1:3,:);
J3_0_w = J3_0(4:6,:);


%% Inertia Matrices

I1 = [I111, I112, I113;
      I112, I122, I123;
      I113, I123, I133];
  
I2 = [I211, I212, I213;
      I212, I222, I223;
      I213, I223, I233];
  
I3 = [I311, I312, I313;
      I312, I322, I323;
      I313, I323, I333];


 %% Rotation Matrices
 
Rcm1_0 = Hcm1_0(1:3,1:3);
Rcm2_0 = Hcm2_0(1:3,1:3);
Rcm3_0 = Hcm3_0(1:3,1:3);

%% Translation Vectors
tcm1_0 = Hcm1_0(1:3,4);
tcm2_0 = Hcm2_0(1:3,4);
tcm3_0 = Hcm3_0(1:3,4);


%% M-Matrix 
% M1 = simplify(expand(m1*transpose(Jcm1_0_v)*Jcm1_0_v + transpose(Jcm1_0_w)*Rcm1_0*I1*transpose(Rcm1_0)*Jcm1_0_w));
% M2 = simplify(expand(m2*transpose(Jcm2_0_v)*Jcm2_0_v + transpose(Jcm2_0_w)*Rcm2_0*I2*transpose(Rcm2_0)*Jcm2_0_w));
% M3 = simplify(expand(m3*transpose(Jcm3_0_v)*Jcm3_0_v + transpose(Jcm3_0_w)*Rcm3_0*I3*transpose(Rcm3_0)*Jcm3_0_w));
% 
% M = simplify(M1 + M2 + M3);


M1 = simplify(expand(m1 * (Jcm1_0_v' * Jcm1_0_v) + Jcm1_0_w' * Rcm1_0 * I1 * Rcm1_0' * Jcm1_0_w));
M2 = simplify(expand(m2 * (Jcm2_0_v' * Jcm2_0_v) + Jcm2_0_w' * Rcm2_0 * I2 * Rcm2_0' * Jcm2_0_w));
M3 = simplify(expand(m3 * (Jcm3_0_v' * Jcm3_0_v) + Jcm3_0_w' * Rcm3_0 * I3 * Rcm3_0' * Jcm3_0_w));

M = simplify(M1 + M2 + M3);

%% C-Matrix

for k=1:3 
    for j=1:3 
        C(k,j) = simplify(0.5 * ((diff(M(k,j),q1) + diff(M(k,1), q(j)) - diff(M(1,j), q(k))) * q1p + ...
                                 (diff(M(k,j),q2) + diff(M(k,2), q(j)) - diff(M(2,j), q(k))) * q2p + ...
                                 (diff(M(k,j),q3) + diff(M(k,3), q(j)) - diff(M(3,j), q(k))) * q3p)); 
    end 
end
C = simplify(C);

%% Checking skew symmetry
M - transpose(M);

Md =  simplify(diff(M,q1)*q1p + diff(M,q2)*q2p + diff(M,q3)*q3p);
N = simplify(Md - 2*C);
simplify(N + transpose(N));

%% G-Matrix

grav = [gx; gy; gz];
P = m1*transpose(grav)*tcm1_0 + m2*transpose(grav)*tcm2_0 + m3*transpose(grav)*tcm3_0;

for k=1:3 
    G(k,1) = diff(P, q(k));
end







