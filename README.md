# Ur10 Bender

Draw like bender.

## USB Cam object detection

- Info for driver: http://wiki.ros.org/libuvc_camera
- Specs of camera: https://support.logitech.com/en_us/product/webcam-pro-9000/specs

### Instructions to install

1. Install camera driver: `sudo apt install ros-kinetic-libuvc-camera`
2. Setup udev rule to allow read on camera: `sudo nano /etc/udev/rules.d/99-uvc.rules`. Content:

```
    # UVC cameras
    SUBSYSTEMS=="usb", ENV{DEVTYPE}=="usb_device", ATTRS{idVendor}=="046d", ATTRS{idProduct}=="0809", MODE="0666"
```

3. Launch file for driver: `roslaunch camera_launcher usb_cam.launch`
4. Run `find_goal` node: `roslaunch find_goal find_goal.launch`
chr