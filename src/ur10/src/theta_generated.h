#pragma once

#include <Eigen/Eigen>
#include<tumtools/Math/EigenDefs.h>

typedef ModelT double;

namespace Dinamica{
const int DIMENSION_1 = 94;
const int DIMENSION_2 = 1;
using Container = Eigen::Matrix<ModelT, 94, 1>;



void update(Container& out)
{
    
    ModelT t2 = I6_y*(5.0/2.0);
    ModelT t3 = a2*cmOffsets5_3*m5;
    ModelT t4 = a2*cmOffsets6_3*m6;
    ModelT t5 = a2*d6*m6;
    ModelT t6 = a2*endeffector_z_offset*m6;
    ModelT t7 = a3*cmOffsets5_3*m5;
    ModelT t8 = a3*cmOffsets6_3*m6;
    ModelT t9 = a3*d6*m6;
    ModelT t10 = a3*endeffector_z_offset*m6;
    ModelT t11 = d5*d5;
    ModelT t12 = cmOffsets5_3*d5*m5;
    ModelT t13 = cmOffsets6_3*d5*m6;
    ModelT t14 = d5*d6*m6;
    ModelT t15 = d5*endeffector_z_offset*m6;
    ModelT t16 = a3*a3;
    ModelT t17 = cmOffsets6_3*cmOffsets6_3;
    ModelT t18 = d6*d6;
    ModelT t19 = endeffector_z_offset*endeffector_z_offset;
    ModelT t20 = a2*a2;
    ModelT t21 = d4*d4;
    ModelT t22 = cmOffsets6_1*cmOffsets6_1;
    ModelT t23 = cmOffsets6_2*cmOffsets6_2;
    ModelT t24 = cmOffsets5_3*d5*m5*(1.0/2.0);
    ModelT t25 = cmOffsets5_3*d4*m5;
    ModelT t26 = cmOffsets6_3*d4*m6;
    ModelT t27 = d4*d6*m6;
    ModelT t28 = d4*endeffector_z_offset*m6;
    out(0,0) =  I1_y;
    out(1,0) =  I2_x;
    out(2,0) =  I2_y;
    out(3,0) =  I3_x;
    out(4,0) =  I3_y;
    out(5,0) =  I4_x;
    out(6,0) =  I5_x;
    out(7,0) =  I6_x;
    out(8,0) =  I5_z;
    out(9,0) =  I6_y;
    out(10,0) =  I6_z;
    out(11,0) =  t12-cmOffsets5_2*cmOffsets5_3*m5;
    out(12,0) =  (cmOffsets5_3*cmOffsets5_3)*m5;
    out(13,0) =  m6*t22;
    out(14,0) =  m6*t23;
    out(15,0) =  -I4_z;
    out(16,0) =  -I5_y;
    out(17,0) =  I5_y*(1.0/2.0);
    out(18,0) =  I5_y*(3.0/2.0);
    out(19,0) =  I5_z*(1.0/2.0)+I6_z*(1.0/2.0);
    out(20,0) =  I6_x*(-5.0/2.0)+t2;
    out(21,0) =  I6_x*(5.0/2.0)-t2;
    out(22,0) =  I4_z+I5_y;
    out(23,0) =  t4+t5+t6;
    out(24,0) =  t8+t9+t10;
    out(25,0) =  a2*cmOffsets6_3*m6*(-1.0/2.0)-a2*d6*m6*(1.0/2.0)-a2*endeffector_z_offset*m6*(1.0/2.0);
    out(26,0) =  a2*cmOffsets6_3*m6*(1.0/2.0)+a2*d6*m6*(1.0/2.0)+a2*endeffector_z_offset*m6*(1.0/2.0);
    out(27,0) =  a3*cmOffsets6_3*m6*(-1.0/2.0)-a3*d6*m6*(1.0/2.0)-a3*endeffector_z_offset*m6*(1.0/2.0);
    out(28,0) =  a3*cmOffsets6_3*m6*(1.0/2.0)+a3*d6*m6*(1.0/2.0)+a3*endeffector_z_offset*m6*(1.0/2.0);
    out(29,0) =  cmOffsets6_1*cmOffsets6_3*m6+cmOffsets6_1*d6*m6+cmOffsets6_1*endeffector_z_offset*m6;
    out(30,0) =  cmOffsets6_2*cmOffsets6_3*m6+cmOffsets6_2*d6*m6+cmOffsets6_2*endeffector_z_offset*m6;
    out(31,0) =  cmOffsets6_1*cmOffsets6_3*m6*(-1.0/2.0)-cmOffsets6_1*d6*m6*(1.0/2.0)-cmOffsets6_1*endeffector_z_offset*m6*(1.0/2.0);
    out(32,0) =  cmOffsets6_1*cmOffsets6_3*m6*(1.0/2.0)+cmOffsets6_1*d6*m6*(1.0/2.0)+cmOffsets6_1*endeffector_z_offset*m6*(1.0/2.0);
    out(33,0) =  cmOffsets6_2*cmOffsets6_3*m6*(-1.0/2.0)-cmOffsets6_2*d6*m6*(1.0/2.0)-cmOffsets6_2*endeffector_z_offset*m6*(1.0/2.0);
    out(34,0) =  cmOffsets6_2*cmOffsets6_3*m6*(1.0/2.0)+cmOffsets6_2*d6*m6*(1.0/2.0)+cmOffsets6_2*endeffector_z_offset*m6*(1.0/2.0);
    out(35,0) =  t26+t27+t28;
    out(36,0) =  t13+t14+t15;
    out(37,0) =  cmOffsets6_3*d5*m6*(-1.0/2.0)-d5*d6*m6*(1.0/2.0)-d5*endeffector_z_offset*m6*(1.0/2.0);
    out(38,0) =  cmOffsets6_3*d5*m6*(1.0/2.0)+d5*d6*m6*(1.0/2.0)+d5*endeffector_z_offset*m6*(1.0/2.0);
    out(39,0) =  I5_x*-2.0+I5_z*2.0+I6_z*2.0;
    out(40,0) =  t3;
    out(41,0) =  t7;
    out(42,0) =  a2*cmOffsets6_1*m6;
    out(43,0) =  a2*cmOffsets6_2*m6;
    out(44,0) =  a3*cmOffsets6_1*m6;
    out(45,0) =  a3*cmOffsets6_2*m6;
    out(46,0) =  cmOffsets6_1*cmOffsets6_2*m6;
    out(47,0) =  t25;
    out(48,0) =  cmOffsets6_1*d4*m6;
    out(49,0) =  cmOffsets6_1*d5*m6;
    out(50,0) =  cmOffsets6_2*d4*m6;
    out(51,0) =  cmOffsets6_2*d5*m6;
    out(52,0) =  a2*cmOffsets4_3*m4-a2*cmOffsets5_2*m5+a2*d5*m5+a2*d5*m6;
    out(53,0) =  a3*cmOffsets4_3*m4-a3*cmOffsets5_2*m5+a3*d5*m5+a3*d5*m6;
    out(54,0) =  -t3-t4-t5-t6;
    out(55,0) =  -t7-t8-t9-t10;
    out(56,0) =  cmOffsets5_3*d4*m5*2.0+cmOffsets6_3*d4*m6*2.0+d4*d6*m6*2.0+d4*endeffector_z_offset*m6*2.0;
    out(57,0) =  -a2*a3*m3-a2*a3*m4-a2*a3*m5-a2*a3*m6-a2*cmOffsets3_1*m3;
    out(58,0) =  m5*t11+m6*t11+(cmOffsets4_3*cmOffsets4_3)*m4+(cmOffsets5_2*cmOffsets5_2)*m5-cmOffsets5_2*d5*m5*2.0;
    out(59,0) =  cmOffsets4_2*cmOffsets4_3*m4+cmOffsets4_3*d4*m4-cmOffsets5_2*d4*m5+d4*d5*m5+d4*d5*m6;
    out(60,0) =  -t12-t13-t14-t15+cmOffsets5_2*cmOffsets5_3*m5;
    out(61,0) =  m3*t16+m4*t16+m5*t16+m6*t16+(cmOffsets3_1*cmOffsets3_1)*m3+a3*cmOffsets3_1*m3*2.0;
    out(62,0) =  -a3*cmOffsets3_3*m3-a3*cmOffsets4_2*m4-a3*d4*m4-a3*d4*m5-a3*d4*m6-cmOffsets3_1*cmOffsets3_3*m3;
    out(63,0) =  m6*t17+m6*t18+m6*t19+cmOffsets6_3*d6*m6*2.0+cmOffsets6_3*endeffector_z_offset*m6*2.0+d6*endeffector_z_offset*m6*2.0;
    out(64,0) =  m6*t17*(1.0/2.0)+m6*t18*(1.0/2.0)+m6*t19*(1.0/2.0)+cmOffsets6_3*d6*m6+cmOffsets6_3*endeffector_z_offset*m6+d6*endeffector_z_offset*m6;
    out(65,0) =  m2*t20+m3*t20+m4*t20+m5*t20+m6*t20+(cmOffsets2_1*cmOffsets2_1)*m2+a2*cmOffsets2_1*m2*2.0;
    out(66,0) =  -a2*cmOffsets2_3*m2-a2*cmOffsets3_3*m3-a2*cmOffsets4_2*m4-a2*d4*m4-a2*d4*m5-a2*d4*m6-cmOffsets2_1*cmOffsets2_3*m2;
    out(67,0) =  m4*t21+m5*t21+m6*t21+(cmOffsets1_1*cmOffsets1_1)*m1+(cmOffsets1_3*cmOffsets1_3)*m1+(cmOffsets2_3*cmOffsets2_3)*m2+(cmOffsets3_3*cmOffsets3_3)*m3+(cmOffsets4_2*cmOffsets4_2)*m4+cmOffsets4_2*d4*m4*2.0;
    out(68,0) =  I2_z;
    out(69,0) =  I3_z;
    out(70,0) =  I4_y;
    out(71,0) =  I5_y;
    out(72,0) =  cmOffsets6_1*m6;
    out(73,0) =  -I2_x+I2_y;
    out(74,0) =  -I3_x+I3_y;
    out(75,0) =  -I4_x+I4_z;
    out(76,0) =  m6*t22*-2.0+m6*t23*2.0;
    out(77,0) =  t24-cmOffsets5_2*cmOffsets5_3*m5*(1.0/2.0);
    out(78,0) =  -t24+cmOffsets5_2*cmOffsets5_3*m5*(1.0/2.0);
    out(79,0) =  cmOffsets5_2*cmOffsets5_3*m5*(1.0/4.0)-cmOffsets5_3*d5*m5*(1.0/4.0);
    out(80,0) =  I6_x*-6.0+I6_y*6.0;
    out(81,0) =  -cmOffsets6_2*m6;
    out(82,0) =  cmOffsets4_3*m4-cmOffsets5_2*m5+d5*m5+d5*m6;
    out(83,0) =  -cmOffsets5_3*m5-cmOffsets6_3*m6-d6*m6-endeffector_z_offset*m6;
    out(84,0) =  t25+t26+t27+t28;
    out(85,0) =  a3*m3+a3*m4+a3*m5+a3*m6+cmOffsets3_1*m3;
    out(86,0) =  a2*m2+a2*m3+a2*m4+a2*m5+a2*m6+cmOffsets2_1*m2;
    out(87,0) =  cmOffsets6_2*m6;
    out(88,0) =  cmOffsets5_2*cmOffsets5_3*m5*(3.0/2.0)-cmOffsets5_3*d5*m5*(3.0/2.0);
    out(89,0) =  I6_x*-8.0+I6_y*8.0;
    out(90,0) =  a2*cmOffsets6_3*m6*(-3.0/2.0)-a2*d6*m6*(3.0/2.0)-a2*endeffector_z_offset*m6*(3.0/2.0);
    out(91,0) =  a3*cmOffsets6_3*m6*(-3.0/2.0)-a3*d6*m6*(3.0/2.0)-a3*endeffector_z_offset*m6*(3.0/2.0);
    out(92,0) =  cmOffsets6_3*d5*m6*(3.0/2.0)+d5*d6*m6*(3.0/2.0)+d5*endeffector_z_offset*m6*(3.0/2.0);
    out(93,0) =  m6*t17*(3.0/2.0)+m6*t18*(3.0/2.0)+m6*t19*(3.0/2.0)+cmOffsets6_3*d6*m6*3.0+cmOffsets6_3*endeffector_z_offset*m6*3.0+d6*endeffector_z_offset*m6*3.0;

}
} // namespace Dinamica
