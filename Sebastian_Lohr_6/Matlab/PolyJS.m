function a = CalculatePolynomials(u)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
%Current Joint Position

q=u(1);

%Current Joint Velocity
qp=u(2);

%Destination Joint Position
q_t=u(3);

%Destination Joint Velocity
qp_t=u(4);

%Time
ti=u(5);
tf=u(6);



%% 

x = [q;q_t;qp;qp_t;0;0];

T = ...
    [1, ti, ti^2, ti^3, ti^4, ti^5;
     1, tf, tf^2, tf^3, tf^4, tf^5;
     0,  1, 2*ti, 3*ti^2, 4*ti^3, 5*ti^4;
     0,  1, 2*tf, 3*tf^2, 4*tf^3, 5*tf^4;
     0,  0, 2, 6*ti, 12*ti^2, 20*ti^3;
     0,  0, 2, 6*tf, 12*tf^2, 20*tf^3];

 
a = T\x;

