#ifndef CONTROL_H
#define CONTROL_H

#include <geometry_msgs/WrenchStamped.h>

#include<tum_ics_ur_robot_lli/RobotControllers/ControlEffort.h>
#include"../include/Dinamica.h"

#include "../include/matrixdefs.h"
#include "../include/TrajGen.h"

#include"ur10bender/GoalQ.h"
#include"ur10bender/GoalX.h"
#include"ur10bender/DrawGoals.h"
#include"ur10bender/CtrlGain.h"

#define NO_CONTROLLER 0
#define SIMPLE_JS_PD_CONTROLLER 1
#define JS_ADAPTIVE_CONTROLLER 2
#define OS_CONTROLLER 3
#define FS_SIMPLE_CONTROLLER 4
#define FS_CONTROLLER 5


namespace tum_ics_ur_robot_lli{
namespace RobotControllers{

class BenderControl: public ControlEffort
{
    // Member variables
private:

    bool m_startFlag;

    Vector6d m_qStart;
    Vector6d m_xStart;

    JointState m_qInit;
    JointState m_qHome;
    JointState m_qPark;

    Vector6d m_goal_q;
    Vector6d m_goal_x;
    std::vector<Vector6d> m_drawGoals;

    ros::NodeHandle n;
    ros::Publisher pubCtrlData;
    ros::Publisher plotter;

    Matrix6d m_Kp;
    Matrix6d m_Kd;
    Matrix6d m_Ki;

    Matrix6d m_KpX;
    Matrix6d m_KdX;

    double m_totalTimeQ;
    double m_totalTimeX;

    double m_targetTimeQ;
    double m_targetTimeX;
    double m_targetTimeF;
    double m_targetTimeFC;

    double m_gamma;
    double m_beta;

    double m_manipulability;

    Vector6d m_DeltaQ;
    Vector6d m_DeltaQp;
    Vector6d m_iDeltaQ;
    Vector6d m_DeltaX;
    double m_DeltaF;

    Vector6d m_X;
    Vector6d m_X_p;

    Matrix3d m_Rd;

    My::Matrix63d m_Qd;
    Vector6d m_Xd;
    Vector6d m_Xd_p;
    Vector6d m_Xd_pp;

    Vector6d m_Qrp;
    Vector6d m_Qrpp;

    Vector6d m_Xr_p;
    Vector6d m_Xr_pp;


    Vector6d m_Sq;
    Vector6d m_Sx;
    Vector6d m_adaptive;

    Vector3d m_Xw_Drawing;

    // force and torque with history
    std::vector<Vector3d> m_Force;
    std::vector<Vector3d> m_Torque;

    double m_penForce;
    double m_penForce_d;
    double m_good_Z_pos;
    bool m_calc_new_poly;
    bool m_ontheway;
    bool m_never_contact_before;
    double m_totalTimeF;
    double m_timeLeft;
    bool m_first_time_force;
    Vector6d m_Xd_fixed;
    double m_switchCaseForce;
    double m_dt;
    double m_timeLast;


    bool m_goalChangeQ;
    bool m_goalChangeX;
    bool m_ctrlChange;
    double m_ctrlChangeTime;
    int m_controllerType;

    ros::Subscriber m_goalQ_sub;
    ros::Subscriber m_goalX_sub;
    ros::Subscriber m_drawgoals_sub;
    ros::Subscriber m_gain_sub;
    ros::Subscriber m_force_sub;
    ros::Publisher m_goal_reached_pub;

    int m_counter;

    double m_controlPeriod;     //[s]
    double m_controlPeriod_2;   //[s]

    Dinamica dinamica;
    TrajGen m_trajgenQ;
    TrajGen m_trajgenX;
    TrajGen m_trajgenF;
    TrajGen m_trajgenFC;

    pthread_mutex_t m_countMutex;

    // Member Methods
public:
    BenderControl(double weight=1.0,
                        const QString& name="SimpleEffortCtrl");
    ~BenderControl();

    void setQInit(const JointState& qinit);
    void setQHome(const JointState& qhome);
    void setQPark(const JointState& qpark);
    void setQGoal(const Vector6d& qgoal);

    void QGoalCalllback(const ur10bender::GoalQConstPtr& msg);
    void XGoalCalllback(const ur10bender::GoalXConstPtr& msg);
    void DrawGoalCalllback(const ur10bender::DrawGoalsConstPtr& msg);
    void GainCalllback(const ur10bender::CtrlGainConstPtr& msg);
    void WrenchCallback(const geometry_msgs::WrenchStampedConstPtr& msg);

    // Member Methods
private:
    bool init();
    bool start();
    Vector6d update(const RobotTime& time, const JointState &current);
    bool stop();

    //Own functions
    Vector6d NoController();
    Vector6d JS_SimplePD(const RobotTime &time, const JointState &current);
    Vector6d OS_SimplePD(const RobotTime &time, const JointState &current);
    Vector6d JS_adaptiveController(const RobotTime &time, const JointState &current);
    Vector6d OS_controller(const RobotTime &time, const JointState &current);
    Vector6d FS_simpleController(const RobotTime &time, const JointState &current);
    Vector6d FS_controller(const RobotTime &time, const JointState &current);

    void CalculateForce();
    double manipulabilityIndex(Tum::Vector6d Q);
    void butterworth();
};

}
}


#endif // CONTROL_H
