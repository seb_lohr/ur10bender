#ifndef SHAPEDETECTION_H
#define SHAPEDETECTION_H

// ROS INCLUDES //
#include <ros/ros.h>
#include <image_transport/image_transport.h>

#include "opencv2/imgproc.hpp"

// MSG INCLUDES
#include "find_goal/Shape.h"
#include "find_goal/ShapeRequest.h"
#include "find_goal/ShapeResponse.h"

namespace shapeDetection {
class shapeDetection
{
private:

    image_transport::Subscriber m_sub_camera;
    ros::ServiceServer m_shape_service;

    cv::Mat m_image;
    cv::Mat m_binaryImg;
    cv::Mat m_dist;
    cv::Mat m_camMat;
    cv::Mat m_TW_Cam;
    std::vector<double> m_cannyThresholds;
    cv::Rect m_roi;

    std::vector<geometry_msgs::Point> m_shapeLocation;
    pthread_mutex_t m_countMutexSL;
    pthread_mutex_t m_countMutexImg;

public:
    shapeDetection();
    ~shapeDetection();

    void init();


    void getImageCallback(const sensor_msgs::ImageConstPtr &msg);
    void getExtrinsics(cv::Mat binaryImg);

    void setShapeLocation(std::vector<cv::Point3d> shapeLocation);

    cv::Point3d getWorldLocation(cv::Point img_point, cv::Mat trafo);
    cv::Point getImgLocation(cv::Mat world_point, cv::Mat trafo);
    
    std::vector<cv::Point> getShape(bool display);
    std::vector<cv::Point3d> getWorldLocations(std::vector<cv::Point> shape);

    std::vector<double> getParam(std::string name);
    cv::Mat getImage();
    bool shapeLocationCallback(find_goal::ShapeRequest& req,
                                    find_goal::ShapeResponse& res);
};

}
#endif //SHAPEDETECTION_H

