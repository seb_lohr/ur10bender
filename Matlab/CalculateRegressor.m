%CALCULATEREGRESSOR Summary of this function goes here
%   Detailed explanation goes here
    
    
    syms q1 q2 q3 q4 q5 q6 q1p q2p q3p q4p q5p q6p q1pp q2pp q3pp q4pp q5pp q6pp real

    syms L1 L2 L3 L4 L5 L6 L7 L8 L9 L10 L11 L12 real
    syms I111 I112 I113 I122 I123 I133 I211 I212 I213 I222 I223 I233 I311 I312 I313 I322 I323 I333 I411 I412 I413 I422 I423 I433 I511 I512 I513 I522 I523 I533 I611 I612 I613 I622 I623 I633 real
    syms m1 m2 m3 m4 m5 m6 gx gy gz real


    syms q1p_r q2p_r q3p_r q4p_r q5p_r q6p_r q1pp_r q2pp_r q3pp_r q4pp_r q5pp_r q6pp_r;

    I = [I111 I112 I113 I122 I123 I133 I211 I212 I213 I222 I223 I233 I311 I312 I313 I322 I323 I333 I411 I412 I413 I422 I423 I433 I511 I512 I513 I522 I523 I533 I611 I612 I613 I622 I623 I633];
    L = [L1 L2 L3 L4 L5 L6 L7 L8 L9 L10 L11 L12];

    m = [m1 m2 m3 m4 m5 m6]; 
    g = [gx gy gz];

    qp_r = [q1p_r; q2p_r; q3p_r; q4p_r; q5p_r; q6p_r];
    qpp_r = [q1pp_r; q2pp_r; q3pp_r; q4pp_r; q5pp_r; q6pp_r];

    EqM = expand(M * qpp_r + C*qp_r + G);
    
    VectorPrint(EqM, 'Eqm');
    
    %read file
%     text = fileread('EquationofMotion.txt');
    text = fileread('Prints/Eqm.txt');
    
    %split string into lines 
    expression = '\n';
    rowsplit = regexp(text, expression,'split');
    
    
    %split each line into terms
    
    for i=1:6
        rowsplit(i) = strrep(rowsplit(i),'- ','+-');
    end

    expression = '+';
    splitStr_1 = regexp(string(rowsplit(1)),expression,'split');
    splitStr_2 = regexp(string(rowsplit(2)),expression,'split');
    splitStr_3 = regexp(string(rowsplit(3)),expression,'split');
    splitStr_4 = regexp(string(rowsplit(4)),expression,'split');
    splitStr_5 = regexp(string(rowsplit(5)),expression,'split');
    splitStr_6 = regexp(string(rowsplit(6)),expression,'split');

   
    %remove spaces at beginning and end
    for i=1:size(splitStr_1,2)
        splitStr_1(i) = strip(splitStr_1(i));
    end
    for i=1:size(splitStr_2,2)
        splitStr_2(i) = strip(splitStr_2(i));
    end
    for i=1:size(splitStr_3,2)
        splitStr_3(i) = strip(splitStr_3(i));
    end
    for i=1:size(splitStr_4,2)
        splitStr_4(i) = strip(splitStr_4(i));
    end
    for i=1:size(splitStr_5,2)
        splitStr_5(i) = strip(splitStr_5(i));
    end
    for i=1:size(splitStr_6,2)
        splitStr_6(i) = strip(splitStr_6(i));
    end
    
    paramStr_1 = string(ones(1,size(splitStr_1,2)));
    paramStr_2 = string(ones(1,size(splitStr_2,2)));
    paramStr_3 = string(ones(1,size(splitStr_3,2)));
    paramStr_4 = string(ones(1,size(splitStr_4,2)));
    paramStr_5 = string(ones(1,size(splitStr_5,2)));
    paramStr_6 = string(ones(1,size(splitStr_6,2)));
    
    %% for first row
    
    for i=1:size(L,2)
        expression = strcat(string(L(i)),'.2');
        for j=1:size(splitStr_1,2)
            [match,noMatch] = regexp(splitStr_1(j),expression,'match','split');
            if not(isempty(match))
                paramStr_1(j) = strcat(paramStr_1(j),'*',match);
                splitStr_1(j) = strjoin(noMatch);
                strrep(splitStr_1(j),'**','*');
            end
        end
    end
    
    for i=size(L,2):1
        expression = string(L(i));
        for j=1:size(splitStr_1,2)
            [match,noMatch] = regexp(splitStr_1(j),expression,'match','split');
            if not(isempty(match))
                paramStr_1(j) = strcat(paramStr_1(j),'*',match);
                splitStr_1(j) = strjoin(noMatch);
                strrep(splitStr_1(j),'**','*');
            end
        end
    end
    
    for i=1:size(I,2)
        expression = string(I(i));
        for j=1:size(splitStr_1,2)
            [match,noMatch] = regexp(splitStr_1(j),expression,'match','split');
            if not(isempty(match))
                paramStr_1(j) = strcat(paramStr_1(j),'*',match);
                splitStr_1(j) = strjoin(noMatch);
                strrep(splitStr_1(j),'**','*');
            end
        end
    end
    
    for i=1:size(g,2)
        expression = string(g(i));
        for j=1:size(splitStr_1,2)
            [match,noMatch] = regexp(splitStr_1(j),expression,'match','split');
            if not(isempty(match))
                paramStr_1(j) = strcat(paramStr_1(j),'*',match);
                splitStr_1(j) = strjoin(noMatch);
                strrep(splitStr_1(j),'**','*');
            end
        end
    end
    
    for i=1:size(m,2)
        expression = string(m(i));
        for j=1:size(splitStr_1,2)
            [match,noMatch] = regexp(splitStr_1(j),expression,'match','split');
            if not(isempty(match))
                paramStr_1(j) = strcat(paramStr_1(j),'*',match);
                splitStr_1(j) = strjoin(noMatch);
                strrep(splitStr_1(j),'**','*');
            end
        end
    end
    splitStr_1 = strrep(splitStr_1,' *','');

    for j=1:size(paramStr_1,2)
        cellContents = paramStr_1{j};    
        paramStr_1{j} = cellContents(3:end);
    end

    for j=1:size(splitStr_1,2)
        if  splitStr_1{j}(1) ~= '-'
            splitStr_1(j) = strcat('+',splitStr_1(j));
        end
    end

    
    k = 1;
    Theta = string();
    Y_r = string();
    
    for i=1:size(paramStr_1,2)
        if any(strcmp(Theta,paramStr_1(i)))
            for j=1:size(Theta,2)
                if strcmp(Theta(j),paramStr_1(i))
                    Y_r(1,j) = strcat(Y_r(1,j),splitStr_1(i));
                    break
                end
            end 
        else
            Theta(k) = paramStr_1(i);
            Y_r(1,k) = splitStr_1(i);
            k=k+1;
        end
    end
    
    %% For second row
    
    Y_r(2,1:size(Theta,2)) = 0;
    
    for i=1:size(L,2)
        expression = strcat(string(L(i)),'.2');
        for j=1:size(splitStr_2,2)
            [match,noMatch] = regexp(splitStr_2(j),expression,'match','split');
            if not(isempty(match))
                paramStr_2(j) = strcat(paramStr_2(j),'*',match);
                splitStr_2(j) = strjoin(noMatch);
                strrep(splitStr_2(j),'**','*');
            end
        end
    end
    
    for i=size(L,2):1
        expression = string(L(i));
        for j=1:size(splitStr_2,2)
            [match,noMatch] = regexp(splitStr_2(j),expression,'match','split');
            if not(isempty(match))
                paramStr_2(j) = strcat(paramStr_2(j),'*',match);
                splitStr_2(j) = strjoin(noMatch);
                strrep(splitStr_2(j),'**','*');
            end
        end
    end
    
    for i=1:size(I,2)
        expression = string(I(i));
        for j=1:size(splitStr_2,2)
            [match,noMatch] = regexp(splitStr_2(j),expression,'match','split');
            if not(isempty(match))
                paramStr_2(j) = strcat(paramStr_2(j),'*',match);
                splitStr_2(j) = strjoin(noMatch);
                strrep(splitStr_2(j),'**','*');
            end
        end
    end
    
    for i=1:size(g,2)
        expression = string(g(i));
        for j=1:size(splitStr_2,2)
            [match,noMatch] = regexp(splitStr_2(j),expression,'match','split');
            if not(isempty(match))
                paramStr_2(j) = strcat(paramStr_2(j),'*',match);
                splitStr_2(j) = strjoin(noMatch);
                strrep(splitStr_2(j),'**','*');
            end
        end
    end
    
    for i=1:size(m,2)
        expression = string(m(i));
        for j=1:size(splitStr_2,2)
            [match,noMatch] = regexp(splitStr_2(j),expression,'match','split');
            if not(isempty(match))
                paramStr_2(j) = strcat(paramStr_2(j),'*',match);
                splitStr_2(j) = strjoin(noMatch);
                strrep(splitStr_2(j),'**','*');
            end
        end
    end
    splitStr_2 = strrep(splitStr_2,' *','');

    for j=1:size(paramStr_2,2)
        cellContents = paramStr_2{j};    
        paramStr_2{j} = cellContents(3:end);
    end

    for j=1:size(splitStr_2,2)
        if  splitStr_2{j}(1) ~= '-'
            splitStr_2(j) = strcat('+',splitStr_2(j));
        end
    end

     
    for i=1:size(paramStr_2,2)
        if any(strcmp(Theta,paramStr_2(i)))
            for j=1:size(Theta,2)
                if strcmp(Theta(j),paramStr_2(i))
                    Y_r(2,j) = strcat(Y_r(2,j),splitStr_2(i));
                    break
                end
            end 
        else
            Theta(k) = paramStr_2(i);
            Y_r(2,k) = splitStr_2(i);
            k=k+1;
        end
    end

    %% For third row
    
    Y_r(3,1:size(Theta,2)) = 0;

    for i=1:size(L,2)
        expression = strcat(string(L(i)),'.2');
        for j=1:size(splitStr_3,2)
            [match,noMatch] = regexp(splitStr_3(j),expression,'match','split');
            if not(isempty(match))
                paramStr_3(j) = strcat(paramStr_3(j),'*',match);
                splitStr_3(j) = strjoin(noMatch);
                strrep(splitStr_3(j),'**','*');
            end
        end
    end
    
    for i=size(L,2):1
        expression = string(L(i));
        for j=1:size(splitStr_3,2)
            [match,noMatch] = regexp(splitStr_3(j),expression,'match','split');
            if not(isempty(match))
                paramStr_3(j) = strcat(paramStr_3(j),'*',match);
                splitStr_3(j) = strjoin(noMatch);
                strrep(splitStr_3(j),'**','*');
            end
        end
    end
    
    for i=1:size(I,2)
        expression = string(I(i));
        for j=1:size(splitStr_3,2)
            [match,noMatch] = regexp(splitStr_3(j),expression,'match','split');
            if not(isempty(match))
                paramStr_3(j) = strcat(paramStr_3(j),'*',match);
                splitStr_3(j) = strjoin(noMatch);
                strrep(splitStr_3(j),'**','*');
            end
        end
    end
    
    for i=1:size(g,2)
        expression = string(g(i));
        for j=1:size(splitStr_3,2)
            [match,noMatch] = regexp(splitStr_3(j),expression,'match','split');
            if not(isempty(match))
                paramStr_3(j) = strcat(paramStr_3(j),'*',match);
                splitStr_3(j) = strjoin(noMatch);
                strrep(splitStr_3(j),'**','*');
            end
        end
    end
    
    for i=1:size(m,2)
        expression = string(m(i));
        for j=1:size(splitStr_3,2)
            [match,noMatch] = regexp(splitStr_3(j),expression,'match','split');
            if not(isempty(match))
                paramStr_3(j) = strcat(paramStr_3(j),'*',match);
                splitStr_3(j) = strjoin(noMatch);
                strrep(splitStr_3(j),'**','*');
            end
        end
    end  
    splitStr_3 = strrep(splitStr_3,' *','');
    
    for j=1:size(paramStr_3,2)
        cellContents = paramStr_3{j};    
        paramStr_3{j} = cellContents(3:end);
    end

    for j=1:size(splitStr_3,2)
        if  splitStr_3{j}(1) ~= '-'
            splitStr_3(j) = strcat('+',splitStr_3(j));
        end
    end

         
    for i=1:size(paramStr_3,2)
        if any(strcmp(Theta,paramStr_3(i)))
            for j=1:size(Theta,2)
                if strcmp(Theta(j),paramStr_3(i))
                    Y_r(3,j) = strcat(Y_r(3,j),splitStr_3(i));
                    break
                end
            end 
        else
            Theta(k) = paramStr_3(i);
            Y_r(3,k) = splitStr_3(i);
            k=k+1;
        end
    end
    


    %% for fourth row

    
    Y_r(4,1:size(Theta,2)) = 0;
    
    for i=1:size(L,2)
        expression = strcat(string(L(i)),'.2');
        for j=1:size(splitStr_4,2)
            [match,noMatch] = regexp(splitStr_4(j),expression,'match','split');
            if not(isempty(match))
                paramStr_4(j) = strcat(paramStr_4(j),'*',match);
                splitStr_4(j) = strjoin(noMatch);
                strrep(splitStr_4(j),'**','*');
            end
        end
    end
    
    for i=size(L,2):1
        expression = string(L(i));
        for j=1:size(splitStr_4,2)
            [match,noMatch] = regexp(splitStr_4(j),expression,'match','split');
            if not(isempty(match))
                paramStr_4(j) = strcat(paramStr_4(j),'*',match);
                splitStr_4(j) = strjoin(noMatch);
                strrep(splitStr_4(j),'**','*');
            end
        end
    end
    
    for i=1:size(I,2)
        expression = string(I(i));
        for j=1:size(splitStr_4,2)
            [match,noMatch] = regexp(splitStr_4(j),expression,'match','split');
            if not(isempty(match))
                paramStr_4(j) = strcat(paramStr_4(j),'*',match);
                splitStr_4(j) = strjoin(noMatch);
                strrep(splitStr_4(j),'**','*');
            end
        end
    end
    
    for i=1:size(g,2)
        expression = string(g(i));
        for j=1:size(splitStr_4,2)
            [match,noMatch] = regexp(splitStr_4(j),expression,'match','split');
            if not(isempty(match))
                paramStr_4(j) = strcat(paramStr_4(j),'*',match);
                splitStr_4(j) = strjoin(noMatch);
                strrep(splitStr_4(j),'**','*');
            end
        end
    end
    
    for i=1:size(m,2)
        expression = string(m(i));
        for j=1:size(splitStr_4,2)
            [match,noMatch] = regexp(splitStr_4(j),expression,'match','split');
            if not(isempty(match))
                paramStr_4(j) = strcat(paramStr_4(j),'*',match);
                splitStr_4(j) = strjoin(noMatch);
                strrep(splitStr_4(j),'**','*');
            end
        end
    end
    splitStr_4 = strrep(splitStr_4,' *','');

    for j=1:size(paramStr_4,2)
        cellContents = paramStr_4{j};    
        paramStr_4{j} = cellContents(3:end);
    end

    for j=1:size(splitStr_4,2)
        if  splitStr_4{j}(1) ~= '-'
            splitStr_4(j) = strcat('+',splitStr_4(j));
        end
    end

    
    for i=1:size(paramStr_4,2)
        if any(strcmp(Theta,paramStr_4(i)))
            for j=1:size(Theta,2)
                if strcmp(Theta(j),paramStr_4(i))
                    Y_r(4,j) = strcat(Y_r(4,j),splitStr_4(i));
                    break
                end
            end 
        else
            Theta(k) = paramStr_4(i);
            Y_r(4,k) = splitStr_4(i);
            k=k+1;
        end
    end
    %% for fifth row
    
    Y_r(5,1:size(Theta,2)) = 0;
    
    for i=1:size(L,2)
        expression = strcat(string(L(i)),'.2');
        for j=1:size(splitStr_5,2)
            [match,noMatch] = regexp(splitStr_5(j),expression,'match','split');
            if not(isempty(match))
                paramStr_5(j) = strcat(paramStr_5(j),'*',match);
                splitStr_5(j) = strjoin(noMatch);
                strrep(splitStr_5(j),'**','*');
            end
        end
    end
    
    for i=size(L,2):1
        expression = string(L(i));
        for j=1:size(splitStr_5,2)
            [match,noMatch] = regexp(splitStr_5(j),expression,'match','split');
            if not(isempty(match))
                paramStr_5(j) = strcat(paramStr_5(j),'*',match);
                splitStr_5(j) = strjoin(noMatch);
                strrep(splitStr_5(j),'**','*');
            end
        end
    end
    
    for i=1:size(I,2)
        expression = string(I(i));
        for j=1:size(splitStr_5,2)
            [match,noMatch] = regexp(splitStr_5(j),expression,'match','split');
            if not(isempty(match))
                paramStr_5(j) = strcat(paramStr_5(j),'*',match);
                splitStr_5(j) = strjoin(noMatch);
                strrep(splitStr_5(j),'**','*');
            end
        end
    end
    
    for i=1:size(g,2)
        expression = string(g(i));
        for j=1:size(splitStr_5,2)
            [match,noMatch] = regexp(splitStr_5(j),expression,'match','split');
            if not(isempty(match))
                paramStr_5(j) = strcat(paramStr_5(j),'*',match);
                splitStr_5(j) = strjoin(noMatch);
                strrep(splitStr_5(j),'**','*');
            end
        end
    end
    
    for i=1:size(m,2)
        expression = string(m(i));
        for j=1:size(splitStr_5,2)
            [match,noMatch] = regexp(splitStr_5(j),expression,'match','split');
            if not(isempty(match))
                paramStr_5(j) = strcat(paramStr_5(j),'*',match);
                splitStr_5(j) = strjoin(noMatch);
                strrep(splitStr_5(j),'**','*');
            end
        end
    end
    splitStr_5 = strrep(splitStr_5,' *','');
    
    for j=1:size(paramStr_5,2)
        cellContents = paramStr_5{j};    
        paramStr_5{j} = cellContents(3:end);
    end
    
    for j=1:size(splitStr_5,2)
        if  splitStr_5{j}(1) ~= '-'
            splitStr_5(j) = strcat('+',splitStr_5(j));
        end
    end

    
    for i=1:size(paramStr_5,2)
        if any(strcmp(Theta,paramStr_5(i)))
            for j=1:size(Theta,2)
                if strcmp(Theta(j),paramStr_5(i))
                    Y_r(5,j) = strcat(Y_r(5,j),splitStr_5(i));
                    break
                end
            end 
        else
            Theta(k) = paramStr_5(i);
            Y_r(5,k) = splitStr_5(i);
            k=k+1;
        end
    end
    
    %% for sixth row
    
    Y_r(6,1:size(Theta,2)) = 0;

    
    for i=1:size(L,2)
        expression = strcat(string(L(i)),'.2');
        for j=1:size(splitStr_6,2)
            [match,noMatch] = regexp(splitStr_6(j),expression,'match','split');
            if not(isempty(match))
                paramStr_6(j) = strcat(paramStr_6(j),'*',match);
                splitStr_6(j) = strjoin(noMatch);
                strrep(splitStr_6(j),'**','*');
            end
        end
    end
    
    for i=size(L,2):1
        expression = string(L(i));
        for j=1:size(splitStr_6,2)
            [match,noMatch] = regexp(splitStr_6(j),expression,'match','split');
            if not(isempty(match))
                paramStr_6(j) = strcat(paramStr_6(j),'*',match);
                splitStr_6(j) = strjoin(noMatch);
                strrep(splitStr_6(j),'**','*');
            end
        end
    end
    
    for i=1:size(I,2)
        expression = string(I(i));
        for j=1:size(splitStr_6,2)
            [match,noMatch] = regexp(splitStr_6(j),expression,'match','split');
            if not(isempty(match))
                paramStr_6(j) = strcat(paramStr_6(j),'*',match);
                splitStr_6(j) = strjoin(noMatch);
                strrep(splitStr_6(j),'**','*');
            end
        end
    end
    
    for i=1:size(g,2)
        expression = string(g(i));
        for j=1:size(splitStr_6,2)
            [match,noMatch] = regexp(splitStr_6(j),expression,'match','split');
            if not(isempty(match))
                paramStr_6(j) = strcat(paramStr_6(j),'*',match);
                splitStr_6(j) = strjoin(noMatch);
                strrep(splitStr_6(j),'**','*');
            end
        end
    end
    
    for i=1:size(m,2)
        expression = string(m(i));
        for j=1:size(splitStr_6,2)
            [match,noMatch] = regexp(splitStr_6(j),expression,'match','split');
            if not(isempty(match))
                paramStr_6(j) = strcat(paramStr_6(j),'*',match);
                splitStr_6(j) = strjoin(noMatch);
                strrep(splitStr_6(j),'**','*');
            end
        end
    end
    splitStr_6 = strrep(splitStr_6,' *','');
    
    for j=1:size(splitStr_6,2)
        if  splitStr_6{j}(1) ~= '-'
            splitStr_6(j) = strcat('+',splitStr_6(j));
        end
    end
    
    for j=1:size(paramStr_6,2)
        cellContents = paramStr_6{j};    
        paramStr_6{j} = cellContents(3:end);
    end

    %     
    for i=1:size(paramStr_6,2)
        if any(strcmp(Theta,paramStr_6(i)))
            for j=1:size(Theta,2)
                if strcmp(Theta(j),paramStr_6(i))
                    Y_r(6,j) = strcat(Y_r(6,j),splitStr_6(i));
                    break
                end
            end 
        else
            Theta(k) = paramStr_6(i);
            Y_r(6,k) = splitStr_6(i);
            k=k+1;
        end
    end
    
    %% Test
    Y_r = fillmissing(string(Y_r),'constant',string(0));
%     Theta
    Y_r = str2sym(Y_r)
    MatrixPrint(Y_r,'Y_r')
    Theta = str2sym(Theta)'
    VectorPrint_double(Theta,'Theta')
%     eqm = convertCharsToStrings(text);
%     eqm = splitlines(eqm);
%     eqm = str2sym(eqm);
%     eqm = str2sym(char2strtext);
    
    Result = simplify(simplify(Y_r*Theta - EqM))
%     simplify((str2sym(Y_r) * str2sym(Theta)) - str2sym(text))
    VectorPrint(Result, 'regressor_02.03.0907');
    
    
    