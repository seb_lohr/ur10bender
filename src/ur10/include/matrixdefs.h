#ifndef MATRIXDEFS_H
#define MATRIXDEFS_H

#include <Eigen/Eigen>

namespace My{

typedef Eigen::Matrix<double,STD_DOF,68> MatrixRegressor;
typedef Eigen::Matrix<double,68,1> VectorRegressor;

typedef Eigen::Matrix<double,76,76> Matrix76d;
typedef Eigen::Matrix<double,6,3> Matrix63d;


}


#endif // MATRIXDEFS_H
