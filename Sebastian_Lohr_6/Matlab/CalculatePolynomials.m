function a = CalculatePolynomials(u)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
Qip=deg2rad([u(1);u(2);u(3)]);

% Initial Joint Position
Qi=deg2rad([u(4);u(5);u(6)]);


%Kinematic Parameters
L1=u(7);
L2=u(8);
L3=u(9);
L4=u(10);
L5=u(11);
L6=u(12);

Le=[L1;L2;L3;L4;L5;L6];

%Time
t=u(13);

% Get The Sample Time from the simulink model
dT=str2double(get_param('KSimulator_UR10_3_KCtrl','FixedStep'));


%% Robot Base wrt World coordinate frame

t0_w=[u(14);u(15);u(16)];

phi=u(17);
theta=u(18);
psi=u(19);

T0_W=eye(4);
% dbstack (for debugging)

%% Xef_W Target (increments for the end-effector (ef) in the world coordinate frame wcf)
DXef_t_W(1,1)=u(20);
DXef_t_W(2,1)=u(21);
DXef_t_W(3,1)=u(22);

% Initial time and final time for the trajectory generator
ti=u(23);
tf=u(24);

% Manipulability index (w) threshold
wThreshold=u(25);

%% 
XefIni_0=FK_UR10_3([Qi; Le]);
DXef_t_0=DXef_t_W-T0_W(1:3,4);
Xef_t_0=DXef_t_0+XefIni_0;

x1 = [XefIni_0(1);Xef_t_0(1);0;0;0;0];
x2 = [XefIni_0(2);Xef_t_0(2);0;0;0;0];
x3 = [XefIni_0(3);Xef_t_0(3);0;0;0;0];

T = ...
    [1, ti, ti^2, ti^3, ti^4, ti^5;
     1, tf, tf^2, tf^3, tf^4, tf^5;
     0,  1, 2*ti, 3*ti^2, 4*ti^3, 5*ti^4;
     0,  1, 2*tf, 3*tf^2, 4*tf^3, 5*tf^4;
     0,  0, 2, 6*ti, 12*ti^2, 20*ti^3;
     0,  0, 2, 6*tf, 12*tf^2, 20*tf^3];

 
a = [T\x1, T\x2, T\x3];

