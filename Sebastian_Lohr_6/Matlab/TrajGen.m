    function [Qd] = TrajGen( u )
%TRAJGEN Summary of this function goes here
%   Detailed explanation goes here

global t0 t1 t2 t3 t4 t5 t6 singularity t_s
persistent q1d_old q2d_old q3d_old a1 a2 a3 td1 td2 td3 td Xef_t_old Xef_t_start Xef_tar

% t0 = 10;
t0 = 0;
t1 = 3.5;
t2 = 8;
t3 = 17;

q1d = deg2rad(u(1));
q2d = deg2rad(u(2));
q3d = deg2rad(u(3));

t=u(4);



%Joint Position
q1=u(5);
q2=u(6);
q3=u(7);

Q = [q1;q2;q3];

%Joint Velocity
qp1=u(8);
qp2=u(9);
qp3=u(10);

Qp = [qp1;qp2;qp3];

%Length
L1=u(11);
L2=u(12);
L4=u(13);
L6=u(14);
L7=u(15);
L9=u(16);
L3=u(17);
L5=u(18);
L8=u(19);
L10=u(20);

L=[L1;L2;L4;L6;L7;L9;L3;L5;L8;L10];

%Target position
Xef_t_1 = u(21);
Xef_t_2 = u(22);
Xef_t_3 = u(23);

Xef_t = [Xef_t_1;Xef_t_2;Xef_t_3];

Qd=zeros(9,1);

if (t==0)
    singularity = 0;
end


    Qd=[q1d;q2d;q3d;zeros(6,1)];


%% Joint Space
if (t<t1 && t>=t0)
    
    q1dp= 0;
    q2dp= 0;
    q3dp= 0;

    q1dpp= 0;
    q2dpp= 0;
    q3dpp= 0;

    if (t==t0)
        td1 = t+3.0;
        a1 = PolyJS([q1;qp1;q1d;0;t;td1]);
        td2 = t+3.0;
        a2 = PolyJS([q2;qp2;q2d;0;t;td2]);
        td3 = t+3.0;
        a3 = PolyJS([q3;qp3;q3d;0;t;td3]);
        q1d_old = q1d;
        q2d_old = q2d;
        q3d_old = q3d;
    end 

    if (q1d ~= q1d_old)
        td1 = t+3.0;
        a1 = PolyJS([q1;qp1;q1d;0;t;td1]);
    end 
    if (q2d ~= q2d_old)
        td2 = t+3.0;
        a2 = PolyJS([q2;qp2;q2d;0;t;td2]);
    end 
    if (q3d ~= q3d_old)
        td3 = t+3.0;
        a3 = PolyJS([q3;qp3;q3d;0;t;td3]);
    end 

    q1d_old = q1d;
    q2d_old = q2d;
    q3d_old = q3d;


    if (t<=td1)
        q1d = a1(1,1) + t * a1(2,1) + t^2 * a1(3,1) + t^3 * a1(4,1) + t^4 * a1(5,1) + t^5 * a1(6,1);
        q1dp = a1(2,1) + 2*t * a1(3,1) + 3*t^2 * a1(4,1) + 4*t^3 * a1(5,1) + 5*t^4 * a1(6,1);
        q1dpp = 2 * a1(3,1) + 6*t * a1(4,1) + 12*t^2 * a1(5,1) + 20*t^3 * a1(6,1);
    end

    if (t<=td2)
        q2d = a2(1,1) + t * a2(2,1) + t^2 * a2(3,1) + t^3 * a2(4,1) + t^4 * a2(5,1) + t^5 * a2(6,1);
        q2dp = a2(2,1) + 2*t * a2(3,1) + 3*t^2 * a2(4,1) + 4*t^3 * a2(5,1) + 5*t^4 * a2(6,1);
        q2dpp = 2 * a2(3,1) + 6*t * a2(4,1) + 12*t^2 * a2(5,1) + 20*t^3 * a2(6,1);
    end

    if( t<=td3)
        q3d = a3(1,1) + t * a3(2,1) + t^2 * a3(3,1) + t^3 * a3(4,1) + t^4 * a3(5,1) + t^5 * a3(6,1);
        q3dp = a3(2,1) + 2*t * a3(3,1) + 3*t^2 * a3(4,1) + 4*t^3 * a3(5,1) + 5*t^4 * a3(6,1);
        q3dpp = 2 * a3(3,1) + 6*t * a3(4,1) + 12*t^2 * a3(5,1) + 20*t^3 * a3(6,1);
    end
    % Interval between PD, PD+G, and PID+G
    % Generate Different trajectories at different times

    qd = [q1d;q2d;q3d];
    qdp = [q1dp;q2dp;q3dp];
    qdpp = [q1dpp;q2dpp;q3dpp];

    Qd=[qd;qdp;qdpp];

end


%% Singularity


%Compute Jacobian 
J3_0 = ...
    [ L2*cos(q1) + L4*cos(q1) + L3*cos(q2)*sin(q1) + L5*cos(q2)*cos(q3)*sin(q1) + L5*sin(q1)*sin(q2)*sin(q3), cos(q1)*(L3*sin(q2) + L5*sin(q2 - q3)), -L5*sin(q2 - q3)*cos(q1);
      L2*sin(q1) + L4*sin(q1) - L3*cos(q1)*cos(q2) - L5*cos(q1)*cos(q2)*cos(q3) - L5*cos(q1)*sin(q2)*sin(q3), sin(q1)*(L3*sin(q2) + L5*sin(q2 - q3)), -L5*sin(q2 - q3)*sin(q1);
                                                                                                           0,         - L3*cos(q2) - L5*cos(q2 - q3),          L5*cos(q2 - q3)];

% Compute the manipulability Index (w)
[~,S,~] = svd(J3_0);
w=S(1,1)*S(2,2)*S(3,3);


if ((t>=t1 && w<0.1) || singularity)
    if (not(singularity))
        t_s = t;
        td = t+5.0;
        a1 = PolyJS([q1;qp1;q1;0;t;td]);
        a2 = PolyJS([q2;qp2;deg2rad(45);0;t;td]);
        a3 = PolyJS([q3;qp3;deg2rad(45);0;t;td]);
    end 

    if (t<=td)
        q1d = a1(1,1) + t * a1(2,1) + t^2 * a1(3,1) + t^3 * a1(4,1) + t^4 * a1(5,1) + t^5 * a1(6,1);
        q1dp = a1(2,1) + 2*t * a1(3,1) + 3*t^2 * a1(4,1) + 4*t^3 * a1(5,1) + 5*t^4 * a1(6,1);
        q2d = a2(1,1) + t * a2(2,1) + t^2 * a2(3,1) + t^3 * a2(4,1) + t^4 * a2(5,1) + t^5 * a2(6,1);
        q2dp = a2(2,1) + 2*t * a2(3,1) + 3*t^2 * a2(4,1) + 4*t^3 * a2(5,1) + 5*t^4 * a2(6,1);
        q3d = a3(1,1) + t * a3(2,1) + t^2 * a3(3,1) + t^3 * a3(4,1) + t^4 * a3(5,1) + t^5 * a3(6,1);
        q3dp = a3(2,1) + 2*t * a3(3,1) + 3*t^2 * a3(4,1) + 4*t^3 * a3(5,1) + 5*t^4 * a3(6,1);
        q1dpp = 2 * a1(3,1) + 6*t * a1(4,1) + 12*t^2 * a1(5,1) + 20*t^3 * a1(6,1);
        q2dpp = 2 * a2(3,1) + 6*t * a2(4,1) + 12*t^2 * a2(5,1) + 20*t^3 * a2(6,1);
        q3dpp = 2 * a3(3,1) + 6*t * a3(4,1) + 12*t^2 * a3(5,1) + 20*t^3 * a3(6,1);
    end
    
    if (t>td)
        q1d = q1;
        q2d = deg2rad(45);
        q3d = deg2rad(45);
        q1dp= 0;
        q2dp= 0;
        q3dp= 0;
        q1dpp= 0;
        q2dpp= 0;
        q3dpp= 0;
    end
    
    singularity = 1;
    qd = [q1d;q2d;q3d];
    qdp = [q1dp;q2dp;q3dp];
    qdpp = [q1dpp;q2dpp;q3dpp];

    Qd=[qd;qdp;qdpp];


end




%% Operational Space

if (t>=t1 && not(singularity))
    
    
    Xef_Ini = FK_ur10_3DOF([Q;Qp;L]);
    Xpef_Ini = DifFK_ur10_3DOF([Q;Qp;L]);
%     Xpef_Ini = [0;0;0];

    
% %For relative position input    
%     if (t==t1)
%         td = t + 5.0;
%         Xef_tar = Xef_t + Xef_Ini;
%         a1 = PolyJS([Xef_Ini(1);Xpef_Ini(1);Xef_tar(1);0;t;td]);
%         a2 = PolyJS([Xef_Ini(2);Xpef_Ini(2);Xef_tar(2);0;t;td]);
%         a3 = PolyJS([Xef_Ini(3);Xpef_Ini(3);Xef_tar(3);0;t;td]);
%         Xef_t_old = Xef_t;
%         Xef_t_start = Xef_Ini; 
%     end
%     if not(isequal(Xef_t,Xef_t_old))
%         td = t + 5.0;
%         Xef_tar = Xef_t_start + Xef_t;
%         a1 = PolyJS([Xef_Ini(1);Xpef_Ini(1);Xef_tar(1);0;t;td]);
%         a2 = PolyJS([Xef_Ini(2);Xpef_Ini(2);Xef_tar(2);0;t;td]);
%         a3 = PolyJS([Xef_Ini(3);Xpef_Ini(3);Xef_tar(3);0;t;td]);
%         Xef_t_old = Xef_t;
%     end 

    if (t==t1)
        td = t + 5.0;
        a1 = PolyJS([Xef_Ini(1);Xpef_Ini(1);Xef_t(1);0;t;td]);
        a2 = PolyJS([Xef_Ini(2);Xpef_Ini(2);Xef_t(2);0;t;td]);
        a3 = PolyJS([Xef_Ini(3);Xpef_Ini(3);Xef_t(3);0;t;td]);
        Xef_t_old = Xef_t;
    end
    
    if not(isequal(Xef_t,Xef_t_old))
        td = t + 5.0;
        a1 = PolyJS([Xef_Ini(1);Xpef_Ini(1);Xef_t(1);0;t;td]);
        a2 = PolyJS([Xef_Ini(2);Xpef_Ini(2);Xef_t(2);0;t;td]);
        a3 = PolyJS([Xef_Ini(3);Xpef_Ini(3);Xef_t(3);0;t;td]);
        Xef_t_old = Xef_t;
    end 


    if (t<td)
        Xef_d_1 = a1(1,1) + t * a1(2,1) + t^2 * a1(3,1) + t^3 * a1(4,1) + t^4 * a1(5,1) + t^5 * a1(6,1);
        Xpef_d_1 = a1(2,1) + 2*t * a1(3,1) + 3*t^2 * a1(4,1) + 4*t^3 * a1(5,1) + 5*t^4 * a1(6,1);
        Xppef_d_1 = 2 * a1(3,1) + 6*t * a1(4,1) + 12*t^2 * a1(5,1) + 20*t^3 * a1(6,1);

        Xef_d_2 = a2(1,1) + t * a2(2,1) + t^2 * a2(3,1) + t^3 * a2(4,1) + t^4 * a2(5,1) + t^5 * a2(6,1);
        Xpef_d_2 = a2(2,1) + 2*t * a2(3,1) + 3*t^2 * a2(4,1) + 4*t^3 * a2(5,1) + 5*t^4 * a2(6,1);
        Xppef_d_2 = 2 * a2(3,1) + 6*t * a2(4,1) + 12*t^2 * a2(5,1) + 20*t^3 * a2(6,1);

        Xef_d_3 = a3(1,1) + t * a3(2,1) + t^2 * a3(3,1) + t^3 * a3(4,1) + t^4 * a3(5,1) + t^5 * a3(6,1);
        Xpef_d_3 = a3(2,1) + 2*t * a3(3,1) + 3*t^2 * a3(4,1) + 4*t^3 * a3(5,1) + 5*t^4 * a3(6,1);
        Xppef_d_3 = 2 * a3(3,1) + 6*t * a3(4,1) + 12*t^2 * a3(5,1) + 20*t^3 * a3(6,1);

        Xef_d = [Xef_d_1;Xef_d_2;Xef_d_3];
        Xpef_d = [Xpef_d_1;Xpef_d_2;Xpef_d_3];
        Xppef_d = [Xppef_d_1;Xppef_d_2;Xppef_d_3];
    end
    
    if (t>=td)
        
        Xef_d = Xef_t;
        
        Xpef_d = zeros(3,1);
        
        Xppef_d = zeros(3,1);

    end

    
    
    Qd = [Xef_d;Xpef_d;Xppef_d];
end



end

