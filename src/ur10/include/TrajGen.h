#ifndef TRAJGEN_H
#define TRAJGEN_H

#include<tum_ics_ur_robot_lli/RobotControllers/ControlEffort.h>
#include"matrixdefs.h"


class TrajGen{
    // Member variables
private:
    Tum::Vector6d a1d;
    Tum::Matrix6d a6d;

    // Member Methods
public:
    TrajGen();
    ~TrajGen();

    void CalcPolynomial1d(double &s, double &s_t, double &sp, double &sp_t, double &spp, double &spp_t, double ti, double tf);
    void CalculatePolynomial6d(Tum::Vector6d &S, Tum::Vector6d &S_t, Tum::Vector6d &Sp, Tum::Vector6d &Sp_t, Tum::Vector6d &Spp, Tum::Vector6d &Spp_t, double ti, double tf);

    Tum::Vector3d GetCurrentStatesOnTrajectory1d(double t, double targettime);
    My::Matrix63d GetCurrentStatesOnTrajectory6d(double t, double targettime);
    // Member Methods
private:
    Tum::Vector6d CalcPolynomial(double &s, double &s_t, double &sp, double &sp_t, double &spp, double &spp_t, double &ti, double &tf);

};



#endif // TRAJGEN_H
