
#include"../include/TrajGen.h"


TrajGen::TrajGen(){

    a1d.setZero();
    a6d.setZero();

}


TrajGen::~TrajGen(){

}



Tum::Vector6d TrajGen::CalcPolynomial(double &s, double &s_t, double &sp, double &sp_t, double &spp, double &spp_t, double &ti, double &tf){

  Tum::Vector6d a;
  Tum::Vector6d S;


  S << s, s_t, sp, sp_t, spp, spp_t;


	double ti2 = pow(ti,2);
	double ti3 = pow(ti,3);
	double ti4 = pow(ti,4);
	double ti5 = pow(ti,5);
	double tf2 = pow(tf,2);
	double tf3 = pow(tf,3);
	double tf4 = pow(tf,4);
	double tf5 = pow(tf,5);

  Tum::Matrix6d T;
  T << 	1, ti, ti2, ti3, ti4, ti5,
        1, tf, tf2, tf3, tf4, tf5,
        0, 1, 2*ti, 3*ti2, 4*ti3, 5*ti4,
        0, 1, 2*tf, 3*tf2, 4*tf3, 5*tf4,
        0, 0, 2, 6*ti, 12*ti2, 20*ti3,
        0, 0, 2, 6*tf, 12*tf2, 20*tf3;
	
//  ROS_INFO_STREAM(T);
  a = T.inverse()*S;
  return a;

}

void TrajGen::CalcPolynomial1d(double &s, double &s_t, double &sp, double &sp_t, double &spp, double &spp_t, double ti, double tf){
    a1d = CalcPolynomial(s, s_t, sp, sp_t, spp, spp_t, ti, tf);
//    ROS_INFO_STREAM("a1d");
//    ROS_INFO_STREAM(a1d);
}


void TrajGen::CalculatePolynomial6d(Tum::Vector6d &S, Tum::Vector6d &S_t, Tum::Vector6d &Sp, Tum::Vector6d &Sp_t, Tum::Vector6d &Spp, Tum::Vector6d &Spp_t, double ti, double tf){
  Tum::Matrix6d a;
  if( (tf-ti) <= 0){
    tf = ti+10;
  }
  for (int i = 0; i<6; i++){
    Tum::Vector6d vector;
    vector = CalcPolynomial(S[i], S_t[i], Sp[i], Sp_t[i], Spp[i], Spp_t[i], ti, tf);
    for (int j = 0; j<6; j++){
        a(i,j) = vector[j];
    }
  }
  a6d = a;
  // ROS_INFO_STREAM("a6d");
  // ROS_INFO_STREAM(a6d);
}


Tum::Vector3d TrajGen::GetCurrentStatesOnTrajectory1d(double t, double targettime){
    Tum::Vector3d S_d;
    S_d(0) = a1d[1] + t * a1d[2] + t*t * a1d[3] + t*t*t * a1d[4] + t*t*t*t * a1d[5] + t*t*t*t*t * a1d[6];
    S_d(1) = a1d[2] + 2*t * a1d[3] + 3*t*t * a1d[4] + 4*t*t*t * a1d[5] + 5*t*t*t*t * a1d[6];
    S_d(2) = 2 * a1d[3] + 6*t * a1d[4] + 12*t*t * a1d[5] + 20*t*t*t * a1d[6];
    if (t>=targettime){
      S_d(0) = a1d[1] + targettime * a1d[2] + targettime*targettime * a1d[3] + targettime*targettime*targettime * a1d[4] + targettime*targettime*targettime*targettime * a1d[5] + targettime*targettime*targettime*targettime*targettime * a1d[6];
      S_d(1) = a1d[2] + 2*targettime * a1d[3] + 3*targettime*targettime * a1d[4] + 4*targettime*targettime*targettime * a1d[5] + 5*targettime*targettime*targettime*targettime * a1d[6];
      S_d(2) = 2 * a1d[3] + 6*targettime * a1d[4] + 12*targettime*targettime * a1d[5] + 20*targettime*targettime*targettime * a1d[6];
    }
    return S_d;
}

My::Matrix63d TrajGen::GetCurrentStatesOnTrajectory6d(double t, double targettime){
    My::Matrix63d S_d;

    for (int i=0; i<6; i++){
      S_d(i,0) = a6d(i,0) + t * a6d(i,1) + t*t * a6d(i,2) + t*t*t * a6d(i,3) + t*t*t*t * a6d(i,4) + t*t*t*t*t * a6d(i,5);
      S_d(i,1) = a6d(i,1) + 2*t * a6d(i,2) + 3*t*t * a6d(i,3) + 4*t*t*t * a6d(i,4) + 5*t*t*t*t * a6d(i,5);
      S_d(i,2) = 2 * a6d(i,2) + 6*t * a6d(i,3) + 12*t*t * a6d(i,4) + 20*t*t*t * a6d(i,5);
      if (t>=targettime){
        S_d(i,0) = a6d(i,0) + targettime * a6d(i,1) + targettime*targettime * a6d(i,2) + targettime*targettime*targettime * a6d(i,3) + targettime*targettime*targettime*targettime * a6d(i,4) + targettime*targettime*targettime*targettime*targettime * a6d(i,5);
        S_d(i,1) = a6d(i,1) + 2*targettime * a6d(i,2) + 3*targettime*targettime * a6d(i,3) + 4*targettime*targettime*targettime * a6d(i,4) + 5*targettime*targettime*targettime*targettime * a6d(i,5);
        S_d(i,2) = 2 * a6d(i,2) + 6*targettime * a6d(i,3) + 12*targettime*targettime * a6d(i,4) + 20*targettime*targettime*targettime * a6d(i,5);
      }
    }
//    ROS_INFO_STREAM(S_d);
//    ROS_INFO_STREAM(t);
//    ROS_INFO_STREAM(targettime);
    return S_d;
}

