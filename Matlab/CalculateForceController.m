%Needs Homogeneous transformations from CalculateEquationofMotion.m


%% Calculate Phi(q):
Phi_q = simplify(H6_0(3,4));


%% Calculate Jacobian
J_phi = sym(zeros(1,6));
for i=1:6
    J_phi(i) = simplify(diff(Phi_q,q(i)));
    
end



Q = sym(eye(6))-transpose(J_phi)*J_phi;
J_phi_dot = simplify(diff(J_phi, q1) * q1p + diff(J_phi, q2) * q2p + diff(J_phi, q2) * q2p...
           + diff(J_phi, q4) * q4p + diff(J_phi, q5) * q5p + diff(J_phi, q6) * q6p);

simplify(Q*J_phi')
simplify(J6_0*J_phi')

       
       
VectorPrint_double(J_phi,'J_phi');
VectorPrint_double(J_phi,'J_phi');

