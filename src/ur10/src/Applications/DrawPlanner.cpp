#include "../include/Drawing.h"

// MSG/SRV Includes
#include "ur10bender/GoalQ.h"
#include "../include/Control.h"

#define DRAW_PREF "[DrawPlanner] "


int main(int argc, char **argv) {
    ros::init(argc, argv, "drawer");
    ros::NodeHandle n;
    // ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);

    int drawForce = 0;
    std::string s = "/DrawPlanner/simple_effort_ctrl/drawForce";
    ros::param::get(s, drawForce);
    ROS_INFO_STREAM(DRAW_PREF << "draw with force: " << drawForce);
    
    ros::Rate loop_rate(10);
    tum_ics_ur_robot_lli::RobotTime time(ros::Time::now());

    // init drawing planner, start 90000 secs after start
    Drawing draw_planner;
    draw_planner.init(n, 90000);

    ROS_INFO_STREAM(DRAW_PREF << "Start draw planner");

    ros::Rate r(30);

    while(ros::ok()) {

        ros::spinOnce();
        ros::param::get(s, drawForce);

        // update time
        time.update(ros::Time::now());
        
        // if start time reached, call the service for shape detection
        if (time.tD() > draw_planner.getStartTime() && draw_planner.getDrawing() == 0) {
            int ret = draw_planner.serviceCall();
            if (ret == 0) {
                draw_planner.setDrawing(1);
                ROS_INFO_STREAM(DRAW_PREF << "Planning Bender's moves");
                draw_planner.calculateTimeSteps();
            }
        }

        if (time.tD() > draw_planner.getStartTime() && (draw_planner.getDrawing() == 1 || draw_planner.getDrawing() == 2)) {
            // actually draw
            if (drawForce) {
                draw_planner.nextStepFSSimple(time.tD());
            } else {
                draw_planner.nextStep(time.tD());
            }
        }

        r.sleep();

        ros::spinOnce();

    }

    ROS_INFO_STREAM(DRAW_PREF << "Stopped draw planner");

}