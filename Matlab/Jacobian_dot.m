%% calculate the time-derivative of the Jacobian

J6_0_dot = diff(J6_0, q1) * q1p + diff(J6_0, q2) * q2p + diff(J6_0, q2) * q2p...
           + diff(J6_0, q4) * q4p + diff(J6_0, q5) * q5p + diff(J6_0, q6) * q6p;