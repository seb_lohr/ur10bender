// MSG/SRV INCLUDES
#include "../include/Drawing.h"
#include "../include/Control.h"
#include "find_goal/Shape.h"
#include "ur10bender/GoalQ.h"
#include "ur10bender/GoalX.h"
#include "ur10bender/DrawGoals.h"
#include "ur10bender/DataController.h"

#include "ur10bender/drawConfig.h"
#include "ur10bender/drawConfigRequest.h"
#include "ur10bender/drawConfigResponse.h"

#include "std_srvs/Trigger.h"

// ICS Includes
#include<tum_ics_ur_robot_msgs/ControlData.h>

// ROS Includes
#include <ros/ros.h>
#include <Eigen/Eigen>

// boost for vector operation
#include<boost/range/numeric.hpp>

#define DRAW_PREF "[DrawPlanner] "

Drawing::Drawing() {
    m_timeSteps = {};
    m_shapeCoordinates = {};

    // never ever start drawing directly at zero seconds
    m_startTime = 999;
    m_currStep = 0;
    m_drawing = 0;

    m_countMutex = PTHREAD_MUTEX_INITIALIZER;
}

Drawing::~Drawing() {

    pthread_mutex_destroy(&this->m_countMutex);

}

/** Initialise the robot after start up
 */
void Drawing::init(ros::NodeHandle n, double start_time) {

    m_shapeClient = n.serviceClient<find_goal::Shape>("/find_goal/shape_location");
    m_goalPubQ = n.advertise<ur10bender::GoalQ>("/bender/goal_q", 1000, false);
    m_goalPubX = n.advertise<ur10bender::GoalX>("/bender/goal_x", 1000, false);
    m_drawGoalsPub = n.advertise<ur10bender::DrawGoals>("/bender/draw_goals", 1000, false);
    m_goalsReached = n.subscribe("/bender/goal_reached", 100, &Drawing::GoalReachedCallback, this);

    m_drawPlanning = n.advertiseService("/bender/draw_planning", &Drawing::startDrawPlanning, this);
    m_drawInitialise = n.advertiseService("/bender/draw_init", &Drawing::initDrawing, this);
    m_startTime = start_time;
    m_currStep = 0;

    m_goalReachedFS = false;
}

void Drawing::GoalReachedCallback(const std_msgs::String& msg) {

    ROS_INFO_STREAM("reached goal");
    m_goalReachedFS = true;
}

/** @brief service call back to start drawing
 * 
 * This services allows to trigger our actual drawing tasks and
 * starts the drawing process.
 */ 
bool Drawing::startDrawPlanning(ur10bender::drawConfigRequest& req, ur10bender::drawConfigResponse& res) {
    if (req.start) {
        // get current time of control
        ros::Duration timeout(2, 0);
        ur10bender::DataControllerConstPtr benderData = ros::topic::waitForMessage<ur10bender::DataController>("/bender/SimpleEffortCtrlData", timeout);
        double curr_time = benderData->time;

        if (req.start_time + curr_time > m_startTime && (m_drawing == 1 || m_drawing == 2)) {
            // bender is currently drawing
            res.result = false;
            res.reason = "Bender already started.";
        } else if (req.start_time + curr_time > m_startTime && m_drawing > 2) {
            // bender is already done, start again
            res.result = true;
            m_startTime = req.start_time  + curr_time;
            m_currStep = 0;
            m_drawing = 0;
            std::stringstream reason;
            reason <<  "Repeat drawing in " << req.start_time;
            res.reason = reason.str();
        } else if (req.start_time + curr_time < m_startTime) {
            // nothing happened so far, set new start time
            m_startTime = req.start_time + curr_time;
            res.result = true;
            std::stringstream reason;
            reason << "Bender will start in " << m_startTime - curr_time;
            res.reason = reason.str();
        }
        return true;
    } else if (!req.start) {
        // TODO: stop drawing and return home?
        ROS_INFO_STREAM(DRAW_PREF << "Bringing bender home.");
        ur10bender::GoalQ goalMsg;
        goalMsg.q_desired = {0, -90, 0, -90, 0, 0};
        goalMsg.now_time = 30;
        goalMsg.set_ctrl = true;
        m_goalPubQ.publish(goalMsg);
        sleep(2);
        m_goalPubQ.publish(goalMsg);
        res.result = true;
        res.reason = "Stopped Bender now.";
        m_currStep = 0;
        m_drawing = 0;
        m_startTime = 999999;
        return true;
    }
    return false;
}

/** Move robot to position where we can start with drawing and switch controllers */
bool Drawing::initDrawing(std_srvs::TriggerRequest& req, std_srvs::TriggerResponse& res) {

    ROS_INFO_STREAM(DRAW_PREF << "Init drawing position...");
    ur10bender::GoalX goalX_msg;
    
    goalX_msg.x_desired = {-0.85, 0.0, 0.4};
    
    goalX_msg.orientation_desired = {0.785, -1.57, 0};
    goalX_msg.now_time = 30;
    goalX_msg.set_ctrl = true;
    // publish next goal
    m_goalPubX.publish(goalX_msg);
    sleep(2);
    m_goalPubX.publish(goalX_msg);

    res.success = true;
    res.message = "Switch to OS and move robot to starting pose";
    return true;
}

/** @brief call service from find_goal node to obtain shapes
 * 
 * accquire shape coordinates which should then be drawn by 
 * the robot. Remap them to the actual location of the paper
 * on which we will draw.
 */
int Drawing::serviceCall() {
    find_goal::Shape req;
    req.request.send = 1;

    // transformation the coordinates to the actual goal
    double tx = -0.60, ty = 0.2, tz = 0.20;
    Tum::Matrix4d trafo;
    trafo << -1, 0, 0, tx,
              0, 1, 0, ty,
              0, 0, 1, tz,
              0, 0, 0, 1;

    // call the service and store response in 
    std::vector<Tum::Vector4d> origCoordinates;
    if (m_shapeClient.call(req)) {
        int no_points = req.response.shape.size();
        origCoordinates.resize(no_points+2);
        ROS_INFO_STREAM(DRAW_PREF << "Received new goals:");
        for (int i=0; i< no_points; i++) {
            origCoordinates[i][0] = req.response.shape[i].x;
            origCoordinates[i][1] = req.response.shape[i].y;
            origCoordinates[i][2] = req.response.shape[i].z;
            origCoordinates[i][3] = 1;
            origCoordinates[i] = trafo * origCoordinates[i];
            ROS_INFO_STREAM(DRAW_PREF << i << ": " << origCoordinates[i].transpose());
        }
        // last point shoudl be first point again
        origCoordinates[no_points][0] = req.response.shape[0].x;
        origCoordinates[no_points][1] = req.response.shape[0].y;
        origCoordinates[no_points][2] = req.response.shape[0].z;
        origCoordinates[no_points][3] = 1;
        origCoordinates[no_points] = trafo * origCoordinates[no_points];
        ROS_INFO_STREAM(DRAW_PREF << no_points << ": " << origCoordinates[no_points].transpose());
        // add step after last point: go up 20cm
        origCoordinates[no_points+1][0] = req.response.shape[0].x;
        origCoordinates[no_points+1][1] = req.response.shape[0].y;
        origCoordinates[no_points+1][2] = req.response.shape[0].z + 0.2;
        origCoordinates[no_points+1][3] = 1;
        origCoordinates[no_points+1] = trafo * origCoordinates[no_points+1];
        ROS_INFO_STREAM(DRAW_PREF << no_points+1 << ": " << origCoordinates[no_points+1].transpose());
        
    } else {
        ROS_ERROR_STREAM(DRAW_PREF << "Failed to call service 'shape_location'");
        return 1;
    }

    pthread_mutex_lock(&this->m_countMutex);
    // save resulting coordinates
    m_shapeCoordinates.resize(origCoordinates.size());
    for (uint i=0; i<origCoordinates.size(); i++) {
        m_shapeCoordinates[i] = origCoordinates[i].head<3>();
    }
    pthread_mutex_unlock(&this->m_countMutex);

    return 0;
}

/** @brief calculate duration of each move between two points
 * 
 * based on the points in the m_shape_coordinates, calculate
 * the time the robot gets to draw the line between two points. 
 */
void Drawing::calculateTimeSteps() {
    pthread_mutex_lock(&this->m_countMutex);
    double no_shape_points = m_shapeCoordinates.size();
    m_timeSteps.resize(no_shape_points + 1);
    Tum::Vector3d diff;
    double dist = 0.0;

    // first step: get current position and calc distance with timeout of 5sec
    ros::Duration timeout(5, 0);
    ur10bender::DataControllerConstPtr benderData = ros::topic::waitForMessage<ur10bender::DataController>("/bender/SimpleEffortCtrlData", timeout);
    Tum::Vector3d currX;
    for (int i=0; i<3; i++) {
        currX[i] = benderData->Xef_0[i];
    }
    diff = currX - m_shapeCoordinates[0];
    dist = diff.norm();
    m_timeSteps[0] = dist * 50;
    ROS_INFO_STREAM(DRAW_PREF << "step " << 0 << ": " << m_timeSteps[0]);

    // steps in shape
    for (int i=1; i<no_shape_points+1; i++) {
        // calculate the distance to the following point
        diff = m_shapeCoordinates[i-1] - m_shapeCoordinates[i];
        dist = diff.norm();
        // plan 5sec 0.10m
        m_timeSteps[i] = dist * 50;
        if (m_timeSteps[i] < 1) {
            m_timeSteps[i] += 2;
        }
        ROS_INFO_STREAM(DRAW_PREF << "step " << i << ": " << m_timeSteps[i]);
    }
    pthread_mutex_unlock(&this->m_countMutex);

}

/** @brief Perform the next step in the drawing 
 * 
 * checks the time and publishs a corresponding 
 * goal if necessary 
 */
void Drawing::nextStep(double time) {
    // calc time from start until last step
    double time_since_start = 0.0;
    for (int i=0; i<m_currStep; i++ ){
        time_since_start += m_timeSteps[i];
    }
    // ROS_INFO_STREAM("time since start " << time_since_start);

    // publish first goal, 
    if (m_drawing ==1) {
        m_drawing = 2;
        m_currStep++;

        pthread_mutex_lock(&this->m_countMutex);
        Tum::Vector3d currShape = m_shapeCoordinates[0];
        pthread_mutex_unlock(&this->m_countMutex);

        ur10bender::GoalX goalX_msg;
        for (int i=0; i<3; i++) {
            goalX_msg.x_desired[i] = currShape[i];
        }
        goalX_msg.orientation_desired = {0.785, -1.57, 0};
        goalX_msg.now_time = m_timeSteps[0];
        goalX_msg.set_ctrl = false;
        // publish next goal
        m_goalPubX.publish(goalX_msg);
    }

    if (time - m_startTime - time_since_start > m_timeSteps[m_currStep]) {
        // the next step is due
        ROS_INFO_STREAM(DRAW_PREF << "Current Step " << m_currStep);

        pthread_mutex_lock(&this->m_countMutex);
        Tum::Vector3d currShape = m_shapeCoordinates[m_currStep];
        pthread_mutex_unlock(&this->m_countMutex);

        ur10bender::GoalX goalX_msg;
        for (int i=0; i<3; i++) {
            goalX_msg.x_desired[i] = currShape[i];
        }
        goalX_msg.orientation_desired = {0.785, -1.57, 0};
        goalX_msg.now_time = m_timeSteps[m_currStep];
        goalX_msg.set_ctrl = false;
        // publish next goal
        m_goalPubX.publish(goalX_msg);
        ROS_INFO_STREAM(DRAW_PREF << "In this step we have time: " << m_timeSteps[m_currStep]);
        m_currStep++;
    }

    if (m_currStep == m_timeSteps.size()-1) {
        ROS_INFO_STREAM(DRAW_PREF << "Done drawing at time " << time << " after started at " << m_startTime);
        m_drawing = 3;
    }
    
}

/** @brief Perform the next step in the drawing 
 * 
 * checks topic goal_reached and publishes new goal
 */
void Drawing::nextStepFSSimple(double time) {
    // publish first goal
    
// calc time from start until last step
    double time_since_start = 0.0;
    for (int i=0; i<m_currStep; i++ ){
        time_since_start += m_timeSteps[i];
    }
    // ROS_INFO_STREAM("time since start " << time_since_start);

    // publish first goal, 
    if (m_drawing == 1) {
        if (m_goalReachedFS) {
            m_drawing = 2;
            m_currStep++;
            m_startTime = time;
        }

        pthread_mutex_lock(&this->m_countMutex);
        Tum::Vector3d currShape = m_shapeCoordinates[0];
        pthread_mutex_unlock(&this->m_countMutex);

        ur10bender::GoalX goalX_msg;
        for (int i=0; i<3; i++) {
            goalX_msg.x_desired[i] = currShape[i];
        }
        goalX_msg.orientation_desired = {0.785, -1.57, 0};
        goalX_msg.now_time = m_timeSteps[0];
        goalX_msg.set_ctrl = false;
        // publish next goal
        m_goalPubX.publish(goalX_msg);

        sleep(1);
        ros::param::set("/Bender/simple_effort_ctrl/controller", FS_SIMPLE_CONTROLLER);

    } else if (time - m_startTime - time_since_start > m_timeSteps[m_currStep]) {
        // the next step is due
        ROS_INFO_STREAM(DRAW_PREF << "Current Step " << m_currStep);

        pthread_mutex_lock(&this->m_countMutex);
        Tum::Vector3d currShape = m_shapeCoordinates[m_currStep];
        pthread_mutex_unlock(&this->m_countMutex);

        ur10bender::GoalX goalX_msg;
        for (int i=0; i<3; i++) {
            goalX_msg.x_desired[i] = currShape[i];
        }
        goalX_msg.orientation_desired = {0.785, -1.57, 0};
        goalX_msg.now_time = m_timeSteps[m_currStep];
        goalX_msg.set_ctrl = false;
        // publish next goal
        m_goalPubX.publish(goalX_msg);
        ROS_INFO_STREAM(DRAW_PREF << "In this step we have time: " << m_timeSteps[m_currStep]);
        m_currStep++;
    }

    if (m_currStep == m_timeSteps.size()-1) {
        ros::param::set("/Bender/simple_effort_ctrl/controller", OS_CONTROLLER);
        ROS_INFO_STREAM(DRAW_PREF << "Done drawing at time " << time << " after started at " << m_startTime);
        m_drawing = 3;
    }
}

/** @brief publish all goals of the shape
 * 
 * takes the shapes from find_goal and the corresponding time steps
 * in order to publish an array of goals together with their time
 */
void Drawing::publishGoals() {

    pthread_mutex_lock(&this->m_countMutex);
    std::vector<Tum::Vector3d> shapes = m_shapeCoordinates;
    pthread_mutex_unlock(&this->m_countMutex);

    // message
    ur10bender::DrawGoals goals;
    // size corresponds to # points in shape
    goals.size = shapes.size();
    if (goals.size != m_timeSteps.size()) {
        ROS_WARN_STREAM("Goal size " << goals.size << " not equal to number time steps " << m_timeSteps.size());
    }

    goals.draw_goals.resize(goals.size);
    for (uint i=0; i< goals.size; i++) {
        ur10bender::GoalX goal_x;
        goal_x.x_desired[0] = shapes[i](0);
        goal_x.x_desired[1] = shapes[i](1);
        goal_x.x_desired[2] = shapes[i](2);
        goal_x.orientation_desired = {0, -1.57, 0};
        goal_x.now_time = m_timeSteps[i];
    }

    m_drawGoalsPub.publish(goals);

} 

/** Get starting time */
double Drawing::getStartTime() {
    return m_startTime;
}

/** Get number of time steps */
double Drawing::sizeTimeSteps() {
    return m_timeSteps.size();
}

/** set if robot is currently drawing */
void Drawing::setDrawing(int drawing) {
    m_drawing = drawing;
}

/** check if robot is currently drawing */
int Drawing::getDrawing() {
    return m_drawing;
}